// import packages
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const path = require("path");

// app
const app = express();

// connect database
// mongoose
//   .connect(
//     "mongodb+srv://admin:admin123@ransisarcade-rdqwa.mongodb.net/test?retryWrites=true&w=majority" ||
//       "mongodb://localhost:27017/ecommerce",
//     {
//       useNewUrlParser: true,
//       useCreateIndex: true
//     }
//   )
//   .then(result => console.log("db is running"))
//   .catch(err => console.log(err));

mongoose
  .connect(
    process.env.MONGO_URI || "mongodb://localhost:27017/ecommerce",
    {
      useNewUrlParser: true,
      useCreateIndex: true
    }
  )
  .then(result => console.log("db is running"))
  .catch(err => console.log(err));

// load routers
const authRoutes = require("./routes/api/authRoutes");
const userRoutes = require("./routes/api/userRoutes");
const categoryRoutes = require("./routes/api/categoryRoutes");
const fabricRoutes = require("./routes/api/fabricRoutes");
const productRoutes = require("./routes/api/productRoutes");
const deliveryFeeRoutes = require("./routes/api/deliveryFeeRoutes");
const braintreeRoutes = require("./routes/api/braintreeRoutes");
const orderRoutes = require("./routes/api/orderRoutes");
const testimonialRoutes = require("./routes/api/testimonialRoutes");
const careerRoutes = require("./routes/api/careerRoutes");
const advertisementRoutes = require("./routes/api/advertisementRoutes");
const contactFormRoutes = require("./routes/api/contactFormRoutes");
const sendingMailsRoutes = require("./routes/api/sendingMailsRoutes");
const reportRoutes = require("./routes/api/reportRoutes");
const ratingRoutes = require("./routes/api/ratingRoutes");
const commentRoutes = require("./routes/api/commentRoutes");
const productLikeRoutes = require("./routes/api/productLikeRoutes");

// use middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use(cookieParser());

// routes middleware
app.use("/api", authRoutes);
app.use("/api", userRoutes);
app.use("/api", categoryRoutes);
app.use("/api", fabricRoutes);
app.use("/api", productRoutes);
app.use("/api", deliveryFeeRoutes);
app.use("/api", braintreeRoutes);
app.use("/api", orderRoutes);
app.use("/api", testimonialRoutes);
app.use("/api", careerRoutes);
app.use("/api", advertisementRoutes);
app.use("/api", contactFormRoutes);
app.use("/api", sendingMailsRoutes);
app.use("/api", reportRoutes);
app.use("/api", ratingRoutes);
app.use("/api", commentRoutes);
app.use("/api", productLikeRoutes);

// server static assets if in production
if (process.env.NODE_ENV === "production") {
  // set static folder
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "client", "build", "index.html"));
  });
}

// port
const PORT = process.env.PORT || 5000;

// run server
app.listen(PORT, () => console.log(`server is running in Port ${PORT}`));

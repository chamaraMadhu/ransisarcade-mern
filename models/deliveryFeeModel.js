const mongoose = require("mongoose");

const deliveryFeeSchema = mongoose.Schema({
  city: { type: String },
  fee: { type: Number },
  belongsToColombo: { type: Number }
});

module.exports = mongoose.model("DeliveryFee", deliveryFeeSchema);

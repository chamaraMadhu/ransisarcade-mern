const mongoose = require("mongoose");

const testimonialSchema = mongoose.Schema(
  {
    name: { type: String },
    image: { data: Buffer, contentType: String },
    testimonial: { type: String }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Testimonial", testimonialSchema);

const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const productSchema = mongoose.Schema(
  {
    // _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, trim: true },
    category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
    fabric: { type: mongoose.Schema.Types.ObjectId, ref: "Fabric" },
    color: { type: String, trim: true },
    oldPrice: { type: Number, trim: true },
    newPrice: { type: Number, trim: true },
    cost: { type: Number, trim: true },
    quantity: { type: Number, trim: true },
    sold: { type: Number, default: 0 },
    likes: { type: Number, default: 0 },
    rating: { type: Number, default: 0 },
    comments: { type: Number, default: 0 },
    occasionCasual: { type: Boolean },
    occasionParty: { type: Boolean },
    occasionOffice: { type: Boolean },
    occasionCocktail: { type: Boolean },
    occasionWedding: { type: Boolean },
    description: { type: String, trim: true },
    imageFront: { data: Buffer, contentType: String },
    imageJacket: { data: Buffer, contentType: String },
    imageBorder: { data: Buffer, contentType: String },
    imageBack: { data: Buffer, contentType: String },
    washAndCare: { type: String },
    // status: { type: Boolean },
    keywords: { type: String, trim: true }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Product", productSchema);

const mongoose = require("mongoose");

const advertisementSchema = mongoose.Schema(
  {
    purpose: { type: String },
    image: { data: Buffer, contentType: String },
    start: { type: String },
    end: { type: String },
    status: { type: Boolean }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Advertisement", advertisementSchema);

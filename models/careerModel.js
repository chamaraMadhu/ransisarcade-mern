const mongoose = require("mongoose");

const careerSchema = mongoose.Schema(
  {
    jobType: { type: String },
    position: { type: String },
    closingDate: { type: String },
    image: { data: Buffer, contentType: String },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Career", careerSchema);

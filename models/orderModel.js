const mongoose = require("mongoose");
// const Schema = mongoose.Schema;
const { ObjectId } = mongoose.Schema;

const CartItemSchema = new mongoose.Schema(
  {
    productId: { type: String, ref: "Product" },
    name: String,
    newPrice: Number,
    cost: Number,
    category: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
    fabric: { type: mongoose.Schema.Types.ObjectId, ref: "Fabric" },
    color: String,
    quantity: Number
  },
  { timestamps: true }
);

const CartItem = mongoose.model("CartItem", CartItemSchema);

const OrderSchema = new mongoose.Schema(
  {
    orderItems: [CartItemSchema],
    transactionId: {},
    amount: { type: Number },
    transactionTime: String,
    title: String,
    recipientsName: String,
    phone: String,
    address: String,
    deliveryFee: String,
    locationType: String,
    deliveryInstructions: String,
    status: {
      type: String,
      default: "Not processed",
      enum: ["Not processed", "Processing", "Delivered", "Cancelled"] // enum means string objects
    },
    updates: Date,
    user: { type: ObjectId, ref: "User" },
    isOnlinePayment: { type: Boolean }
  },
  { timestamps: true }
);

const Order = mongoose.model("OrderItems", OrderSchema);

module.exports = { Order, CartItem };

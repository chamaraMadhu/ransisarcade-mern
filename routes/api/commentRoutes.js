const express = require("express");
const router = express.Router();

// // load product model
const Product = require("../../models/productModel");

// load user model
const User = require("../../models/userModel");

// load like model
const Comment = require("../../models/commentModel");

// user identification
// router.param("userId", (req, res, next, id) => {
//   User.findById(id).exec((err, user) => {
//     if (err) {
//       res.status(400).json({ error: "user not fount" });
//     }
//     req.profile = user;
//     next();
//   });
// });

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// add comment
router.post(
  "/product/add/comment",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    comment = new Comment(req.body);
    comment
      .save()
      .then(result => {
        Product.findOne({ _id: req.body.productId })
          .select("-imageFront -imageJacket -imageBorder -imageBack")
          .then(product => {
            product.comments = product.comments + 1;
            product
              .save()
              .then(result => {
                res.status(200).json(result);
              })
              .catch(err => {
                res.status(400).json(err);
              });
          });
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

// get all comments
router.get(
  "/products/on/comments",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Comment.find()
      .populate("productId", "name")
      .populate("userId", "fName lName image")
      .sort([["createdAt", "desc"]])
      .then(result => {
        res.status(200).json(result);
      });
  }
);

// get particular product's comments
router.get(
  "/product/comments/:id",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Comment.find({ productId: req.params.id })
      .populate("userId", "fName lName image")
      .then(result => {
        res.status(200).json(result);
      });
  }
);

module.exports = router;

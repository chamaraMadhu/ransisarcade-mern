const express = require("express");
const router = express.Router();
const braintree = require("braintree");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const { requireLogin, isAuth } = require("../../auth-middleware/check-auth");

// load braintree variables from config file
const {
  braintreeMerchantId,
  braintreePublicKey,
  braintreePrivateKey
} = require("../../config/keys");

// load checkout validator
const { checkoutValidator } = require("../../validator/checkoutValidator");

// connect braintree to the API
const gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: braintreeMerchantId,
  publicKey: braintreePublicKey,
  privateKey: braintreePrivateKey
});

// generate token
router.get("/braintree/getToken/:userId", requireLogin, isAuth, (req, res) => {
  // generate client token
  gateway.clientToken.generate({}, (err, response) => {
    if (err) {
      res.status(500).json(err);
    } else {
      res.status(200).json(response);
    }
  });
});

router.post(
  "/braintree/deliveryInfo/:userId",
  requireLogin,
  isAuth,
  (req, res) => {
    const { errors, isValid } = checkoutValidator(req.body);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      res.status(200).json({ deliveryFee: req.body.city });
    }
  }
);

router.post("/braintree/payment/:userId", requireLogin, isAuth, (req, res) => {
  let nonceFromTheClient = req.body.paymentMethodNonce;
  let amountFromTheClient = req.body.amountFromTheClient;

  // charge
  let newTransaction = gateway.transaction.sale(
    {
      amount: amountFromTheClient,
      paymentMethodNonce: nonceFromTheClient,
      options: {
        submitForSettlement: true
      }
    },
    (err, result) => {
      if (err) {
        res.status(500).json(err);
      } else {
        res.status(200).json(result);
      }
    }
  );
});

module.exports = router;

const express = require("express");
const router = express.Router();
const formidable = require("formidable");
const fs = require("fs");

// load fabric model
const Advertisement = require("../../models/advertisementModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// load fabric validator
const {
  advertisementValidator
} = require("../../validator/advertisementValidator");
// const { fabricEditValidator } = require("../../validator/fabricValidator");

// add fabric
router.post(
  "/advertisement/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res) => {
    const form = new formidable.IncomingForm();
    form.keepExtensions = true;
    form.parse(req, (err, fields, files) => {
      const { errors, isValid } = advertisementValidator(fields);

      if (!files.image) {
        errors.image = "Advertisement image is required";
        return res.status(400).json(errors);
      }

      // check error freenas
      if (!isValid) {
        res.status(400).json(errors);
      } else {
        //check fabric ID existance
        const advertisement = new Advertisement(fields);

        if (files.image) {
          advertisement.image.data = fs.readFileSync(files.image.path);
          advertisement.image.contentType = files.image.type;
        }

        advertisement
          .save()
          .then(result => {
            res.status(200).json(result);
          })
          .catch(err => {
            errors.fail = "Advertisement is not added successfully";
            res.status(400).json(errors);
          });
      }
    });
  }
);

// update career
router.put("/advertisement/:id/:userId", requireLogin, isAuth, isAdmin, (req, res) => {
  const form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    const { errors, isValid } = advertisementValidator(fields);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      //check advertisement ID existance
      Advertisement.findOne({ _id: req.params.id })
        .then(advertisement => {
          advertisement.purpose = fields.purpose;
          advertisement.start = fields.start;
          advertisement.end = fields.end;
          advertisement.status = fields.status;

          if (files.image) {
            advertisement.image.data = fs.readFileSync(files.image.path);
            advertisement.image.contentType = files.image.type;
          }

          advertisement
            .save()
            .then(result => {
              res.status(200).json(result);
            })
            .catch(err => {
              errors.fail = "Career is not added successfully";
              res.status(400).json(errors);
            });
        })
        .catch(err => {
          res.status(400).json({ error: err });
        });
    }
  });
});

// get all advertisements
router.get("/advertisements", (req, res) => {
  Advertisement.find()
    .select("-image")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No advertisement" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get  spwcific advertisement
router.get("/advertisement/:id", (req, res) => {
  Advertisement.findOne({ _id: req.params.id })
    .select("-image")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No career" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// count all advertisements
router.get("/advertisements/count", (req, res) => {
  Advertisement.find()
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// delete particular testimonial
router.delete(
  "/advertisement/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    Advertisement.deleteOne({ _id: req.params.id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  }
);

// get photos
router.get("/advertisement/photo/:id", (req, res) => {
  Advertisement.findOne({ _id: req.params.id })
    .exec()
    .then(advertisement => {
      if (advertisement.image.data) {
        res.set("Content-Type", advertisement.image.contentType);
        return res.send(advertisement.image.data);
      }
    })
    .catch(err => {
      res.status(500).json({ error: "no advertisement" });
    });
});

module.exports = router;

const express = require("express");
const router = express.Router();

// load delivery fee model
const DeliveryFee = require("../../models/deliveryFeeModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// load fabric validator
const {
  deliveryFeeValidator
} = require("../../validator/deliveryFeeValidator");
// const { fabricEditValidator } = require("../../validator/fabricValidator");

// add fabric
router.post(
  "/deliveryFee/add/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res) => {
    const { errors, isValid } = deliveryFeeValidator(req.body);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      const deliveryFee = new DeliveryFee(req.body);

      deliveryFee
        .save()
        .then(result => {
          res.status(200).json(result);
        })
        .catch(err => {
          errors.fail = "Delivery fee is not added successfully!";
          res.status(400).json(errors);
        });
    }
  }
);

// get all delivery fees
router.get("/deliveryFees", (req, res) => {
  DeliveryFee.find()
    .sort("city")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No fabrics" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// count all cities
router.get("/islandwide/count", (req, res) => {
  DeliveryFee.find()
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// count all cities in Colombo
router.get("/colombo/count", (req, res) => {
  DeliveryFee.find({ belongsToColombo: 0 })
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get all delivery fees which belongs to Colombo district
router.get("/deliveryFees/colombo", (req, res) => {
  DeliveryFee.find({ belongsToColombo: 1 })
    .sort("city")
    .then(result => {
      if (result.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No fabrics" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get particular delivery fee
router.get("/deliveryFee/:id", (req, res, next) => {
  DeliveryFee.findOne({ _id: req.params.id })
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// update delivery fee
router.put(
  "/deliveryFee/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    const { errors, isValid } = deliveryFeeValidator(req.body);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      DeliveryFee.findOneAndUpdate({ _id: req.params.id }, req.body)
        .then(result => {
          res.status(200).json(result);
        })
        .catch(err => {
          res.status(400).json({ error: err });
        });
    }
  }
);

// delete particular delivery fee
router.delete(
  "/deliveryFee/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    DeliveryFee.deleteOne({ _id: req.params.id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  }
);

module.exports = router;

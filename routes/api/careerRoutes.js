const express = require("express");
const router = express.Router();
const formidable = require("formidable");
const fs = require("fs");

// load fabric model
const Career = require("../../models/careerModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// load career validator
const { careerValidator } = require("../../validator/careerValidator");
// const { fabricEditValidator } = require("../../validator/fabricValidator");

// add career
router.post("/career/:userId", requireLogin, isAuth, isAdmin, (req, res) => {
  const form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    const { errors, isValid } = careerValidator(fields);

    if (!files.image) {
      errors.image = "Advertisement image is required";
      return res.status(400).json(errors);
    }

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      //check career ID existance
      const career = new Career(fields);

      if (files.image) {
        career.image.data = fs.readFileSync(files.image.path);
        career.image.contentType = files.image.type;
      }

      career
        .save()
        .then(result => {
          res.status(200).json(result);
        })
        .catch(err => {
          errors.fail = "Career is not added successfully";
          res.status(400).json(errors);
        });
    }
  });
});

// update career
router.put("/career/:id/:userId", requireLogin, isAuth, isAdmin, (req, res) => {
  const form = new formidable.IncomingForm();
  form.keepExtensions = true;
  form.parse(req, (err, fields, files) => {
    const { errors, isValid } = careerValidator(fields);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      Career.findOne({ _id: req.params.id })
        .then(career => {
          //check career ID existance
          career.jobType = fields.jobType;
          career.position = fields.position;
          career.closingDate = fields.closingDate;

          if (files.image) {
            career.image.data = fs.readFileSync(files.image.path);
            career.image.contentType = files.image.type;
          }

          career
            .save()
            .then(result => {
              res.status(200).json(result);
            })
            .catch(err => {
              errors.fail = "Career is not added successfully";
              res.status(400).json(errors);
            });
        })
        .catch(err => {
          res.status(400).json({ error: err });
        });
    }
  });
});

// get alla spwcific careers
router.get("/career/:id", (req, res) => {
  Career.findOne({ _id: req.params.id })
    .select("-image")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No career" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get all careers
router.get("/careers", (req, res, next) => {
  Career.find()
    .select("-image")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No career" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// count all career
router.get("/careers/count", (req, res, next) => {
  Career.find()
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// delete particular career
router.delete(
  "/career/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    Career.deleteOne({ _id: req.params.id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  }
);

// get photos
router.get("/career/photo/:id", (req, res) => {
  Career.findOne({ _id: req.params.id })
    .exec()
    .then(career => {
      if (career.image.data) {
        res.set("Content-Type", career.image.contentType);
        return res.send(career.image.data);
      }
    })
    .catch(err => {
      res.status(500).json({ error: "no career" });
    });
});

module.exports = router;

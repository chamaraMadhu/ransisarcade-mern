const express = require("express");
const router = express.Router();

// load product model
const Product = require("../../models/productModel");

// load user model
const User = require("../../models/userModel");

// load like model
const Like = require("../../models/productLikeModel");

// user identification
// router.param("userId", (req, res, next, id) => {
//   User.findById(id).exec((err, user) => {
//     if (err) {
//       res.status(400).json({ error: "user not fount" });
//     }
//     req.profile = user;
//     next();
//   });
// });

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// add product
router.post(
  "/product/add/like/:id/:userId",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Like.find({ productId: req.params.id }).then(result => {
      if (result.length === 0) {
        const like = new Like({
          productId: req.params.id,
          userId: req.params.userId
        });

        like.save().then(
          Product.findOne({ _id: req.params.id }).then(product => {
            product.likes = product.likes + 1;

            product.save().then(result => {
              Product.find()
                .select("-imageFront -imageJacket -imageBorder -imageBack")
                .then(result => {
                  res.status(200).json(result);
                });
            });
          })
        );
      } else {
        var uniqueUser = false;

        // check user existance
        const checkUser = () => {
          for (let i = 0; i < result.length; i++) {
            if (result[i].userId !== req.params.userId) {
              return (uniqueUser = true);
            }
          }
        };

        // execute the function
        checkUser(() => {
          if (uniqueUser) {
            const like = new Like({
              _id: req.params.id,
              userId: req.params.userId
            });

            like.save().then(
              Product.findOne({ _id: req.params.id }).then(product => {
                product.likes = product.likes + 1;

                product.save().then(result => {
                  res.status(200).json({ message: "Thanks for like" });
                });
              })
            );
          } else {
            console.log("error");
            res.status(400).json({
              alreadyLikeError: "You have already Liked to this product"
            });
          }
        });
      }
    });
  }
);

// get likes
router.get(
  "/products/on/likes",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Like.find()
      .populate("productId", "name")
      .populate("userId", "fName lName image")
      .sort([["createdAt", "desc"]])
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

// get likes
router.get(
  "/product/likes/:id",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Like.find({ productId: req.params.id })
      .populate("userId", "fName lName image")
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

// check likes
router.get(
  "/product/check/like/:id/:userId",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Like.find({ productId: req.params.id }).then(result => {
      if (result.length === 0) {
        // res.status(200).json({like: "User didn't like yet"})
        return true;
      } else {
        var uniqueUser = true;

        // check user function
        const checkUser = () => {
          for (let i = 0; i < result.length; i++) {
            if (result[i].userId === req.params.userId) {
              return (uniqueUser = false);
            }
          }
        };

        // execute the function
        checkUser();

        if (uniqueUser) {
          // res.status(200).json({like: "User didn't like yet"})
          return true;
        } else {
          // return res.status(200).json({liked: "User already like it"})
          return false;
        }
      }
    });
  }
);

module.exports = router;

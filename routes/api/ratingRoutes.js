const express = require("express");
const router = express.Router();

// // load product model
const Product = require("../../models/productModel");

// load user model
const User = require("../../models/userModel");

// load like model
const Rating = require("../../models/ratingModel");

// user identification
// router.param("userId", (req, res, next, id) => {
//   User.findById(id).exec((err, user) => {
//     if (err) {
//       res.status(400).json({ error: "user not fount" });
//     }
//     req.profile = user;
//     next();
//   });
// });

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// add review
router.post(
  "/product/add/rating",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    rating = new Rating(req.body);
    rating
      .save()
      .then(result => {
        Product.findOne({ _id: req.body.productId })
          .select("-imageFront -imageJacket -imageBorder -imageBack")
          .then(product => {
            let rate = product.rating;

            if (rate === 0) {
              product.rating = req.body.rating;
            } else {
              product.rating = (rate + req.body.rating) / 2;
            }

            product
              .save()
              .then(result => {
                res.status(200).json(result.rating);
              })
              .catch(err => {
                res.status(400).json(err);
              });
          });
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

// get reviews
router.get(
  "/products/reviews",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Rating.find()
      .populate("productId", "name")
      .populate("userId", "fName lName image")
      .sort([["createdAt", "desc"]])
      .then(result => {
        if (result.length > 0) {
          res.status(200).json(result);
        }
      });
  }
);

// get reviews
router.get(
  "/product/ratings/:id",
  //   requireLogin,
  //   isAuth,
  //   isAdmin,
  (req, res) => {
    Rating.find({ productId: req.params.id })
      .populate("userId", "fName lName image")
      .then(result => {
        if (result) {
          res.status(200).json(result);
        }
      });
  }
);

module.exports = router;

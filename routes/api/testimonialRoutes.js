const express = require("express");
const router = express.Router();
const formidable = require("formidable");
const fs = require("fs");

// load fabric model
const Testimonial = require("../../models/testimonialModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// load fabric validator
const {
  testimonialValidator
} = require("../../validator/testimonialValidator");

// add testimonials
router.post(
  "/testimonial/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res) => {
    const form = new formidable.IncomingForm();
    form.keepExtensions = true;
    form.parse(req, (err, fields, files) => {
      const { errors, isValid } = testimonialValidator(fields);

      if (!files.image) {
        errors.image = "Customer image is required";
        return res.status(400).json(errors);
      }

      // check error freenas
      if (!isValid) {
        res.status(400).json(errors);
      } else {
        //check fabric ID existance
        const testimonial = new Testimonial(fields);

        if (files.image) {
          testimonial.image.data = fs.readFileSync(files.image.path);
          testimonial.image.contentType = files.image.type;
        }

        testimonial
          .save()
          .then(result => {
            res.status(200).json(result);
          })
          .catch(err => {
            errors.fail = "Testimonial is not added successfully";
            res.status(400).json(errors);
          });
      }
    });
  }
);

// update testimonials
router.put(
  "/testimonial/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res) => {
    const form = new formidable.IncomingForm();
    form.keepExtensions = true;
    form.parse(req, (err, fields, files) => {
      const { errors, isValid } = testimonialValidator(fields);

      // check error freenas
      if (!isValid) {
        res.status(400).json(errors);
      } else {
        Testimonial.findOne({ _id: req.params.id })
          .exec()
          .then(testimonial => {
            //check fabric ID existance
            testimonial.name = fields.name;
            testimonial.testimonial = fields.testimonial;

            if (files.image) {
              testimonial.image.data = fs.readFileSync(files.image.path);
              testimonial.image.contentType = files.image.type;
            }

            testimonial
              .save()
              .then(result => {
                res.status(200).json(result);
              })
              .catch(err => {
                errors.fail = "Testimonial is not updated successfully";
                res.status(400).json(errors);
              });
          })
          .catch(err => {
            res.status(400).json({ error: err });
          });
      }
    });
  }
);

// get a specific testimonials
router.get("/testimonial/:id", (req, res) => {
  Testimonial.findOne({ _id: req.params.id })
    .select("-image")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No testimonial" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get all testimonials
router.get("/testimonials", (req, res) => {
  Testimonial.find()
    .select("-image")
    .then(result => {
      console.log(result);
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No testimonial" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get 3 random testimonials for home page
router.get("/testimonials/random", (req, res) => {
  Testimonial.aggregate([{ $sample: { size: 3 } }])
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No testimonial" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// count all testimonials
router.get("/testimonials/count", (req, res) => {
  Testimonial.find()
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// delete particular testimonial
router.delete(
  "/testimonial/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    Testimonial.deleteOne({ _id: req.params.id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  }
);

// get photos
router.get("/testimonial/photo/:id", (req, res) => {
  Testimonial.findOne({ _id: req.params.id })
    .exec()
    .then(testimonial => {
      if (testimonial.image.data) {
        res.set("Content-Type", testimonial.image.contentType);
        return res.send(testimonial.image.data);
      }
    })
    .catch(err => {
      res.status(500).json({ error: "no testimonial" });
    });
});

module.exports = router;

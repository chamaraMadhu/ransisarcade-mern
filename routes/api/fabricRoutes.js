const express = require("express");
const router = express.Router();
const formidable = require("formidable");
const fs = require("fs");

// load fabric model
const Fabric = require("../../models/fabricModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// load fabric validator
const { fabricValidator } = require("../../validator/fabricValidator");

// add fabric
router.post(
  "/fabric/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    const { errors, isValid } = fabricValidator(req.body);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      // check fabric name existance
      Fabric.findOne({ name: req.body.name })
        .exec()
        .then(fabricName => {
          if (fabricName) {
            errors.name = "Fabric name is already exists";
            return res.status(400).json(errors);
          } else {
            const fabric = new Fabric(req.body);

            fabric
              .save()
              .then(result => {
                res.status(200).json(result);
              })
              .catch(err => {
                errors.fail = "Fabric is not added successfully";
                res.status(400).json(errors);
              });
          }
        })
        .catch(err => {
          res.status(400).json({ error: err });
        });
    }
  }
);

// get all fabrics
router.get("/fabrics", (req, res, next) => {
  Fabric.find()
    .select("-image")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No fabrics" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get particular fabric
router.get("/fabric/:id", (req, res, next) => {
  Fabric.findOne({ _id: req.params.id })
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// update fabric
router.put(
  "/fabric/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    const { errors, isValid } = fabricValidator(req.body);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      Fabric.findOne({ _id: req.params.id }).then(fabric => {
        fabric.name = req.body.name;

        fabric
          .save()
          .then(result => {
            res.status(200).json(result);
          })
          .catch(err => {
            errors.fail = "fabric is not edited successfully";
            res.status(400).json(errors);
          });
      });
    }
  }
);

// delete particular fabric
router.delete(
  "/fabric/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    Fabric.deleteOne({ _id: req.params.id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  }
);

module.exports = router;

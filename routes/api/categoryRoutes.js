const express = require("express");
const router = express.Router();
const formidable = require("formidable");
const _ = require("lodash");
const fs = require("fs");

// load category model
const Category = require("../../models/categoryModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// load category validator
const { categoryValidator } = require("../../validator/categoryValidator");

// add category
router.post(
  "/category/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    const { errors, isValid } = categoryValidator(req.body);

    // check data validity
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      // check category name existance
      Category.findOne({ name: req.body.name })
        .exec()
        .then(categoryName => {
          if (categoryName) {
            errors.name = "Category name is already exists";
            return res.status(400).json(errors);
          } else {
            const category = new Category({
              name: req.body.name
            });
            category
              .save()
              .then(result => {
                res.status(200).json(result);
              })
              .catch(err => {
                errors.fail = "Category is not added successfully";
                res.status(400).json(errors);
                console.log(err);
              });
          }
        })
        .catch(err => {
          res.status(400).json({ error: err });
        });
    }
  }
);

// check category ID existance
// Category.findOne({ categoryId: req.body.categoryId })
//   .exec()
//   .then(categoryId => {
//     if (categoryId) {
//       errors.categoryId = "Category ID is already exists";
//       return res.status(400).json(errors);
//     } else {
//       // check category name existance
//       Category.findOne({ name: req.body.name })
//         .exec()
//         .then(categoryName => {
//           if (categoryName) {
//             errors.name = "Category name is already exists";
//             return res.status(400).json(errors);
//           } else {
//             const form = new formidable.IncomingForm();
//             form.keepExtensions = true;
//             form.parse(req, (err, fields, files) => {
//               if (err) {
//                 res.status(400).json({ error: "image was not uploaded" });
//               }
//               const category = new Category(fields);
//               if (files.image) {
//                 category.image.data = fs.readFileSync(files.image.path);
//                 category.image.contentType = files.image.type;
//               }
//               category
//                 .save()
//                 .then(result => {
//                   res.status(200).json({ message: result });
//                 })
//                 .catch(err => {
//                   res.status(400).json({ error: err });
//                 });
//             });
//           }
//         })
//         .catch(err => {
//           res.status(400).json({ error: err });
//         });
//     }
//   })
//   .catch(err => {
//     res.status(400).json({ error: err });
//   });

// router.post("/category", (req, res, next) => {
//   const category = new Category(req.body);
//   category
//     .save()
//     .then(result => {
//       res.status(200).json({ message: result });
//     })
//     .catch(err => {
//       res.status(400).json({ error: err });
//     });
// });

// get all category
router.get("/categories", (req, res, next) => {
  Category.find()
    .then(result => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.status(400).json({ msg: "No category" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// get specific category
router.get("/category/:id", (req, res, next) => {
  Category.findOne({ _id: req.params.id })

    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
});

// update category
router.put(
  "/category/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    const { errors, isValid } = categoryValidator(req.body);

    // check error freenas
    if (!isValid) {
      res.status(400).json(errors);
    } else {
      Category.findOne({ _id: req.params.id }).then(category => {
        category.name = req.body.name;

        category
          .save()
          .then(result => {
            res.status(200).json(result);
          })
          .catch(err => {
            errors.fail = "Category is not edited successfully";
            res.status(400).json(errors);
          });
      });
    }
  }
);

// delete paticular category
router.delete(
  "/category/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res, next) => {
    Category.deleteOne({ _id: req.params.id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json({ error: err });
      });
  }
);

module.exports = router;

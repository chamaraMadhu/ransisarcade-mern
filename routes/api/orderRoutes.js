const express = require("express");
const router = express.Router();

// load fabric model
const { Order, CartItem } = require("../../models/orderModel");

// load user model
const Product = require("../../models/productModel");

// load user model
const User = require("../../models/userModel");

// user identification
router.param("userId", (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err) {
      res.status(400).json({ error: "user not fount" });
    }
    req.profile = user;
    next();
  });
});

// load auth-middleware
const {
  requireLogin,
  isAuth,
  isAdmin
} = require("../../auth-middleware/check-auth");

// // add order to  user history
// const addOrderToUserHistory = (req, res, next) => {
//   let history = [];

//   req.body.orderItems.forEach(item => {
//     history.push({
//       productId: item.productId,
//       name: item.name,
//       category: item.category,
//       fabric: item.fabric,
//       color: item.color,
//       newPrice: item.newPrice,
//       quantity: item.count,
//       description: item.description,
//       transactionId: req.body.transactionId,
//       amount: req.body.amount,
//       transactionTime: req.body.transactionTime
//     });
//   });

//   User.findOneAndUpdate(
//     { _id: req.profile._id },
//     { $push: { history: history } },
//     { new: true }
//   )
//     .then()
//     .catch(err => {
//       res.status(400).json({ error: "could not update purchase history" });
//     });

//   next();
// };

// decrease available quanity and increase sold quantity
const decreaseQtyIncreaseSold = (req, res, next) => {
  let bulkOps = req.body.orderItems.map(item => {
    return {
      updateOne: {
        filter: { _id: item._id },
        update: { $inc: { quantity: -item.quantity, sold: +item.quantity } }
      }
    };
  });

  Product.bulkWrite(bulkOps, {}, (err, products) => {
    if (err) {
      res.status(400).json({ error: "Could not update product" });
    }
  });

  next();
};

// add order
router.post(
  "/order/add/:userId",
  requireLogin,
  isAuth,
  decreaseQtyIncreaseSold,
  (req, res) => {
    req.body.user = req.profile;

    const order = new Order(req.body);

    order
      .save()
      .then(data => {
        res.status(200).json(data);
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

// get all order for admin - online
router.get("/orders/online/:userId", requireLogin, isAuth, isAdmin, (req, res) => {
  Order.find({isOnlinePayment: true})
    .sort("-createdAt")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      }
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

// get all order for admin - cash on delivery
router.get("/orders/cash-on-delivery/:userId", requireLogin, isAuth, isAdmin, (req, res) => {
  Order.find({isOnlinePayment: false})
    .sort("-createdAt")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      }
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

// get all my orders - online payment
router.get("/myOrders/online/:userId", requireLogin, isAuth, (req, res) => {
  Order.find({ user: req.params.userId, isOnlinePayment: true})
    .sort("-createdAt")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      }
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

// get all my orders - cash on delivery
router.get("/myOrders/cash-on-delivery/:userId", requireLogin, isAuth, (req, res) => {
  Order.find({ user: req.params.userId, isOnlinePayment: false })
    .sort("-createdAt")
    .then(result => {
      if (result) {
        res.status(200).json(result);
      }
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

// update order status
router.put(
  "/order/change/status/:id/:userId",
  requireLogin,
  isAuth,
  isAdmin,
  (req, res) => {
    Order.findOneAndUpdate({ _id: req.params.id }, req.body)
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }
);

// get all not processed
router.get("/notProcessed/count", (req, res) => {
  Order.find({ status: "Not processed" })
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

// get all processing
router.get("/processing/count", (req, res) => {
  Order.find({ status: "Processing" })
    .countDocuments()
    .then(result => {
      res.status(200).json(result);
    })
    .catch(err => {
      res.status(400).json(err);
    });
});

module.exports = router;

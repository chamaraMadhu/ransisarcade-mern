const validator = require("validator");

exports.categoryValidator = data => {
  let errors = {};

  if (validator.isEmpty(data.name, { ignore_whitespace: true })) {
    errors.name = "Category name is required";
  } else if (!validator.matches(data.name, /^[a-zA-Z\s]*$/)) {
    errors.name = "Category name must contain only letters and spaces";
  }

  let isValid = null;

  if (
    errors === undefined ||
    errors === null ||
    (typeof errors === "object" && Object.keys(errors).length === 0) ||
    (typeof errors === "string" && errors.trim().length === 0)
  ) {
    isValid = true;
  } else {
    isValid = false;
  }

  return {
    errors: errors,
    isValid: isValid
  };
};

const validator = require("validator");

exports.deliveryFeeValidator = data => {
  let errors = {};

  if (validator.isEmpty(data.city, { ignore_whitespace: true })) {
    errors.city = "Delivery city is required";
  }

  if (validator.isEmpty(data.fee, { ignore_whitespace: true })) {
    errors.fee = "Delivery fee is required";
  }

  let isValid = null;

  if (
    errors === undefined ||
    errors === null ||
    (typeof errors === "object" && Object.keys(errors).length === 0) ||
    (typeof errors === "string" && errors.trim().length === 0)
  ) {
    isValid = true;
  } else {
    isValid = false;
  }

  return {
    errors: errors,
    isValid: isValid
  };
};

// exports.fabricEditValidator = data => {
//   let errors = {};

//   if (validator.isEmpty(data.name, { ignore_whitespace: true })) {
//     errors.name = "Fabric name is required";
//   } else if (!validator.matches(data.name, /^[a-zA-Z\s]*$/)) {
//     errors.name = "Fabric name must contain only letters and spaces";
//   }

//   let isValid = null;

//   if (
//     errors === undefined ||
//     errors === null ||
//     (typeof errors === "object" && Object.keys(errors).length === 0) ||
//     (typeof errors === "string" && errors.trim().length === 0)
//   ) {
//     isValid = true;
//   } else {
//     isValid = false;
//   }

//   return {
//     errors: errors,
//     isValid: isValid
//   };
// };

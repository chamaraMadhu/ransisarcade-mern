const validator = require("validator");

exports.checkoutValidator = data => {
  let errors = {};

  if (validator.isEmpty(data.title)) {
    errors.title = "Title is required";
  }

  if (validator.isEmpty(data.recipientsName, { ignore_whitespace: true })) {
    errors.recipientsName = "Recipient's name is required";
  } else if (!validator.matches(data.recipientsName, /^[a-zA-Z\s]*$/)) {
    errors.recipientsName =
      "recipient's name must contain letters and spaces only";
  }

  if (validator.isEmpty(data.phone, { ignore_whitespace: true })) {
    errors.phone = "Phone is required";
  } else if (!validator.isMobilePhone(data.phone, ["sk-SK", "sr-RS"])) {
    errors.phone = "Phone number is invalid";
  }

  if (validator.isEmpty(data.address, { ignore_whitespace: true })) {
    errors.address = "Delivery address is required";
  }

  if (validator.isEmpty(data.city)) {
    errors.city = "Delivery city is required";
  }

  if (validator.isEmpty(data.locationType)) {
    errors.locationType = "Location type is required";
  }

  let isValid = null;

  if (
    errors === undefined ||
    errors === null ||
    (typeof errors === "object" && Object.keys(errors).length === 0) ||
    (typeof errors === "string" && errors.trim().length === 0)
  ) {
    isValid = true;
  } else {
    isValid = false;
  }

  return {
    errors: errors,
    isValid: isValid
  };
};

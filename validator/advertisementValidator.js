const validator = require("validator");

exports.advertisementValidator = data => {
  let errors = {};

  if (validator.isEmpty(data.purpose, { ignore_whitespace: true })) {
    errors.purpose = "Purpose is required";
  }

  if (validator.isEmpty(data.start, { ignore_whitespace: true })) {
    errors.start = "Advertisement start is required";
  }

  if (validator.isEmpty(data.end, { ignore_whitespace: true })) {
    errors.end = "Advertisement end is required";
  }

  let isValid = null;

  if (
    errors === undefined ||
    errors === null ||
    (typeof errors === "object" && Object.keys(errors).length === 0) ||
    (typeof errors === "string" && errors.trim().length === 0)
  ) {
    isValid = true;
  } else {
    isValid = false;
  }

  return {
    errors: errors,
    isValid: isValid
  };
};


exports.fabricEditValidator = data => {
  let errors = {};

  if (validator.isEmpty(data.name, { ignore_whitespace: true })) {
    errors.name = "Fabric name is required";
  } else if (!validator.matches(data.name, /^[a-zA-Z\s]*$/)) {
    errors.name = "Fabric name must contain only letters and spaces"; 
  }

  let isValid = null;

  if (
    errors === undefined ||
    errors === null ||
    (typeof errors === "object" && Object.keys(errors).length === 0) ||
    (typeof errors === "string" && errors.trim().length === 0)
  ) {
    isValid = true;
  } else {
    isValid = false;
  }

  return {
    errors: errors,
    isValid: isValid
  };
};
import React from "react";
import { Link } from "react-router-dom";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default function Sidebar() {
  // get authenticated user info
  const {
    user: { fName, lName, image, _id }
  } = isAuthenticated();

  return (
    <div className="admin-sidebar-col">
      <Link to={`/admin/profile/${_id}`} ><img src={image} className="profile-img-sidebar" alt={fName} /></Link>
      <p className="sidebar-user-name">
        {fName} {lName}
      </p>
      <div className="accordion" id="accordion">
        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseOne"
            aria-expanded="true"
            aria-controls="collapseOne"
          >
            <span className="card-title">&nbsp; Inventory</span>
          </div>

          <div id="collapseOne" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-product"
                className="list-group-item list-group-item-action justify-content-between py-3"
              >
                <i className="fas fa-plus"></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-product"
                className="list-group-item list-group-item-action justify-content-between py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Edit/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapse13"
            aria-expanded="true"
            aria-controls="collapse13"
          >
            <span className="card-title">&nbsp; Sold Products</span>
          </div>

          <div id="collapse13" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/view-sold-product"
                className="list-group-item list-group-item-action justify-content-between py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseTwo"
            aria-expanded="true"
            aria-controls="collapseTwo"
          >
            <span className="card-title">&nbsp; Category</span>
          </div>

          <div id="collapseTwo" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-category"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-plus "></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-category"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Edit/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseThree"
            aria-expanded="true"
            aria-controls="collapseTwo"
          >
            <span className="card-title">&nbsp; Fabric</span>
          </div>

          <div id="collapseThree" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-fabric"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-plus "></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-fabric"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Edit/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseFour"
            aria-expanded="true"
            aria-controls="collapseFour"
          >
            <span className="card-title">&nbsp; Customer Orders</span>
          </div>

          <div id="collapseFour" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/customer-orders/online"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; Online payments
              </Link>
              <Link
                to="/admin/customer-orders/cash-on-delivery"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; Cash On delivery
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseFive"
            aria-expanded="true"
            aria-controls="collapseFive"
          >
            <span className="card-title">&nbsp; Delivery Fees</span>
          </div>

          <div id="collapseFive" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-delivery-fee"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-plus "></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-delivery-fee"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Edit/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseSix"
            aria-expanded="true"
            aria-controls="collapseSix"
          >
            <span className="card-title">&nbsp; Testimonial</span>
          </div>

          <div id="collapseSix" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-testimonial"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-plus "></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-testimonial"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Edit/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseSeven"
            aria-expanded="true"
            aria-controls="collapseSeven"
          >
            <span className="card-title">&nbsp; Career</span>
          </div>

          <div id="collapseSeven" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-career"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-plus "></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-career"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Edit/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseEight"
            aria-expanded="true"
            aria-controls="collapseEight"
          >
            <span className="card-title">&nbsp; Advertisement</span>
          </div>

          <div id="collapseEight" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/add-advertisement"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-plus "></i> &nbsp; Add
              </Link>
              <Link
                to="/admin/view-advertisement"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View/Delete
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseNine"
            aria-expanded="true"
            aria-controls="collapseNine"
          >
            <span className="card-title">&nbsp; Likes</span>
          </div>

          <div id="collapseNine" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/view-likes"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View Likes
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapseTen"
            aria-expanded="true"
            aria-controls="collapseTen"
          >
            <span className="card-title">&nbsp; Comments</span>
          </div>

          <div id="collapseTen" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/view-comments"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View Comments
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapse11"
            aria-expanded="true"
            aria-controls="collapse11"
          >
            <span className="card-title">&nbsp; Reviews</span>
          </div>

          <div id="collapse11" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/view-reviews"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-pencil-alt"></i> &nbsp; View Reviews
              </Link>
            </div>
          </div>
        </div>

        <div className="card">
          <div
            className="card-header"
            data-toggle="collapse"
            data-target="#collapse12"
            aria-expanded="true"
            aria-controls="collapse12"
          >
            <span className="card-title">&nbsp; Reports</span>
          </div>

          <div id="collapse12" className="collapse" data-parent="#accordion">
            <div className="card-body p-0">
              <Link
                to="/admin/inventory-by-category"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Category wise
                Inventory
              </Link>
              <Link
                to="/admin/inventory-by-fabrics"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Fabric wise
                Inventory
              </Link>
              <Link
                to="/admin/inventory-by-price"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Price wise Inventory
              </Link>
              <Link
                to="/admin/color-wise-inventory"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Color wise Inventory
              </Link>
              <Link
                to="/admin/Like-wise-inventory"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Like wise Inventory
              </Link>
              <Link
                to="/admin/comment-wise-inventory"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Comment wise
                Inventory
              </Link>
              <Link
                to="/admin/rating-wise-inventory"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Rating wise
                Inventory
              </Link>
              <Link
                to="/admin/category-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Category wise Sold
                Product
              </Link>
              <Link
                to="/admin/fabric-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Fabric wise Sold
                Product
              </Link>
              <Link
                to="/admin/price-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Price wise Sold
                Product
              </Link>
              <Link
                to="/admin/color-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Color wise Sold
                Product
              </Link>
              <Link
                to="/admin/like-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Like wise Sold
                Product
              </Link>
              <Link
                to="/admin/comment-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Comment wise Sold
                Product
              </Link>
              <Link
                to="/admin/rating-wise-sold-products"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Rating wise Sold
                Product
              </Link>
              <Link
                to="/admin/sales-report"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Sales Report
              </Link>
              <Link
                to="/admin/inventory-report"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Inventory Report
              </Link>
              <Link
                to="/admin/sold-products-report"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Sold Products Report
              </Link>
              <Link
                to="/admin/customer-report"
                className="list-group-item list-group-item-action justify-content-between pl-5 py-3"
              >
                <i className="fas fa-chart-pie"></i> &nbsp; Customer Report
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

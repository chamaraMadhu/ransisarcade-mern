import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import logo from "../../images/ransis-arcade-logo.jpg";
import cartJPG from "../../images/cart.png";
import cartGIF from "../../images/cart.gif";
import wishlistGIF from "../../images/wishlist.gif";
import wishlistPNG from "../../images/heart.png";
import "../../css/client/NavbarStyle.css";
import axios from "axios";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

// load ItemsOfCart methord from cartMethods
import { itemsOfCart } from "../../components/client/CartMethods";
import { itemsOfwishlist } from "../../methods/wishlistMethods";

export default class Navbar extends Component {
  state = {
    logoutSuccessfully: false
  };

  handleLogout = () => {
    axios
      .get(`/api/logout`)
      .then(result => {
        if (result) {
          if (typeof window !== "undefined") {
            localStorage.removeItem("jwt");
          }
          this.setState({ logoutSuccessfully: true });
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    return (
      <div>
        {this.state.logoutSuccessfully === true ? (
          <Redirect to="/login" />
        ) : null}

        <div className="nav-top">
          <ul>
            {!isAuthenticated() ? (
              <li className="mt-1">
                <Link to="/login">
                  <i className="fas fa-sign-in-alt"></i> Login
                </Link>
              </li>
            ) : null}
            {!isAuthenticated() ? (
              <li className="mt-1">
                <Link to="/register" className="p-2">
                  <i className="fas fa-edit"></i> Register
                </Link>
              </li>
            ) : null}
            {isAuthenticated() && isAuthenticated().user.role === 0 ? (
              <div
                className="btn-group float-right mr-0 mr-md-3 mb-2"
                style={{ cursor: "pointer" }}
              >
                <span
                  className="dropdown-toggle text-light"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <img
                    className="rounded-circle"
                    src={isAuthenticated().user.image}
                    width="30"
                    height="30"
                    alt={isAuthenticated().user.fName}
                  />
                </span>
                <div
                  className="dropdown-menu dropdown-menu-right"
                  style={{ zIndex: 9999 }}
                >
                  <Link className="dropdown-item text-dark" to="/user-account">
                    My Account
                  </Link>
                  <Link className="dropdown-item text-dark" to="/my-orders/online">
                    My Orders - Online
                  </Link>
                  <Link className="dropdown-item text-dark" to="/my-orders/cash-on-delivery">
                    My Orders - Cash On Delivery
                  </Link>
                  <Link
                    className="dropdown-item text-dark"
                    to="#"
                    onClick={this.handleLogout}
                  >
                    Logout
                  </Link>
                </div>
              </div>
            ) : null}
            {isAuthenticated() && isAuthenticated().user.role === 0 ? (
              <li className="nav-item mr-3 mt-1">
                <span className="text-light">
                  Hi {isAuthenticated().user.fName}
                </span>
              </li>
            ) : null}
          </ul>
        </div>

        <div className="sticky-top">
          <nav className="navbar navbar-expand navbar-light bg-white navbar-middle">
            <Link to="/">
              <img
                src={logo}
                width="100"
                height="60"
                id="com-logo-non-mobile"
                alt="ransis arcade logo"
                className="mr-3"
              />
            </Link>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto container-fluid">
                <li className="nav-item">
                  <Link className="nav-link" to="/">
                    Home
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/about">
                    About us
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/shop">
                    Shop
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/faq">
                    FAQ
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/career">
                    Careers
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/contact">
                    Contact us
                  </Link>
                </li>
              </ul>

              <ul className="navbar-nav ml-auto shopping-cart">
                <li className="nav-item">
                  <div className="row">
                    <div className="col">
                      <Link
                        className="nav-link"
                        to="/wishlist"
                        style={{ background: "none" }}
                      >
                        {itemsOfwishlist() > 0 ? (
                          <img
                            className="cart"
                            src={wishlistGIF}
                            alt="wishlist"
                          />
                        ) : (
                          <img
                            className="cart"
                            src={wishlistPNG}
                            alt="wishlist"
                          />
                        )}

                        <p
                          className="cart-badge"
                          style={{ margin: "-66px 0 0 31px" }}
                        >
                          {itemsOfwishlist()}
                        </p>
                      </Link>
                    </div>
                  </div>
                </li>
                <li className="nav-item">
                  <div className="row">
                    <div className="col">
                      <Link
                        className="nav-link"
                        to="/cart"
                        style={{ background: "none" }}
                      >
                        {itemsOfCart() > 0 ? (
                          <img
                            className="cart"
                            src={cartGIF}
                            alt="shopping cart"
                          />
                        ) : (
                          <img
                            className="cart"
                            src={cartJPG}
                            alt="shopping cart"
                          />
                        )}

                        <p
                          className="cart-badge"
                          style={{ margin: "-66px 0 0 31px" }}
                        >
                          {itemsOfCart()}
                        </p>
                      </Link>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </nav>

          <nav className="navbar navbar-expand-md navbar-light bg-white border-bottom navbar-bottom">
            <Link to="/">
              <img
                src={logo}
                width="90"
                height="50"
                id="com-logo-mobile"
                alt="ransis arcade logo"
              />
            </Link>
            <Link
              className="nav-link wishlist-mobile"
              to="/wishlist"
              style={{ background: "none" }}
            >
              {itemsOfwishlist() > 0 ? (
                <img className="cart" src={wishlistGIF} alt="wishlist" />
              ) : (
                <img className="cart" src={wishlistPNG} alt="wishlist" />
              )}

              <p className="cart-badge">{itemsOfwishlist()}</p>
            </Link>
            <Link
              className="nav-link shopping-cart-mobile"
              to="/cart"
              style={{ background: "none" }}
            >
              {itemsOfCart() > 0 ? (
                <img className="cart" src={cartGIF} alt="shopping cart" />
              ) : (
                <img className="cart" src={cartJPG} alt="shopping cart" />
              )}
              <p className="cart-badge">{itemsOfCart()}</p>
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
              style={{ border: "unset", padding: "0" }}
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto container-fluid">
                <li className="nav-item">
                  <Link className="nav-link" to="/">
                    Home
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/about">
                    About us
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/shop">
                    Shop
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/faq">
                    FAQ
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/career">
                    Careers
                  </Link>
                </li>

                <li className="nav-item">
                  <Link className="nav-link" to="/contact">
                    Contact us
                  </Link>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import { Link } from "react-router-dom";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class UserAccountSidebar extends Component {
  render() {
    // get authenticated user info
    const {
      user: { fName, image }
    } = isAuthenticated();

    return (
      <div className="side-bar">
        <div className="mt-4">
          <Link to="/user-account">
            <img
              className="rounded-circle d-block mx-auto"
              src={image}
              width="100px"
              height="100px"
              alt="profile"
            />
          </Link>
        </div>

        <h5 className="text-center text-light my-3">{fName}</h5>
        <div className="list-group mb-3">
          <Link to="/update-profile" className="card-title sidebar-link">
            Update Profile
          </Link>
          <Link to="/my-orders/online" className="card-title sidebar-link">
            My orders - Online
          </Link>
          <Link
            to="/my-orders/cash-on-delivery"
            className="card-title sidebar-link"
          >
            My orders - Cash On Delivery
          </Link>
          <Link to="/change-password" className="card-title sidebar-link">
            Change Password
          </Link>
        </div>
      </div>
    );
  }
}

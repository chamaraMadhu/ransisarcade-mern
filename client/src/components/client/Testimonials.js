import React, { useState, useEffect } from "react";
import axios from "axios";
import Rating from "react-rating";

export default function Testimonials() {
  const [testimonials, setTestimonials] = useState([]);

  useEffect(() => {
    axios.get(`/api/testimonials/random`).then(result => {
      setTestimonials(result.data);
    });
  }, []);

  return (
    <div className="container-fluid p-0 mt-5 testimonial-sec">
      <div className="shop-container p-0">
        {testimonials.length > 0 ? (
          <div className="row mx-0 mt-4">
            <h5 className="mb-4 mt-4 d-block mx-auto text-center">
              WHAT OUR CUSTOMERS THINK
            </h5>
          </div>
        ) : null}
        <div className="row m-0">
          {testimonials.map((testimonial, i) => (
            <div className="col-sm-6 col-md-4 mt-3" key={i}>
              <img
                className="rounded-circle testimonial-img"
                src={`/api/testimonial/photo/${testimonial._id}`}
                alt="testimonial.name"
              />
              <div className="mt-2">
                <p className="text-center mb-0">
                  <Rating
                    className="testimonial-ratings"
                    initialRating="5"
                    readonly
                    emptySymbol="far fa-star"
                    fullSymbol="fas fa-star"
                  />
                </p>

                <p className="text-center text-uppercase font-weight-bold py-2">
                  {testimonial.name}
                </p>
                <p className="text-center mt-3 mb-4 px-2">
                  <sup>
                    <i className="fas fa-quote-left"></i>
                  </sup>{" "}
                  {testimonial.testimonial}{" "}
                  <sup>
                    <i className="fas fa-quote-right"></i>
                  </sup>
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

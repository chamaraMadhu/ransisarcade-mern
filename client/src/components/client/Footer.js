import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../../css/client/FooterStyle.css";

export default class Footer extends Component {
  // scroll to top
  handleTopUp = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  render() {
    return (
      <React.Fragment>
        <div className="container-fluid footer-sec">
          <div className="row footer">
            <div className="col-sm-6 col-md-2 pt-4  text-center">
              <h5 className="mb-4 footer-sub-heading">Quick links</h5>
              <Link to="/">Home</Link>
              <br />
              <Link to="/about">About us</Link>
              <br />
              <Link to="/shop">Shop</Link>
              <br />
              <Link to="/career">Careers</Link>
              <br />
            </div>

            <div className="col-sm-6 col-md-3 pt-4 text-center">
              <h5 className="mb-4 footer-sub-heading">Support</h5>
              <Link to="/faq">FAQs</Link>
              <br />
              <Link to="/contact">Contact us</Link>
              <br />
              <Link to="/contact?map">Site map</Link>
              <br />
            </div>

            <div className="col-sm-6 col-md-2 pt-4 text-center">
              <h5 className="mb-4 footer-sub-heading">Legal</h5>
              <Link to="/">Payment methods</Link>
              <br />
              <Link to="/">Delivery method</Link>
              <br />
              <Link to="/">Return & exchange</Link>
              <br />
              <Link to="/">Terms & conditions</Link>
              <br />
              <Link to="/">Privacy policy</Link>
            </div>

            <div className="col-sm-6 col-md-2 pt-4 text-center">
              <h5 className="mb-4 footer-sub-heading">Follow Us</h5>
              <a
                href="https://www.facebook.com/Ransis-Arcade-1727332197501318/"
                target="_blank"
                className="follow-us"
                rel="noopener noreferrer"
              >
                <i className="fab fa-facebook"></i>
              </a>
            </div>

            <div className="col-md-3 pt-4">
              <h3 className="mb-4 text-center footer-sub-heading">
                Ransi's Arcade
              </h3>
              <table align="center" style={{ margin: "auto" }}>
                <tbody>
                  <tr>
                    <td>
                      <i className="fa fa-map-marker mr-4" />
                    </td>
                    <td>No. 72/2,</td>
                  </tr>

                  <tr>
                    <td />
                    <td>Buthgamuwa Road,</td>
                  </tr>

                  <tr>
                    <td />
                    <td>Rajagiriya.</td>
                  </tr>

                  <tr>
                    <td>
                      <i className="fa fa-envelope" />
                    </td>
                    <td>
                      <Link to="mailto:ransisarcade@gmail.com">
                        ransir@yahoo.com
                      </Link>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <i className="fa fa-phone  fa-rotate-90"></i>
                    </td>
                    <td>(+011) 2 793 889 (General line)</td>
                  </tr>

                  <tr>
                    <td>
                      <i className="fa fa-mobile-alt" />
                    </td>
                    <td>(+94) 727 352 576 (Sales)</td>
                  </tr>

                  <tr>
                    <td />
                    <td>(+94) 772 725 205</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <div className="row footer mb-0">
            <div className="col mt-3">
              <span
                className="text-center text-light"
                style={{
                  fontSize: "14px"
                }}
              >
                Designed and Developed by Chamara Madhushanka.
              </span>
              <span
                style={{
                  display: "block",
                  float: "right",
                  background: "#000",
                  padding: "7px 12px",
                  color: "#fff",
                  fontSize: "30px",
                  cursor: "pointer"
                }}
                onClick={this.handleTopUp}
              >
                <i className="fas fa-angle-up"></i>
              </span>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

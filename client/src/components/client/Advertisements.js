import React, { useState, useEffect } from "react";
import axios from "axios";
import moment from "moment";

import islandwideDelivery from "../../images/islandwide-delivery.jpg";
import cashOnDelivery from "../../images/cash-on-delivery.jpg";
import returnAndExchange from "../../images/return-and-exchange.jpg";
import giftVouchers from "../../images/gift-vouchers.jpg";
import securePayments from "../../images/secure-payments.jpg";
import PaymentMethods from "../../images/payment-methods.png";

export default function Advertisements() {
  const [advertisements, setadvertisements] = useState([]);

  useEffect(() => {
    axios.get(`/api/advertisements`).then(result => {
      setadvertisements(result.data);
    });
  }, []);

  return (
    <React.Fragment>
      {advertisements.map((advertisement, i) => (
        advertisement.status && advertisement.start <= moment().format("YYYY-MM-DD") && moment().format("YYYY-MM-DD") <= advertisement.end  ? (
        <div
          className="row m-0 advertisement-component"
          key={i}
          style={{ borderTop: "5px solid #401B33" }}
        >
          <div className="col p-0">
            <img
              src={`/api/advertisement/photo/${advertisement._id}`}
              className="w-100"
              alt={advertisement.purpose}
            />
          </div>
        </div>) : null
      ))}

      <div className="row m-0 advertisement-component">
        <div className="col p-0">
          <p className="advertisement-component-heading">
            Island Wide Delivery
          </p>
          <img
            src={islandwideDelivery}
            className="cash-on-delivery-img"
            alt="islandwide delivery"
          />
          <p className="advertisement-component-para">Delivery within 5 days</p>
        </div>
      </div>

      <div className="row m-0 advertisement-component">
        <div className="col p-0">
          <p className="advertisement-component-heading">Cash on Delivery</p>
          <img
            src={cashOnDelivery}
            className="cash-on-delivery-img"
            alt="cash on delivery"
          />
          <p className="advertisement-component-para">Delivery within 1 days</p>
        </div>
      </div>

      <div className="row m-0 advertisement-component">
        <div className="col p-0">
          <p className="advertisement-component-heading">Return and Exchange</p>
          <img
            src={returnAndExchange}
            className="cash-on-delivery-img"
            alt="gift vouchers"
          />
        </div>
      </div>

      <div className="row m-0 advertisement-component">
        <div className="col p-0">
          <p className="advertisement-component-heading">Gift Vouchers</p>
          <img
            src={giftVouchers}
            className="cash-on-delivery-img"
            alt="gift vouchers"
          />
        </div>
      </div>

      <div
        className="row m-0 advertisement-component"
        style={{ borderBottom: "5px solid #401B33" }}
      >
        <div className="col p-0">
          <p className="advertisement-component-heading">Secure Payments</p>
          <img
            src={securePayments}
            className="cash-on-delivery-img"
            alt="security payments"
          />
          <img
            src={PaymentMethods}
            style={{ width: "100%" }}
            alt="payment methods"
          />
        </div>
      </div>
    </React.Fragment>
  );
}

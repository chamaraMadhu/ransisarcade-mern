// add to cart
export const addToCart = (cartItem, next) => {
  // create empty aray variable called cart
  let cart = [];

  // check window is not undefined
  if (typeof window !== "undefined") {
    if (localStorage.getItem("cart")) {
      // get all cart items from local storage
      cart = JSON.parse(localStorage.getItem("cart"));
    }

    // add count into the cart
    cart.push({
      ...cartItem,
      count: 1
    });

    // remove duplicated cart items
    cart = Array.from(new Set(cart.map(item => item._id))).map(id => {
      return cart.find(item => item._id === id);
    });

    // store cart on local storage
    localStorage.setItem("cart", JSON.stringify(cart));
    next();
  }
};

// items Of Cart
export const itemsOfCart = () => {
  let cart = [];

  if (typeof window !== "undefined") {
    if (localStorage.getItem("cart")) {
      cart = JSON.parse(localStorage.getItem("cart"));

      // counts items
      return cart.length;
    } else {
      return 0;
    }
  }
};

// remove a cart item
export const removeCartItem = id => {
  // create empty aray variable called cart
  let cart = [];

  // check window is not undefined
  if (typeof window !== "undefined") {
    if (localStorage.getItem("cart")) {
      // get all cart items from local storage
      cart = JSON.parse(localStorage.getItem("cart"));
    }

    // remove from cart using splice method
    cart.map((item, i) => {
      if (item._id === id) {
        cart.splice(i, 1);
      }

      // store cart on local storage
      localStorage.setItem("cart", JSON.stringify(cart));
    });
  }
};

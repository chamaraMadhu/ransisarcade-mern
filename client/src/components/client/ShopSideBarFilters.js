import React, { Component } from "react";
import Rating from "react-rating";
import axios from "axios";

// import images
import like from "../../images/like.png";

export default class ShopSideBarFilters extends Component {
  state = {
    categories: [],
    fabrics: []
  };

  componentDidMount = () => {
    // get categories
    axios.get(`/api/categories`).then(categories => {
      this.setState({ categories: categories.data });
    });

    // get fabrics
    axios.get(`/api/fabrics`).then(fabrics => {
      this.setState({ fabrics: fabrics.data });
    });
  };

  render() {
    return (
      <div className="shop-side-bar">
        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Likes
          </h5>
          <div className="dropdown-divider mb-3"></div>

          <button
            href="#"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={this.props.likeFilter}
          >
            <span className="text-capitalize">
              <img
                src={like}
                className="rounded-circle"
                width="20"
                height="20"
                alt="like"
              />{" "}
              - like
            </span>
          </button>
        </div>

        <div className="dropdown-divider mb-3"></div>

        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Comments
          </h5>
          <div className="dropdown-divider mb-3"></div>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={this.props.commentFilter}
          >
            Comment
          </button>
        </div>

        <div className="dropdown-divider mb-3"></div>

        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Ratings
          </h5>
          <div className="dropdown-divider mb-3"></div>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center pb-0"
            onClick={() => this.props.ratingsFilter(0, 0)}
          >
            <Rating
              className="product-filter-ratings"
              initialRating="0"
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center pb-0"
            onClick={() => this.props.ratingsFilter(0, 1)}
          >
            <Rating
              className="product-filter-ratings"
              initialRating="1"
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center pb-0"
            onClick={() => this.props.ratingsFilter(1, 2)}
          >
            <Rating
              className="product-filter-ratings"
              initialRating="2"
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center pb-0"
            onClick={() => this.props.ratingsFilter(2, 3)}
          >
            <Rating
              className="product-filter-ratings"
              initialRating="3"
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center pb-0"
            onClick={() => this.props.ratingsFilter(3, 4)}
          >
            <Rating
              className="product-filter-ratings"
              initialRating="4"
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center pb-0"
            onClick={() => this.props.ratingsFilter(4, 5)}
          >
            <Rating
              className="product-filter-ratings"
              initialRating="5"
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
          </button>
        </div>

        <div className="dropdown-divider mb-3"></div>

        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Category
          </h5>
          <div className="dropdown-divider mb-3"></div>
          {this.state.categories.map((categories, i) => (
            <button
              type="button"
              className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
              key={i}
              onClick={() => this.props.categoryFilter(categories._id)}
            >
              {categories.name}
            </button>
          ))}
        </div>

        <div className="dropdown-divider mb-3"></div>

        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Fabric
          </h5>
          <div className="dropdown-divider mb-3"></div>
          {this.state.fabrics.map((fabric, i) => (
            <button
              key={i}
              type="button"
              className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
              onClick={() => this.props.fabricFilter(fabric._id)}
            >
              {fabric.name}
            </button>
          ))}
        </div>

        <div className="dropdown-divider mb-3"></div>

        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Occasion
          </h5>
          <div className="dropdown-divider mb-3"></div>

          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.occasionFilter("Casual")}
          >
            Casual
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.occasionFilter("Party")}
          >
            Party
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.occasionFilter("Office")}
          >
            Office
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.occasionFilter("Cocktail")}
          >
            Cocktail
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.occasionFilter("Wedding")}
          >
            Wedding & Engagement
          </button>
        </div>

        <div className="dropdown-divider mb-3"></div>

        <div className="list-group">
          <h5 className="list-group-item text-uppercase shop-filter-heading">
            Price
          </h5>
          <div className="dropdown-divider mb-3"></div>

          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(0, 2500)}
          >
            LKR. 0 - LKR. 2,500
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(2501, 5000)}
          >
            LKR. 2,501 - LKR. 5,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(5001, 7500)}
          >
            LKR. 5,001 - LKR. 7,500
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(7501, 10000)}
          >
            LKR. 7,501 - LKR. 10,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(10001, 15000)}
          >
            LKR. 10,001 - LKR. 15,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(15001, 20000)}
          >
            LKR. 15,001 - LKR. 20,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(20001, 25000)}
          >
            LKR. 20,001 - LKR. 25,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(25001, 35000)}
          >
            LKR. 25,001 - LKR. 35,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(35001, 50000)}
          >
            LKR. 35,001 - LKR. 50,000
          </button>
          <button
            type="button"
            className="list-group-item list-group-item-action d-flex justify-content-between align-items-center"
            onClick={() => this.props.priceFilter(50001, 100000)}
          >
            LKR. 50,001 - LKR. 100,000
          </button>
        </div>

        <div className="dropdown-divider mb-3 mt-4"></div>

        <h5 className="list-group-item text-uppercase shop-filter-heading">
          Color
        </h5>
        <div className="dropdown-divider mb-3"></div>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("black")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "#000" }}
          ></span>
          - Black
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("white")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "#fff" }}
          ></span>
          - White
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("red")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Red" }}
          ></span>
          - Red
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Blue")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Blue" }}
          ></span>
          - Blue
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Yellow")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Yellow" }}
          ></span>
          - Yellow
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Green")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Green" }}
          ></span>
          - Green
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Orange")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Orange" }}
          ></span>
          - Orange
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Pink")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Pink" }}
          ></span>
          - Pink
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Grey")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Grey" }}
          ></span>
          - Grey
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Purple")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Purple" }}
          ></span>
          - Purple
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Brown")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Brown" }}
          ></span>
          - Brown
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Beige")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Beige" }}
          ></span>
          - Beige
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Gold")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "gold" }}
          ></span>
          - Gold
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action"
          onClick={() => this.props.colorFilter("Silver")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{ backgroundColor: "Silver" }}
          ></span>
          - Silver
        </button>

        <button
          type="button"
          className="list-group-item list-group-item-action mb-4"
          onClick={() => this.props.colorFilter("Multi")}
        >
          <span
            className="btn btn-sm border"
            role="button"
            style={{
              backgroundImage:
                "linear-gradient(to right, red,orange,yellow,blue)"
            }}
          ></span>
          - Multicolor
        </button>
      </div>
    );
  }
}

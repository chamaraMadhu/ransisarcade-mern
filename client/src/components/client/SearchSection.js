import React, { Component } from "react";

export default class SearchSection extends Component {
  state = {
    keyword: ""
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="jumbotron jumbotron-faq mb-0">
        <div className="container p-0">
          <div className="search-area">
            <input
              type="text"
              className="search-input"
              name="keyword"
              value={this.state.keyword}
              onChange={this.handleChange}
            />
            <button
              className="search-btn"
              onClick={() => this.props.searchProducts(this.state.keyword)}
            >
              Search
            </button>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import Axios from "axios";

export default class AddToModal extends Component {
  state = {
    name: "",
    price: ""
  };

  componentDidMount = () => {
    Axios.get(`/api/product/${this.props.modalId}`).then(product => {
      this.setState({ name: product.data.name, price: product.data.newPrice });
    });
  };

  containerstyle = {
    position: "fixed",
    width: "250px",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)",
    background: "#fff",
    color: "#094e19",
    padding: "5px 10px",
    borderRadius: "2%",
    zIndex: 1050
  };

  messageStyle = {
    fontSize: "18px",
    fontWeight: "600",
    textAlign: "center",
    color: "#1c800a",
    lineHeight: "20px",
    marginTop: "7px"
  };

  imgStyle = {
    width: "100px",
    display: "block",
    margin: "auto"
  };

  nameStyle = {
    fontSize: "17px",
    fontWeight: "600",
    textAlign: "center",
    margin: "10px 0 0 0",
    color: "#000"
  };

  priceStyle = {
    fontSize: "17px",
    fontWeight: "700",
    textAlign: "center",
    color: "#423c59",
    marginBottom: "6px"
  };

  render() {
    return (
      <div className="AddToModal" tabIndex="1" style={this.containerstyle}>
        <p style={this.messageStyle}>Saree has been added successfully!</p>
        <img
          src={`/api/product/photo/front/${this.props.modalId}`}
          style={this.imgStyle}
          alt="product"
        />
        <p style={this.nameStyle}>{this.state.name}</p>
        <p style={this.priceStyle}>LKR. {this.state.price}.00</p>
      </div>
    );
  }
}

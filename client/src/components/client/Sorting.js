import React from "react";

export default function Sorting(props) {
  return (
    <div className="row m-0">
      <div className="col">
        <div
          className="btn-group float-right mr-0 mr-md-3 mb-2"
          style={{ cursor: "pointer", fontSize: "14px" }}
        >
          <span
            className="dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sort by
          </span>
          <div
            className="dropdown-menu dropdown-menu-right"
            style={{ fontSize: "14px" }}
          >
            <span className="dropdown-item" onClick={props.sortByArrivals}>
              Latest Arrivals
            </span>
            <span className="dropdown-item" onClick={props.sortByAZ}>
              Name: A-Z
            </span>
            <span className="dropdown-item" onClick={props.sortByZA}>
              Name: Z-A
            </span>
            <span className="dropdown-item" onClick={props.sortByPriceLH}>
              Price: Low to High
            </span>
            <span className="dropdown-item" onClick={props.sortByPriceHL}>
              Price: High to Low
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}

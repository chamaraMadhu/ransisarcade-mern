import React, { Component } from "react";

export default class NotificationMsg extends Component {
  state = {
    msgType: this.props.msgType
  };

  successStyle = {
    position: "fixed",
    top: "350px",
    right: "30px",
    background: "#d4edda",
    color: "#094e19",
    padding: "5px 10px",
    border: "2px solid #094e19",
    zIndex: 1
  };

  errorStyle = {
    position: "fixed",
    top: "350px",
    right: "30px",
    background: "#f8d7da",
    color: "#721c24",
    padding: "5px 10px",
    border: "2px solid #721c24",
    zIndex: 1
  };

  warningStyle = {
    position: "fixed",
    top: "350px",
    right: "30px",
    background: "#fff3cd",
    color: "#856404",
    padding: "5px 10px",
    border: "2px solid #856404",
    zIndex: 1
  };

  render() {
    // set style and icon based on message type
    let style;
    let icon;
    if (this.state.msgType === "success") {
      style = this.successStyle;
      icon = <i className="fas fa-check"></i>;
    } else if (this.state.msgType === "error") {
      style = this.errorStyle;
      icon = <i className="fas fa-times-circle"></i>;
    } else {
      style = this.warningStyle;
      icon = <i className="fas fa-exclamation-triangle"></i>;
    }

    return (
      <div style={style}>
        {icon}
        &nbsp;
        <b>{this.props.msgType}:</b>{" "}
        <span style={{ fontSize: "14px" }}>{this.props.message}</span>
      </div>
    );
  }
}

import React, { Component } from "react";
import axios from "axios";

export default class AdvancedFilter extends Component {
  state = {
    filterCategory: "",
    filterFabric: "",
    filterOccasion: "",
    filterPrice: "",
    filterColor: "",
    products: [],
    categories: [],
    fabrics: []
  };

  componentDidMount = () => {
    // get categories
    axios.get(`/api/categories`).then(categories => {
      this.setState({ categories: categories.data });
    });

    // get fabrics
    axios.get(`/api/fabrics`).then(fabrics => {
      this.setState({ fabrics: fabrics.data });
    });
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="row m-0 mb-4">
        <div className="col">
          <form>
            <div className="form-row m-0">
              <div className="filter-category-col">
                <label>Category</label>
                <select
                  className="form-control"
                  name="filterCategory"
                  value={this.state.filterCategory}
                  onChange={this.handleChange}
                >
                  <option>Any</option>
                  {this.state.categories.map((categories, i) => (
                    <option key={i} value={categories._id}>
                      {categories.name}
                    </option>
                  ))}
                </select>
              </div>
              <div className="filter-fabric-col">
                <label>Fabric</label>
                <select
                  className="form-control"
                  name="filterFabric"
                  value={this.state.filterFabric}
                  onChange={this.handleChange}
                >
                  <option>Any</option>
                  {this.state.fabrics.map((fabric, i) => (
                    <option key={i} value={fabric._id}>
                      {fabric.name}
                    </option>
                  ))}
                </select>
              </div>

              <div className="filter-occasion-col">
                <label>Occasion</label>
                <select
                  className="form-control"
                  name="filterOccasion"
                  value={this.state.filterOccasion}
                  onChange={this.handleChange}
                >
                  <option>Any</option>
                  <option>Casual</option>
                  <option>Party</option>
                  <option>Office</option>
                  <option>Cocktail</option>
                  <option>Wedding & Engagement</option>
                </select>
              </div>

              <div className="filter-price-col">
                <label>Price</label>
                <select
                  className="form-control"
                  name="filterPrice"
                  value={this.state.filterPrice}
                  onChange={this.handleChange}
                >
                  <option>Any</option>
                  <option>LKR. 0 - LKR. 2500</option>
                  <option>LKR. 2500 - LKR. 5000</option>
                  <option>LKR. 5000 - LKR. 7500</option>
                  <option>LKR. 7500 - LKR. 10000</option>
                  <option>LKR. 10000 - LKR. 15000</option>
                  <option>LKR. 15000 - LKR. 20000</option>
                  <option>LKR. 20000 - LKR. 25000</option>
                  <option>LKR. 25000 - LKR. 35000</option>
                  <option>LKR. 35000 - LKR. 50000</option>
                  <option>LKR. 50000 - LKR. 100000</option>
                </select>
              </div>

              <div className="filter-color-col">
                <label>Color</label>
                <select
                  className="form-control"
                  name="filterColor"
                  value={this.state.filterColor}
                  onChange={this.handleChange}
                >
                  <option>Any</option>
                  <option>Black</option>
                  <option>White</option>
                  <option>Red</option>
                  <option>Green</option>
                  <option>Blue</option>
                  <option>Orange</option>
                  <option>Yellow</option>
                  <option>Purple</option>
                  <option>Pink</option>
                  <option>Grey</option>
                  <option>Silver</option>
                  <option>Gold</option>
                  <option>Multicolor</option>
                </select>
              </div>
            </div>
            <span
              type="submit"
              className="btn-filter my-4"
              onClick={() =>
                this.props.advancedFilter(
                  this.state.filterCategory,
                  this.state.filterFabric,
                  this.state.filterOccasion,
                  this.state.filterPrice,
                  this.state.filterColor
                )
              }
            >
              Filter Now
            </span>
          </form>
        </div>
      </div>
    );
  }
}

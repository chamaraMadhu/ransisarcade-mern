import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import moment from "moment";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class commentModal extends Component {
  state = {
    // latestProducts: [],
    // topRatedProducts: [],
    // topLikedProducts: [],

    // // add to cart
    // redirectToCart: false,

    // // add to wishlist
    // redirectToWishlist: false,

    // // likes
    // likes: [],

    // redirect to login to like
    redirectToLogin: false,
    error: "",

    // comment
    productId: this.props.productId,
    comment: "",
    comments: this.props.comments
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    console.log(this.state.productId);
    return (
      <div
        className="modal fade"
        id="comment-product"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="comment-product"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>

              <p style={{ fontSize: "17px", fontWeight: "600" }}>
                {this.state.comments.length} comments
              </p>
              {this.state.comments.length > 0
                ? this.state.comments.map((comment, i) => (
                    <div className="row m-0" key={i}>
                      <div className="comment-img-col">
                        <img
                          src={comment.userId.image}
                          className="reviwer-img"
                        />
                      </div>
                      <div className="comment-desc-col">
                        <p className="reviwer-name">
                          {comment.userId.fName} {comment.userId.lName}
                          <span className="reviwe-date">
                            &nbsp; {moment(comment.createdAt).fromNow()}
                          </span>
                        </p>
                        <p className="review">{comment.comment}</p>
                      </div>
                    </div>
                  ))
                : null}

              <div className="row m-0 mt-3">
                {isAuthenticated() && isAuthenticated().user._id ? (
                  <React.Fragment>
                    <div className="comment-img-col">
                      <img
                        src={isAuthenticated() && isAuthenticated().user.image}
                        className="reviwer-img"
                      />
                    </div>
                    <div className="comment-desc-col">
                      <textarea
                        className="comment-input d-block"
                        type="text"
                        name="comment"
                        placeholder="Write a comment"
                        value={this.state.comment}
                        onChange={this.handleChange}
                      ></textarea>

                      <button
                        className="btn-register w-100 mt-2"
                        onClick={() =>
                          this.props.handleComment(this.state.productId)
                        }
                      >
                        Submit
                      </button>
                    </div>
                  </React.Fragment>
                ) : (
                  <Link
                    className="btn-register"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={this.handleLoginToComment}
                    to="/login"
                  >
                    Login to Comment
                  </Link>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

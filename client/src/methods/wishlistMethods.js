// add to wishlist
export const addToWishlist = (wishlistItem, next) => {
  let wishlist = [];

  if (typeof window !== "undefined") {
    if (localStorage.getItem("wishlist")) {
      wishlist = JSON.parse(localStorage.getItem("wishlist"));
    }

    wishlist.push({
      ...wishlistItem,
      count: 1
    });

    // remove duplicated cart items
    wishlist = Array.from(new Set(wishlist.map(item => item._id))).map(id => {
      return wishlist.find(item => item._id === id);
    });

    localStorage.setItem("wishlist", JSON.stringify(wishlist));
    next();
  }
};

// items Of wishlist
export const itemsOfwishlist = () => {
  let wishlist = [];

  if (typeof window !== "undefined") {
    if (localStorage.getItem("wishlist")) {
      wishlist = JSON.parse(localStorage.getItem("wishlist"));

      // counts items
      return wishlist.length;
    } else {
      return 0;
    }
  }
};

// remove a wishlist item
export const removeWishlistItem = id => {
  let wishlist = [];

  if (typeof window !== "undefined") {
    if (localStorage.getItem("wishlist")) {
      wishlist = JSON.parse(localStorage.getItem("wishlist"));
    }

    wishlist.map((item, i) => {
      if (item._id === id) {
        wishlist.splice(i, 1);
      }

      localStorage.setItem("wishlist", JSON.stringify(wishlist));
    });
  }
};

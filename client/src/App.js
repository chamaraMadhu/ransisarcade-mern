import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

// import client-side pages
import Home from "./pages/client/Home";
import About from "./pages/client/About";
import Shop from "./pages/client/Shop";
import Faq from "./pages/client/Faq";
import Career from "./pages/client/Career";
import Contact from "./pages/client/Contact";
import Cart from "./pages/client/Cart";
import Wishlist from "./pages/client/Wishlist";
import Checkout from "./pages/client/Checkout";
import cashOnDeliveryCheckout from "./pages/client/cashOnDeliveryCheckout";
import ProductDetail from "./pages/client/ProductDetail";
import Login from "./pages/client/Login";
import Register from "./pages/client/Register";
import UserAccount from "./pages/client/UserAccount";
import UpdateProfile from "./pages/client/UpdateProfile";
import OrderOnline from "./pages/client/OrderOnline";
import OrderCashOnDelivery from "./pages/client/OrderCashOnDelivery";
import ChangeUserPassword from "./pages/client/ChangeUserPassword";

// import owner pages
import AddAdmin from "./pages/admin/AddAdmin";
import ViewAdminUsers from "./pages/admin/ViewAdminUsers";

// profiles
import AdminProfile from "./pages/admin/AdminProfile";
import EditAdminProfile from "./pages/admin/EditAdminProfile";

// import admin pages
import Dashboard from "./pages/admin/Dashboard";
import AddProduct from "./pages/admin/AddProduct";
import ViewProduct from "./pages/admin/ViewProduct";
import EditProduct from "./pages/admin/EditProduct";
import ViewSoldProduct from "./pages/admin/ViewSoldProduct";
import AddCategory from "./pages/admin/AddCategory";
import ViewCategory from "./pages/admin/ViewCategory";
import EditCategory from "./pages/admin/EditCategory";
import AddFabric from "./pages/admin/AddFabric";
import ViewFabric from "./pages/admin/ViewFabric";
import EditFabric from "./pages/admin/EditFabric";
import CustomerOrdersOnline from "./pages/admin/CustomerOrdersOnline";
import CustomerOrdersCashOnDelivery from "./pages/admin/CustomerOrdersCashOnDelivery";
import AddDeliveryFee from "./pages/admin/AddDeliveryFee";
import ViewDeliveryFee from "./pages/admin/ViewDeliveryFee";
import EditDeliveryFee from "./pages/admin/EditDeliveryFee";
import AddTestimonial from "./pages/admin/AddTestimonial";
import ViewTestimonial from "./pages/admin/ViewTestimonial";
import EditTestimonial from "./pages/admin/EditTestimonial";
import AddCareer from "./pages/admin/AddCareer";
import ViewCareer from "./pages/admin/ViewCareer";
import EditCareer from "./pages/admin/EditCareer";
import AddAdvertisement from "./pages/admin/AddAdvertisement";
import EditAdvertisement from "./pages/admin/EditAdvertisement";
import ViewAdvertisement from "./pages/admin/ViewAdvertisement";
import ViewLikes from "./pages/admin/ViewLikes";
import ViewComments from "./pages/admin/ViewComments";
import ViewReviews from "./pages/admin/ViewReviews";
import InventoryByCategory from "./pages/admin/InventoryByCategory";
import InventoryByFabrics from "./pages/admin/InventoryByFabrics";
import InventoryByPrice from "./pages/admin/InventoryByPrice";
import ColorWiseInventory from "./pages/admin/ColorWiseInventory";
import LikeWiseInventory from "./pages/admin/LikeWiseInventory";
import CommentWiseInventory from "./pages/admin/CommentWiseInventory";
import RatingWiseInventory from "./pages/admin/RatingWiseInventory";
import CategoryWiseSoldProducts from "./pages/admin/CategoryWiseSoldProducts";
import FabricWiseSoldProducts from "./pages/admin/FabricWiseSoldProducts";
import PriceWiseSoldProducts from "./pages/admin/PriceWiseSoldProducts";
import ColorWiseSoldProducts from "./pages/admin/ColorWiseSoldProducts";
import LikeWiseSoldProducts from "./pages/admin/LikeWiseSoldProducts";
import CommentWiseSoldProducts from "./pages/admin/CommentWiseSoldProducts";
import RatingWiseSoldProducts from "./pages/admin/RatingWiseSoldProducts";
import SalesReport from "./pages/admin/SalesReport";
import InventoryReport from "./pages/admin/InventoryReport";
import SoldProductsReport from "./pages/admin/SoldProductsReport";
import CustomerReport from "./pages/admin/CustomerReport";

// import 404 page
import PageNotFound from "./pages/client/PageNotFound";

// load isAuthenticated method
import { isAuthenticated } from "./auth/auth.js";

// create a private route for customers
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() && isAuthenticated().user.role === 0 ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: "/login", state: { from: props.location } }}
        />
      )
    }
  />
);

// create a owner route
const OwnerRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() && isAuthenticated().user.role === 2 ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: "/login", state: { from: props.location } }}
        />
      )
    }
  />
);

// create a admmin router
const AdminRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      (isAuthenticated() && isAuthenticated().user.role === 1) ||
      (isAuthenticated() && isAuthenticated().user.role === 2) ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{ pathname: "/login", state: { from: props.location } }}
        />
      )
    }
  />
);

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/about" exact component={About} />
        <Route path="/shop" exact component={Shop} />
        <Route path="/shop/category/:category" exact component={Shop} />
        <Route path="/faq" exact component={Faq} />
        <Route path="/career" exact component={Career} />
        <Route path="/contact" exact component={Contact} />
        <Route path="/cart/" exact component={Cart} />
        <Route path="/wishlist/" exact component={Wishlist} />
        <PrivateRoute path="/checkout/" exact component={Checkout} />
        <PrivateRoute
          path="/checkout/cash-on-delivery/"
          exact
          component={cashOnDeliveryCheckout}
        />
        <Route path="/login" exact component={Login} />
        <Route path="/register" exact component={Register} />
        <Route path="/product-detail/:id" exact component={ProductDetail} />
        <PrivateRoute path="/user-account" exact component={UserAccount} />
        <PrivateRoute path="/update-profile" exact component={UpdateProfile} />
        <PrivateRoute path="/my-orders/online" exact component={OrderOnline} />
        <PrivateRoute
          path="/my-orders/cash-on-delivery"
          exact
          component={OrderCashOnDelivery}
        />
        <PrivateRoute
          path="/change-password"
          exact
          component={ChangeUserPassword}
        />

        <OwnerRoute path="/admin/add-admin" exact component={AddAdmin} />
        <OwnerRoute
          path="/admin/view-all-admin-users"
          exact
          component={ViewAdminUsers}
        />

        <AdminRoute path="/admin/profile/:id" exact component={AdminProfile} />
        <AdminRoute path="/admin/update-admin-profile/:id" exact component={EditAdminProfile} />
        <AdminRoute path="/admin/dashboard" exact component={Dashboard} />
        <AdminRoute path="/admin/add-product" exact component={AddProduct} />
        <AdminRoute path="/admin/view-product" exact component={ViewProduct} />
        <AdminRoute
          path="/admin/edit-product/:id"
          exact
          component={EditProduct}
        />
        <AdminRoute
          path="/admin/view-sold-product"
          exact
          component={ViewSoldProduct}
        />
        <AdminRoute path="/admin/add-category" exact component={AddCategory} />
        <AdminRoute
          path="/admin/view-category"
          exact
          component={ViewCategory}
        />
        <AdminRoute
          path="/admin/edit-category/:id"
          exact
          component={EditCategory}
        />
        <AdminRoute path="/admin/add-fabric" exact component={AddFabric} />
        <AdminRoute path="/admin/view-fabric" exact component={ViewFabric} />
        <AdminRoute
          path="/admin/edit-fabric/:id"
          exact
          component={EditFabric}
        />
        <AdminRoute
          path="/admin/customer-orders/online"
          exact
          component={CustomerOrdersOnline}
        />
        <AdminRoute
          path="/admin/customer-orders/cash-on-delivery"
          exact
          component={CustomerOrdersCashOnDelivery}
        />
        <AdminRoute
          path="/admin/add-delivery-fee"
          exact
          component={AddDeliveryFee}
        />
        <AdminRoute
          path="/admin/view-delivery-fee"
          exact
          component={ViewDeliveryFee}
        />
        <AdminRoute
          path="/admin/edit-delivery-fee/:id"
          exact
          component={EditDeliveryFee}
        />
        <AdminRoute
          path="/admin/add-testimonial"
          exact
          component={AddTestimonial}
        />
        <AdminRoute
          path="/admin/view-testimonial"
          exact
          component={ViewTestimonial}
        />
        <AdminRoute
          path="/admin/edit-testimonial/:id"
          exact
          component={EditTestimonial}
        />
        <AdminRoute path="/admin/add-career" exact component={AddCareer} />
        <AdminRoute path="/admin/view-career" exact component={ViewCareer} />
        <AdminRoute
          path="/admin/edit-career/:id"
          exact
          component={EditCareer}
        />
        <AdminRoute
          path="/admin/add-advertisement"
          exact
          component={AddAdvertisement}
        />
        <AdminRoute
          path="/admin/view-advertisement"
          exact
          component={ViewAdvertisement}
        />
        <AdminRoute
          path="/admin/edit-advertisement/:id"
          exact
          component={EditAdvertisement}
        />
        <AdminRoute path="/admin/view-likes" exact component={ViewLikes} />
        <AdminRoute
          path="/admin/view-comments"
          exact
          component={ViewComments}
        />
        <AdminRoute path="/admin/view-reviews" exact component={ViewReviews} />
        <AdminRoute
          path="/admin/inventory-by-category"
          exact
          component={InventoryByCategory}
        />
        <AdminRoute
          path="/admin/inventory-by-fabrics"
          exact
          component={InventoryByFabrics}
        />
        <AdminRoute
          path="/admin/inventory-by-price"
          exact
          component={InventoryByPrice}
        />
        <AdminRoute
          path="/admin/color-wise-inventory"
          exact
          component={ColorWiseInventory}
        />
        <AdminRoute
          path="/admin/like-wise-inventory"
          exact
          component={LikeWiseInventory}
        />
        <AdminRoute
          path="/admin/comment-wise-inventory"
          exact
          component={CommentWiseInventory}
        />
        <AdminRoute
          path="/admin/rating-wise-inventory"
          exact
          component={RatingWiseInventory}
        />
        <AdminRoute
          path="/admin/category-wise-sold-products"
          exact
          component={CategoryWiseSoldProducts}
        />
        <AdminRoute
          path="/admin/fabric-wise-sold-products"
          exact
          component={FabricWiseSoldProducts}
        />
        <AdminRoute
          path="/admin/price-wise-sold-products"
          exact
          component={PriceWiseSoldProducts}
        />
        <AdminRoute
          path="/admin/color-wise-sold-products"
          exact
          component={ColorWiseSoldProducts}
        />
        <AdminRoute
          path="/admin/like-wise-sold-products"
          exact
          component={LikeWiseSoldProducts}
        />
        <AdminRoute
          path="/admin/comment-wise-sold-products"
          exact
          component={CommentWiseSoldProducts}
        />
        <AdminRoute
          path="/admin/rating-wise-sold-products"
          exact
          component={RatingWiseSoldProducts}
        />
        <AdminRoute path="/admin/sales-report" exact component={SalesReport} />
        <AdminRoute
          path="/admin/inventory-report"
          exact
          component={InventoryReport}
        />
        <AdminRoute
          path="/admin/sold-products-report"
          exact
          component={SoldProductsReport}
        />
        <AdminRoute
          path="/admin/customer-report"
          exact
          component={CustomerReport}
        />

        <Route path="/404" exact component={PageNotFound} />
        <Redirect to="404" />
      </Switch>
    </Router>
  );
}

export default App;

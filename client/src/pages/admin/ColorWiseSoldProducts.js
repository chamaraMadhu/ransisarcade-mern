// import packages
import React, { Component } from "react";
import axios from "axios";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// import CanvasJSReact
import CanvasJSReact from "../../assets/canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class ColorWiseSoldProducts extends Component {
  state = {
    colorsArray: [
      "black",
      "white",
      "red",
      "blue",
      "yellow",
      "green",
      "orange",
      "pink",
      "grey",
      "purple",
      "brown",
      "beige",
      "gold",
      "silver",
      "multicolor"
    ],
    colors: [],
    inventory: 0
  };

  componentDidMount() {
    let colors = [];
    let newColor = {};

    for (let i = 0; i < this.state.colorsArray.length; i++) {
      axios
        .get(
          `/api/report/color-wise-sold-products/${this.state.colorsArray[i]}`
        )
        .then(color => {
          console.log(color);

          newColor = {
            color: this.state.colorsArray[i],
            count: color.data
          };

          colors = [...colors, newColor];

          this.setState({
            colors: colors
          });
        });
    }

    // total sold products
    axios.get(`/api/products/sold/report`).then(inventory => {
      this.setState({
        inventory: inventory.data
      });
    });
  }

  render() {
    // total inventory
    const total = this.state.inventory.length;

    // pie chart
    let pieValues = [];

    this.state.colors.map((color, i) =>
      pieValues.push({
        y: ((color.count / total) * 100).toFixed(2),
        label: color.color
      })
    );

    // CanvasJSReact
    const options = {
      exportEnabled: true,
      animationEnabled: true,
      data: [
        {
          type: "pie",
          startAngle: 75,
          toolTipContent: "<b>{label}</b>: {y}%",
          showInLegend: "true",
          legendText: "{label}",
          indexLabelFontSize: 16,
          indexLabel: "{label} - {y}%",
          dataPoints: pieValues
        }
      ]
    };

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Report
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Price wise Sold Products
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">PRICE WISE SOLD PRODUCTS</h6>
                </div>

                <div className="row">
                  <div className="col">
                    <CanvasJSChart options={options} />
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <table
                      className="table mt-4 table-bordered"
                      style={{ border: "none" }}
                    >
                      <tbody>
                        <tr align="center" className="view-table-tr-color">
                          <td width="175px">Price Range</td>
                          <td width="175px">No. of Sold Products</td>
                          <td width="175px">%</td>
                        </tr>

                        {this.state.colors.map((color, i) => (
                          <tr align="center" key={i}>
                            <td className="text-capitalize">{color.color}</td>
                            <td>{color.count}</td>
                            <td>{((color.count / total) * 100).toFixed(2)}%</td>
                          </tr>
                        ))}

                        <tr className="font-weight-bold" align="center">
                          <td>Total</td>
                          <td>{total}</td>
                          <td>100.00%</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

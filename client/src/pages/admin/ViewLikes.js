// import packages
import React, { Component } from "react";
import axios from "axios";
import moment from "moment";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";
import Pagination from "../../components/Pagination";

export default class ViewLikes extends Component {
  state = {
    likes: [],

    // pagination
    currentPage: 1,
    itemsPerPage: 10
  };

  componentDidMount() {
    // get likes
    axios.get(`/api/products/on/likes`).then(likes => {
      this.setState({
        likes: likes.data
      });
    });
  }

  // pagination change page
  handlePage = page => {
    this.setState({ currentPage: page });

    // scroll up to results
    if (window.innerWidth > 575) {
      window.scrollTo({ top: 0, behavior: "smooth" });
    } else {
      window.scrollTo({ top: 2000, behavior: "smooth" });
    }
  };

  render() {
    // pagination
    // get current products
    const indexOfLast = this.state.currentPage * this.state.itemsPerPage;
    const indexOfFirst = indexOfLast - this.state.itemsPerPage;
    const currentItems = this.state.likes.slice(indexOfFirst, indexOfLast);

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Likes
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View Likes
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW LIKES</h6>
                </div>

                <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                  {this.state.likes.length > 0
                    ? `Showing ${indexOfFirst + 1} - ${indexOfFirst +
                        currentItems.length} out of ${
                        this.state.likes.length
                      } results`
                    : "No likes"}
                </p>

                <div className="table-responsive">
                  <table className="table table-hover table-bordered">
                    <tbody>
                      <tr className="view-table-tr-color">
                        <td>Created At</td>
                        <td>Product ID</td>
                        <td>Product</td>
                        <td>Name</td>
                        <td>Customer</td>
                        <td>Customer Name</td>
                      </tr>
                      {currentItems.map((like, i) => (
                        <tr key={i}>
                          <td>{moment(like.createdAt).fromNow()}</td>
                          <td>{like.productId._id}</td>
                          <td>
                            <img
                              src={`/api/product/photo/front/${like.productId._id}`}
                              style={{
                                width: "60px"
                              }}
                              alt={like.productId.name}
                            />
                          </td>
                          <td>{like.productId.name}</td>
                          <td>
                            <img
                              src={like.userId.image}
                              style={{
                                width: "50px",
                                height: "50px",
                                borderRadius: "50%"
                              }}
                              alt={like.userId.fName}
                            />
                          </td>
                          <td>
                            {like.userId.fName} {like.userId.lName}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>

                {/* starts of pagination */}
                <Pagination
                  className="m-0"
                  noOfAllItems={this.state.likes.length}
                  itemsPerPage={this.state.itemsPerPage}
                  currentPage={this.state.currentPage}
                  handlePage={this.handlePage}
                />
                {/* ends of pagination */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

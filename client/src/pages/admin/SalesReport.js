// import packages
import React, { Component } from "react";
import axios from "axios";
import moment from "moment";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";
// import Pagination from "../../components/Pagination";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class SalesReport extends Component {
  state = {
    from: "",
    to: "",
    orders: []
  };

  componentDidMount() {
    axios
      .get(`/api/orders/${isAuthenticated().user._id}`, {
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
      .then(orders => {
        if (orders.data.length > 0) {
          this.setState({
            orders: orders.data
          });
        }
      });
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = () => {
    console.log(this.state.from, this.state.to);

    axios
      .get(`/api/report/profit/${this.state.from}/${this.state.to}`, {
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
      .then(orders => {
        this.setState({
          orders: orders.data
        });
      });
  };

  dateStyle = {
    width: "250px"
  };

  render() {
    // calculate the total revenue without delivery chargers
    const totalRevenue = this.state.orders.reduce((currentValue, nextValue) => {
      let x = nextValue.orderItems.length;

      for (let i = 0; i < x; i++) {
        currentValue = currentValue + nextValue.orderItems[i].newPrice;
      }
      return currentValue;
    }, 0);

    // calculate the total cost without delivery chargers
    const totalCost = this.state.orders.reduce((currentValue, nextValue) => {
      let x = nextValue.orderItems.length;

      for (let i = 0; i < x; i++) {
        currentValue = currentValue + nextValue.orderItems[i].cost;
      }
      return currentValue;
    }, 0);

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Report
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Sales Report
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">Sales Report</h6>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-row mt-4 ml-2">
                      <div className="mb-3 mr-3" style={this.dateStyle}>
                        <label htmlFor="from" className="font-weight-bold">
                          From:
                        </label>
                        <input
                          type="date"
                          className="form-control"
                          id="from"
                          name="from"
                          value={this.state.from}
                          onChange={this.handleChange}
                        />
                      </div>
                      <div className="mb-3 date-field" style={this.dateStyle}>
                        <label htmlFor="to" className="font-weight-bold">
                          To:
                        </label>
                        <input
                          type="date"
                          className="form-control"
                          id="to"
                          name="to"
                          value={this.state.to}
                          onChange={this.handleChange}
                        />
                      </div>
                    </div>

                    <div className="form-row">
                      <div className="col-md-4 mb-3">
                        <button
                          type="submit"
                          className="btn-register ml-2"
                          id="to"
                          onClick={this.handleSubmit}
                        >
                          Generate
                        </button>
                      </div>
                    </div>

                    <h5 className="mt-4">
                      {this.state.from} to {this.state.to}
                    </h5>

                    <table
                      className="table table-responsive table-bordered"
                      style={{ border: "none" }}
                    >
                      <tbody>
                        <tr className="view-table-tr-color">
                          <td width="130px">ProductId</td>
                          <td style={{ minWidth: "220px" }}>Product</td>
                          <td width="100px" align="center">
                            Quantity
                          </td>
                          <td width="175px" align="right">
                            Revenue (LKR.)
                          </td>
                          <td width="175px" align="right">
                            Cost (LKR.)
                          </td>
                          <td width="175px" align="right">
                            Profit (LKR.)
                          </td>
                          <td width="175px" align="right">
                            %
                          </td>
                        </tr>

                        {this.state.orders.map((order, i) => (
                          <React.Fragment key={i}>
                            <tr style={{ background: "#f2e6fb" }}>
                              <td colSpan="9">
                                {moment(order.createdAt).format("LLL")}
                              </td>
                            </tr>

                            {order.orderItems.map((orderItem, pIndex) => (
                              <tr key={pIndex}>
                                <td>{orderItem._id}</td>
                                <td>{orderItem.name}</td>
                                <td align="center">{orderItem.quantity}</td>
                                <td align="right">{orderItem.newPrice}.00</td>
                                <td align="right">Rs. {orderItem.cost}.00</td>
                                <td align="right">
                                  {" "}
                                  {orderItem.newPrice - orderItem.cost}
                                  .00
                                </td>
                                <td align="right">
                                  {(
                                    ((orderItem.newPrice - orderItem.cost) /
                                      orderItem.newPrice) *
                                    100
                                  ).toFixed(2)}
                                  %
                                </td>
                              </tr>
                            ))}
                          </React.Fragment>
                        ))}

                        <tr className="font-weight-bold">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td align="right">{totalRevenue}.00</td>
                          <td align="right">{totalCost}.00</td>
                          <td align="right">{totalRevenue - totalCost}.00</td>
                          <td align="right">
                            {(
                              ((totalRevenue - totalCost) / totalRevenue) *
                              100
                            ).toFixed(2)}
                            %
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

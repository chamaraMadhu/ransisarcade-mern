// import packages
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

// initiate state
const initialState = {
  name: "",
  category: "",
  categories: [],
  fabric: "",
  fabrics: [],
  imageFront: "",
  imageJacket: "",
  imageBorder: "",
  imageBack: "",
  color: "",
  oldPrice: 0,
  newPrice: 0,
  cost: 0,
  quantity: 1,
  occasionCasual: false,
  occasionParty: false,
  occasionOffice: false,
  occasionCocktail: false,
  occasionWedding: false,
  washAndCare: "",
  description: "",
  keywords: "",
  // status: false,
  success: "",
  errors: {},
  loading: false,
  redirectToView: false
};

export default class EditProduct extends Component {
  state = initialState;

  componentDidMount = () => {
    axios.get(`/api/product/${this.props.match.params.id}/`).then(product => {
      console.log(product.data);
      this.setState({
        name: product.data.name,
        category: product.data.category._id,
        fabric: product.data.fabric._id,
        color: product.data.color,
        oldPrice: product.data.oldPrice,
        newPrice: product.data.newPrice,
        cost: product.data.cost,
        quantity: product.data.quantity,
        occasionCasual: product.data.occasionCasual,
        occasionParty: product.data.occasionParty,
        occasionOffice: product.data.occasionOffice,
        occasionCocktail: product.data.occasionCocktail,
        occasionWedding: product.data.occasionWedding,
        washAndCare: product.data.washAndCare,
        description: product.data.description,
        keywords: product.data.keywords,
        // status: product.data.status
      });
    });

    // get categories
    axios.get(`/api/categories`).then(categories => {
      if (categories.data.length > 0) {
        this.setState({ categories: categories.data });
      }
    });

    // get fabrics
    axios.get(`/api/fabrics`).then(fabrics => {
      if (fabrics.data.length > 0) {
        this.setState({ fabrics: fabrics.data });
      }
    });
  };

  // get input value
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // get file value
  handleChangeFile = e => {
    this.setState({ [e.target.name]: e.target.files[0] });
  };

  // get checkbox value
  handleChangeCheckBox = e => {
    if (e.target.checked) {
      this.setState({ [e.target.name]: true });
    } else {
      this.setState({ [e.target.name]: false });
    }
  };

  // add product
  handleSubmit = e => {
    e.preventDefault();

    // create product object
    var bodyFormData = new FormData();
    bodyFormData.set("name", this.state.name);
    bodyFormData.set("category", this.state.category);
    bodyFormData.set("fabric", this.state.fabric);
    bodyFormData.set("color", this.state.color);
    bodyFormData.set("oldPrice", this.state.oldPrice);
    bodyFormData.set("newPrice", this.state.newPrice);
    bodyFormData.set("cost", this.state.cost);
    bodyFormData.set("quantity", this.state.quantity);
    bodyFormData.set("occasionCasual", this.state.occasionCasual);
    bodyFormData.set("occasionParty", this.state.occasionParty);
    bodyFormData.set("occasionOffice", this.state.occasionOffice);
    bodyFormData.set("occasionCocktail", this.state.occasionCocktail);
    bodyFormData.set("occasionWedding", this.state.occasionWedding);
    bodyFormData.set("washAndCare", this.state.washAndCare);
    bodyFormData.set("description", this.state.description);
    bodyFormData.set("keywords", this.state.keywords);
    bodyFormData.set("status", this.state.status);
    bodyFormData.append("imageFront", this.state.imageFront);
    bodyFormData.append("imageJacket", this.state.imageJacket);
    bodyFormData.append("imageBorder", this.state.imageBorder);
    bodyFormData.append("imageBack", this.state.imageBack);

    // set loading
    this.setState({ loading: true });

    // send post req
    axios({
      method: "put",
      url: `/api/product/${this.props.match.params.id}/${
        isAuthenticated().user._id
      }`,
      data: bodyFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${isAuthenticated().token}`
      }
    })
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // reset image value
          document.getElementById("imageFront").value = "";
          document.getElementById("imageJacket").value = "";
          document.getElementById("imageBorder").value = "";
          document.getElementById("imageBack").value = "";

          // set alert message
          this.setState({ success: true, loading: false });
        }
      })
      .catch(err => {
        this.setState({ errors: err.response.data, loading: false });

        document.getElementById("name-label").focus();
      });
  };

  // close modal
  ok = () => {
    this.setState({ redirectToView: true, sucess: false });
  };

  // close modal
  close = () => {
    this.setState({ errors: {} });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        {this.state.redirectToView ? (
          <Redirect to="/admin/view-product" />
        ) : null}

        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Product
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Edit Product
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">EDIT PRODUCT</h6>
                </div>

                {this.state.success ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.ok}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Product is updated successfully.
                  </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                    danger
                    title="Oops, Something went wrong"
                    onConfirm={this.close}
                    confirmBtnStyle={{ background: "#423c59", border: 0 }}
                  >
                    Product isn't updated successfully.
                  </SweetAlert>
                ) : null}

                <form onSubmit={this.handleSubmit} className="mt-3" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3 form-group row">
                      <label
                        htmlFor="name"
                        id="name-label"
                        className="col-form-label"
                      >
                        Product name <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="name"
                          id="name"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.name
                          })}
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.name}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="category" className="col-form-label">
                        Category <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <select
                          name="category"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.category
                          })}
                          id="category"
                          placeholder="-- select --"
                          value={this.state.category}
                          onChange={this.handleChange}
                        >
                          {this.state.categories.map(categories => (
                            <option key={categories._id} value={categories._id}>
                              {categories.name}
                            </option>
                          ))}
                        </select>
                        <div className="invalid-feedback">
                          {this.state.errors.category}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label htmlFor="imageFront" className="col-form-label">
                        Product Image - Front <b className="text-danger">*</b>
                      </label>
                      <div className="col-input pb-2">
                        <input
                          type="file"
                          name="imageFront"
                          id="imageFront"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.imageFront}
                        </div>
                      </div>
                    </div>

                    {this.state.imageFront ? (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={URL.createObjectURL(this.state.imageFront)}
                            width="100"
                            alt="Front preview"
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={`/api/product/photo/front/${this.props.match.params.id}`}
                            width="100"
                            alt="Front preview"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-0 mt-2 form-group row">
                      <label htmlFor="imageJacket" className="col-form-label">
                        Product Image - Jacket <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="file"
                          name="imageJacket"
                          id="imageJacket"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.imageJacket}
                        </div>
                      </div>
                    </div>

                    {this.state.imageJacket ? (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={URL.createObjectURL(this.state.imageJacket)}
                            width="100"
                            alt="Jacket preview"
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={`/api/product/photo/jacket/${this.props.match.params.id}`}
                            width="100"
                            alt="Jacket preview"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-0 mt-2 form-group row">
                      <label htmlFor="imageBorder" className="col-form-label">
                        Product Image - Border <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="file"
                          name="imageBorder"
                          id="imageBorder"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.imageBorder}
                        </div>
                      </div>
                    </div>

                    {this.state.imageBorder ? (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={URL.createObjectURL(this.state.imageBorder)}
                            width="100"
                            alt="Border preview"
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={`/api/product/photo/border/${this.props.match.params.id}`}
                            width="100"
                            alt="Border preview"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-0 mt-2 form-group row">
                      <label htmlFor="imageBack" className="col-form-label">
                        Product Image - Back <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="file"
                          name="imageBack"
                          id="imageBack"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.imageBack}
                        </div>
                      </div>
                    </div>

                    {this.state.imageBack ? (
                      <div className="col-md-12 mb-3 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={URL.createObjectURL(this.state.imageBack)}
                            width="100"
                            alt="Back preview"
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 mb-3 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={`/api/product/photo/back/${this.props.match.params.id}`}
                            width="100"
                            alt="Back preview"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-3 mt-2 form-group row">
                      <label htmlFor="fabric" className="col-form-label">
                        Fabric <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <select
                          name="fabric"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.fabric
                          })}
                          id="fabric"
                          value={this.state.fabric}
                          onChange={this.handleChange}
                        >
                          {this.state.fabrics.map((fabrics, i) => (
                            <option key={i} value={fabrics._id}>
                              {fabrics.name}
                            </option>
                          ))}
                        </select>
                        <div className="invalid-feedback">
                          {this.state.errors.fabric}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="color" className="col-form-label">
                        Color <b className="text-danger">*</b>
                      </label>
                      <div className="col-input col-input-color-price">
                        <input
                          type="text"
                          name="color"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.color
                          })}
                          id="color"
                          value={this.state.color}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.color}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="oldPrice" className="col-form-label">
                        Old Price (LKR)
                      </label>
                      <div className="col-input col-input-color-price">
                        <input
                          type="number"
                          name="oldPrice"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.oldPrice
                          })}
                          id="oldPrice"
                          min="0"
                          value={this.state.oldPrice}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.oldPrice}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="newPrice" className="col-form-label">
                        New Price (LKR) <b className="text-danger">*</b>
                      </label>
                      <div className="col-input col-input-color-price">
                        <input
                          type="number"
                          name="newPrice"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.newPrice
                          })}
                          id="newPrice"
                          min="0"
                          value={this.state.newPrice}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.newPrice}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="cost" className="col-form-label">
                        Cost (LKR) <b className="text-danger">*</b>
                      </label>
                      <div className="col-input col-input-color-price">
                        <input
                          type="number"
                          name="cost"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.cost
                          })}
                          id="cost"
                          min="0"
                          value={this.state.cost}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.cost}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label className="col-form-label">
                        Occasion <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <table className="text-muted occation-table">
                          <tbody>
                            <tr>
                              <td width="200">
                                <label htmlFor="occasionCasual">Casual</label>
                              </td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="occasionCasual"
                                  id="occasionCasual"
                                  value={this.state.occasionCasual}
                                  onChange={this.handleChangeCheckBox}
                                  checked={this.state.occasionCasual}
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <label htmlFor="occasionParty">Party</label>
                              </td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="occasionParty"
                                  id="occasionParty"
                                  value={this.state.occasionParty}
                                  onChange={this.handleChangeCheckBox}
                                  checked={this.state.occasionParty}
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <label htmlFor="occasionOffice">office</label>
                              </td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="occasionOffice"
                                  id="occasionOffice"
                                  value={this.state.occasionOffice}
                                  onChange={this.handleChangeCheckBox}
                                  checked={this.state.occasionOffice}
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <label htmlFor="occasionCocktail">
                                  Cocktail
                                </label>
                              </td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="occasionCocktail"
                                  id="occasionCocktail"
                                  value={this.state.occasionCocktail}
                                  onChange={this.handleChangeCheckBox}
                                  checked={this.state.occasionCocktail}
                                />
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <label htmlFor="occasionWedding">
                                  Wedding & Engagement
                                </label>
                              </td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="occasionWedding"
                                  id="occasionWedding"
                                  value={this.state.occasionWedding}
                                  onChange={this.handleChangeCheckBox}
                                  checked={this.state.occasionWedding}
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="washAndCare" className="col-form-label">
                        Wash and Care <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <select
                          name="washAndCare"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.washAndCare
                          })}
                          id="washAndCare"
                          value={this.state.washAndCare}
                          onChange={this.handleChange}
                        >
                          <option></option>
                          <option>Hand Wash Separately In Cold Water</option>
                          <option>Dry Clean Recommended</option>
                        </select>
                        <div className="invalid-feedback">
                          {this.state.errors.washAndCare}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="description" className="col-form-label">
                        Description <b className="text-danger">*</b>
                      </label>
                      <div className="col-input col-input-textarea">
                        <textarea
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.description
                          })}
                          rows="5"
                          name="description"
                          id="description"
                          value={this.state.description}
                          onChange={this.handleChange}
                        ></textarea>
                        <div className="invalid-feedback">
                          {this.state.errors.description}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="keywords" className="col-form-label">
                        Keywords <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="keywords"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.keywords
                          })}
                          id="keywords"
                          value={this.state.keywords}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.keywords}
                        </div>
                      </div>
                    </div>

                    {/* <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="status" className="col-form-label">
                        Activate
                      </label>
                      <div className="col-input">
                        <input
                          type="checkbox"
                          className="mt-3"
                          name="status"
                          id="status"
                          value={this.state.status}
                          onChange={this.handleChangeCheckBox}
                          checked={this.state.status}
                        />
                      </div>
                    </div> */}

                    <div className="col-md-12 mb-0 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <button
                          className="btn-register"
                          type="submit"
                          name="submit"
                        >
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Saving..." : "Save"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-12 my-3 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <b className="text-danger" style={{ fontSize: "14px" }}>
                          * Denotes required.
                        </b>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

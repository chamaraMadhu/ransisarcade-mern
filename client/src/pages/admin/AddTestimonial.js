// import packages
import React, { Component } from "react";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

const initialState = {
  name: "",
  image: "",
  testimonial: "",
  file: "",
  status: false,
  errors: {},
  success: ""
};

export default class AddTestimonial extends Component {
  state = initialState;

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChangeFile = e => {
    if (e.target.files[0]) {
      this.setState({
        image: e.target.files[0],
        file: URL.createObjectURL(e.target.files[0])
      });
    } else {
      this.setState({
        image: "",
        file: ""
      });
    }
  };

  handleSubmit = e => {
    e.preventDefault();

    var bodyFormData = new FormData();
    bodyFormData.set("name", this.state.name);
    bodyFormData.append("image", this.state.image);
    bodyFormData.set("testimonial", this.state.testimonial);

    // set loading
    this.setState({ loading: true });

    axios({
      method: "post",
      url: `/api/testimonial/${isAuthenticated().user._id}`,
      data: bodyFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${isAuthenticated().token}`
      }
    })
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // reset image value
          document.getElementById("image").value = "";

          // set alert message
          this.setState({ success: true, loading: false });
        }
      })
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };

  // close modal
  close = () => {
    this.setState({ success: false, errors: {} });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Testimonial
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Add Testimonial
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">ADD TESTIMONIAL</h6>
                </div>

                {this.state.success ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Testimonial is added successfully.
                  </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                    danger
                    title="Oops, Something went wrong"
                    onConfirm={this.close}
                    confirmBtnStyle={{ background: "#423c59", border: 0 }}
                  >
                    Testimonial isn't added successfully.
                  </SweetAlert>
                ) : null}

                <form onSubmit={this.handleSubmit} className="mt-3" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="name" className="col-form-label">
                        Customer Name <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="name"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.name
                          })}
                          id="name"
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.name}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label htmlFor="image" className="col-form-label">
                        Customer Image<b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="file"
                          name="image"
                          id="image"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.image}
                        </div>
                      </div>
                    </div>

                    {this.state.file && (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={this.state.file}
                            width="100"
                            alt="preview"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-3 form-group row">
                      <label
                        htmlFor="testimonial"
                        className="col-form-label mt-3"
                      >
                        Customer Opinion<b className="text-danger">*</b>
                      </label>
                      <div className="col-input mt-3">
                        <textarea
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.testimonial
                          })}
                          style={{ width: "100%" }}
                          rows="5"
                          name="testimonial"
                          id="testimonial"
                          value={this.state.testimonial}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.testimonial}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <button
                          className="btn-register"
                          type="submit"
                          name="submit"
                        >
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Saving..." : "Save"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-12 mt-2 mb-3 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <b className="text-danger" style={{ fontSize: "14px" }}>
                          * Denotes required.
                        </b>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

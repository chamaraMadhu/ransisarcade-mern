// import packages
import React, { Component } from "react";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

const initialState = {
  city: "",
  fee: "",
  belongsToColombo: 0,
  errors: {},
  success: ""
};

export default class AddDeliveryFee extends Component {
  state = initialState;

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();

    var DeliveryData = {
      city: this.state.city,
      fee: this.state.fee,
      belongsToColombo: this.state.belongsToColombo
    };

    // set loading
    this.setState({ loading: true });

    axios({
      method: "post",
      url: `/api/deliveryFee/add/${isAuthenticated().user._id}`,
      data: DeliveryData,
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    })
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // set alert message
          this.setState({ success: true, loading: false });
        }
      })
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };

  // close modal
  close = () => {
    this.setState({ success: false, errors: {} });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Delivery Fee
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Add Delivery Fee
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">ADD DELIVERY FEE</h6>
                </div>

                {this.state.success ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Delivery Fee is added successfully.
                  </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                    danger
                    title="Oops, Something went wrong"
                    onConfirm={this.close}
                    confirmBtnStyle={{ background: "#423c59", border: 0 }}
                  >
                    Delivery Fee isn't added successfully.
                  </SweetAlert>
                ) : null}

                <form onSubmit={this.handleSubmit} className="mt-3" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="city" className="col-form-label">
                        Delivery City <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="city"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.city
                          })}
                          id="city"
                          value={this.state.city}
                          onChange={this.handleChange}
                        />
                        <div
                          className="invalid-feedback mt-0"
                          style={{ width: "170px" }}
                        >
                          {this.state.errors.city}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="fee" className="col-form-label">
                        Delivery Fee <b className="text-danger">*</b>
                      </label>
                      <div className="col-input col-input-color-price">
                        <input
                          type="number"
                          name="fee"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.fee
                          })}
                          id="fee"
                          value={this.state.fee}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.fee}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label
                        htmlFor="belongsToColombo"
                        className="col-form-label"
                      >
                        Is Belongs to Colombo? <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <select
                          name="belongsToColombo"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.belongsToColombo
                          })}
                          id="belongsToColombo"
                          placeholder="-- select --"
                          value={this.state.belongsToColombo}
                          onChange={this.handleChange}
                        >
                          <option></option>
                          <option value="0">No</option>
                          <option value="1">Yes</option>
                          ))}
                        </select>
                        <div className="invalid-feedback">
                          {this.state.errors.colombo}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <button
                          className="btn-register"
                          type="submit"
                          name="submit"
                        >
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Saving..." : "Save"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-12 mt-2 mb-3 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <b className="text-danger" style={{ fontSize: "14px" }}>
                          * Denotes required.
                        </b>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

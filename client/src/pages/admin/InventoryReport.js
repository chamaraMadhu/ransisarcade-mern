// import packages
import React, { Component } from "react";
import axios from "axios";
import moment from "moment";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";
import Pagination from "../../components/Pagination";

export default class InventoryReport extends Component {
  state = {
    inventory: [],

    // pagination
    currentPage: 1,
    itemsPerPage: 20
  };

  componentDidMount() {
    // get inventory
    axios.get(`/api/inventory/report`).then(inventory => {
      this.setState({
        inventory: inventory.data
      });
    });
  }

  // pagination change page
  handlePage = page => {
    this.setState({ currentPage: page });

    // scroll up to results
    if (window.innerWidth > 575) {
      window.scrollTo({ top: 0, behavior: "smooth" });
    } else {
      window.scrollTo({ top: 2000, behavior: "smooth" });
    }
  };


  render() {
    // calculate the inventory value
    const inventoryValue = this.state.inventory.reduce(
      (currentValue, nextValue) => {
        return currentValue + nextValue.cost;
      },
      0
    );

    // calculate the Total Expected Revenue
    const TotalExpectedRevenue = this.state.inventory.reduce(
      (currentValue, nextValue) => {
        return currentValue + nextValue.newPrice;
      },
      0
    );

        // pagination
    // get current products
    const indexOfLast = this.state.currentPage * this.state.itemsPerPage;
    const indexOfFirst = indexOfLast - this.state.itemsPerPage;
    const currentItems = this.state.inventory.slice(indexOfFirst, indexOfLast);

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Report
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Inventory
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">INVENTORY</h6>
                </div>

                <div className="row">
                  <div className="col">

                  <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                  {this.state.inventory.length > 0
                    ? `Showing ${indexOfFirst + 1} - ${indexOfFirst +
                        currentItems.length} out of ${
                        this.state.inventory.length
                      } results`
                    : "No inventory"}
                </p>

                    <table
                      className="table table-responsive table-bordered"
                      style={{ border: "none" }}
                    >
                      <tbody>
                        <tr className="view-table-tr-color">
                          <td>Created At</td>
                          <td>Product ID</td>
                          <td style={{ minWidth: "350px" }}>Name</td>
                          <td width="175px" align="center">
                            Quantity
                          </td>
                          <td width="175px" align="right">
                            Unit Cost (LKR.){" "}
                          </td>
                          <td width="175px" align="right">
                            Unit Selling Price (LKR.)
                          </td>
                        </tr>
                        {currentItems.map((inventory, i) => (
                          <tr key={i}>
                            <td>
                              {moment(inventory.createdAt).format("DD-MM-YYYY hh:mm:A")}
                            </td>
                            <td>{inventory._id}</td>
                            <td>{inventory.name}</td>
                            <td align="center">{inventory.quantity}</td>
                            <td align="right">{inventory.cost}.00</td>
                            <td align="right">{inventory.newPrice}.00</td>
                          </tr>
                        ))}
                        <tr className="font-weight-bold">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td align="center">{this.state.inventory.length}</td>
                          <td align="right">{inventoryValue}.00</td>
                          <td align="right">{TotalExpectedRevenue}.00</td>
                        </tr>
                      </tbody>
                    </table>
                  
                  {/* starts of pagination */}
                <Pagination
                  className="m-0"
                  noOfAllItems={this.state.inventory.length}
                  itemsPerPage={this.state.itemsPerPage}
                  currentPage={this.state.currentPage}
                  handlePage={this.handlePage}
                />
                {/* ends of pagination */}
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

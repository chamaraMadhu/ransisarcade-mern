// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class ViewCategory extends Component {
  state = {
    activeCategories: [],
    categories: [],
    deleteSuccess: ""
  };

  componentDidMount() {
    // get active categories for hide delete buttomn for them
    axios.get(`/api/products/active/categories`).then(activeCategories => {
      if (activeCategories.data.length > 0) {
        this.setState({
          activeCategories: activeCategories.data
        });
      }
    });

    // get all categories
    axios.get(`/api/categories`).then(categories => {
      if (categories.data.length > 0) {
        this.setState({
          categories: categories.data
        });
      }
    });
  }

  handleDelete = id => {
    axios.delete(`/api/category/${id}/${isAuthenticated().user._id}`, {
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    });

    const filterCategories = this.state.categories.filter(
      categories => categories._id !== id
    );

    this.setState({
      categories: filterCategories,
      deleteSuccess: true
    });

  };

  // close modal
  close = () => {
    this.setState({ deleteSuccess: false });
  };

  render() {
    console.log(this.state.activeCategories);
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Category
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View/Edit/Delete category
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW/EDIT/DELETE CATEGORY</h6>
                </div>

                {this.state.deleteSuccess ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Category has been deleted successfully.
                  </SweetAlert>
                ) : null}


                <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                  {this.state.categories.length > 0
                    ? `Showing ${this.state.categories.length} categories`
                    : "No categories"}
                </p>

                <div className="table-responsive">
                  <table
                    id="zero_config"
                    className="table table-hover table-bordered"
                  >
                    <thead>
                      <tr className="view-table-tr-color">
                        <td width="120">Category ID</td>
                        <td>Category Name</td>
                        <td width="110">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.categories.map((categories, i) => (
                        <tr key={i}>
                          <td>{categories._id}</td>
                          <td>{categories.name}</td>
                          <td>
                            <Link to={"/admin/edit-category/" + categories._id}>
                              <button
                                type="submit"
                                className="btn btn-secondary btn-sm m-1"
                              >
                                <i className="fas fa-pencil-alt"></i>
                              </button>
                            </Link>

                            {`${this.state.activeCategories.filter(filter =>
                              filter === categories._id ? true : false
                            )}` ? null : (
                              <button
                                key={categories._id}
                                type="button"
                                className="btn btn-danger btn-sm m-1"
                                onClick={() =>
                                  this.handleDelete(categories._id)
                                }
                              >
                                <i className="fa fa-trash"></i>
                              </button>
                            )}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

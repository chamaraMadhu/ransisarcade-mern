// import packages
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

const initialState = {
  name: "",
  image: "",
  testimonial: "",
  errors: {},
  success: "",
  loading: false,
  redirectToView: false
};

export default class EditTestimonial extends Component {
  state = initialState;

  componentDidMount = () => {
    axios
      .get(`/api/testimonial/${this.props.match.params.id}`)
      .then(testimonial => {
        this.setState({
          name: testimonial.data.name,
          testimonial: testimonial.data.testimonial
        });
      });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChangeFile = e => {
    this.setState({
      image: e.target.files[0]
    });
  };

  handleSubmit = e => {
    e.preventDefault();

    var bodyFormData = new FormData();
    bodyFormData.set("name", this.state.name);
    bodyFormData.set("testimonial", this.state.testimonial);
    bodyFormData.append("image", this.state.image);

    // set loading
    this.setState({ loading: true });

    axios({
      method: "put",
      url: `/api/testimonial/${this.props.match.params.id}/${
        isAuthenticated().user._id
      }`,
      data: bodyFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${isAuthenticated().token}`
      }
    })
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // reset image value
          document.getElementById("image").value = "";

          // set alert message
          this.setState({ success: true, loading: false });
        }
      })
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };

  // redirect
  ok = () => {
    this.setState({ redirectToView: true });
  };

  // close modal
  close = () => {
    this.setState({ errors: {} });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        {this.state.redirectToView ? (
          <Redirect to="/admin/view-testimonial" />
        ) : null}

        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Testimonial
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Edit Testimonial
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">EDIT TESTIMONIAL</h6>
                </div>

                {this.state.success ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.ok}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Testimonial is updated successfully.
                  </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                    danger
                    title="Oops, Something went wrong"
                    onConfirm={this.close}
                    confirmBtnStyle={{ background: "#423c59", border: 0 }}
                  >
                    Testimonial isn't updated successfully.
                  </SweetAlert>
                ) : null}

                <form onSubmit={this.handleSubmit} className="mt-3" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="name" className="col-form-label">
                        Customer Name <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="name"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.name
                          })}
                          id="name"
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.name}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label htmlFor="image" className="col-form-label">
                        Customer Image<b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="file"
                          name="image"
                          id="image"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.image}
                        </div>
                      </div>
                    </div>

                    {this.state.image ? (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={URL.createObjectURL(this.state.image)}
                            width="100"
                            alt="Front preview"
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={`/api/testimonial/photo/${this.props.match.params.id}`}
                            width="100"
                            alt="Front preview"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-3 form-group row">
                      <label
                        htmlFor="testimonial"
                        className="col-form-label mt-3"
                      >
                        Customer Opinion<b className="text-danger">*</b>
                      </label>
                      <div className="col-input mt-3">
                        <textarea
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.testimonial
                          })}
                          style={{ width: "100%" }}
                          rows="5"
                          name="testimonial"
                          id="testimonial"
                          value={this.state.testimonial}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.testimonial}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <button
                          className="btn-register"
                          type="submit"
                          name="submit"
                        >
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Saving..." : "Save"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-12 mt-2 mb-3 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <b className="text-danger" style={{ fontSize: "14px" }}>
                          * Denotes required.
                        </b>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// import packages
import React, { Component } from "react";
import axios from "axios";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// import CanvasJSReact
import CanvasJSReact from "../../assets/canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class CategoryWiseSoldProducts extends Component {
  state = {
    categories: []
  };

  componentDidMount() {
    axios.get(`/api/report/category-wise-sold-products`).then(categories => {
      console.log(categories);
      this.setState({
        categories: categories.data
      });
    });
  }

  render() {
    // calculate the inventory
    const total = this.state.categories.reduce((currentValue, nextValue) => {
      return currentValue + nextValue.count;
    }, 0);

    let pieValues = [];

    this.state.categories.map((categories, i) =>
      pieValues.push({
        y: ((categories.count / total) * 100).toFixed(2),
        label: categories._id.name
      })
    );

    // CanvasJSReact
    const options = {
      exportEnabled: true,
      animationEnabled: true,
      data: [
        {
          type: "pie",
          startAngle: 75,
          toolTipContent: "<b>{label}</b>: {y}%",
          showInLegend: "true",
          legendText: "{label}",
          indexLabelFontSize: 16,
          indexLabel: "{label} - {y}%",
          dataPoints: pieValues
        }
      ]
    };

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Report
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Category wise Sold Products
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">CATEGORY WISE SOLD PRODUCTS</h6>
                </div>

                <div className="row">
                  <div className="col">
                    <CanvasJSChart options={options} />
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <div className="table-responsive mt-4">
                      <table className="table table-bordered">
                        <tbody>
                          <tr align="center" className="view-table-tr-color">
                            <td width="175px">Category</td>
                            <td width="175px">No. of Sold Products</td>
                            <td width="175px">%</td>
                          </tr>

                          {this.state.categories.map((categories, i) => (
                            <tr align="center" key={i}>
                              <td>{categories._id.name}</td>
                              <td>{categories.count}</td>
                              <td>
                                {((categories.count / total) * 100).toFixed(2)}%
                              </td>
                            </tr>
                          ))}

                          <tr className="font-weight-bold" align="center">
                            <td>Total</td>
                            <td>{total}</td>
                            <td>100.00%</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

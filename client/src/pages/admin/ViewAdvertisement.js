// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class ViewCareer extends Component {
  state = {
    advertisements: [],
    deleteSuccess: false
  };

  componentDidMount() {
    axios.get(`/api/advertisements`).then(advertisements => {
      if (advertisements.data.length > 0) {
        this.setState({
          advertisements: advertisements.data
        });
      }
    });
  }

  handleDelete = id => {
    axios.delete(`/api/advertisement/${id}/${isAuthenticated().user._id}`, {
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    });

    const filterAdvertisements = this.state.advertisements.filter(
      advertisements => advertisements._id !== id
    );

    this.setState({
      advertisements: filterAdvertisements,
      deleteSuccess: true
    });

    setTimeout(() => this.setState({ deleteSuccess: false }), 5000);
  };

   // close modal
   close = () => {
    this.setState({ deleteSuccess: false });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Advertisement
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View Advertisement
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW ADVERTISEMENT</h6>
                </div>

                {this.state.deleteSuccess ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Advertisement has been deleted successfully.
                  </SweetAlert>
                ) : null}

                <div className="table-responsive mt-4">
                  <table
                    id="zero_config"
                    className="table table-hover table-bordered"
                  >
                    <thead>
                      <tr className="view-table-tr-color">
                        <td style={{ maxWidth: "200px" }}>Purpose</td>
                        <td style={{ minWidth: "400px" }}>Advertisement</td>
                        <td style={{ minWidth: "150px" }}>Start</td>
                        <td style={{ minWidth: "150px" }}>End</td>
                        <td style={{ minWidth: "150px" }}>Status</td>
                        <td style={{ width: "110px" }}>Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.advertisements.map((advertisement, i) => (
                        <tr key={i}>
                          <td>{advertisement.purpose}</td>
                          <td align="center">
                            <img
                              src={
                                `/api/advertisement/photo/` + advertisement._id
                              }
                              alt={advertisement.purpose}
                              width="60px"
                              className="py-1"
                            />
                          </td>
                          <td>
                            {advertisement.start ?  advertisement.start : null
                              }
                          </td>
                          <td>
                          {advertisement.end ?  advertisement.end : null
                              }
                          </td>
                          <td>
                            {advertisement.status ? (
                              <span className="text-success">Active</span>
                            ) : (
                              <span className="text-danger">Deactive</span>
                            )}
                          </td>
                          <td>
                            <Link
                              to={"/admin/edit-advertisement/" + advertisement._id}
                              className="btn btn-secondary btn-sm m-1"
                            >
                              <i className="fas fa-pencil-alt"></i>
                            </Link>
                            <button
                              type="button"
                              className="btn btn-danger btn-sm m-1"
                              onClick={() =>
                                this.handleDelete(advertisement._id)
                              }
                            >
                              <i className="fa fa-trash"></i>
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

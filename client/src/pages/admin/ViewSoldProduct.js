// import packages
import React, { Component } from "react";
import axios from "axios";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";
import Pagination from "../../components/Pagination";

export default class ViewSoldProduct extends Component {
  state = {
    // model
    modelId: false,

    productId: "",
    name: "",
    category: "",
    fabric: "",
    imageFront: "",
    imageJacket: "",
    imageBorder: "",
    imageBack: "",
    color: "",
    oldPrice: "",
    newPrice: "",
    cost: "",
    quantity: "",
    likes: "",
    comments: "",
    rating: "",
    occasionCasual: false,
    occasionParty: false,
    occasionOffice: false,
    occasionCocktail: false,
    occasionWedding: false,
    washAndCare: "",
    description: "",
    keywords: "",
    status: false,
    products: [],
    deleteSuccess: false,

    // pagination
    currentPage: 1,
    itemsPerPage: 10
  };

  componentDidMount() {
    // get all products
    axios.get(`/api/sold`).then(products => {
      if (products.data.length > 0) {
        this.setState({
          products: products.data
        });
      }
    });
  }

  // product model
  handelModel = id => {
    this.setState({ modelId: true });

    axios.get(`/api/product/${id}`).then(product => {
      this.setState({
        productId: product.data._id,
        name: product.data.name,
        category: product.data.category.name,
        fabric: product.data.fabric.name,
        oldPrice: product.data.oldPrice,
        newPrice: product.data.newPrice,
        cost: product.data.cost,
        quantity: product.data.quantity,
        color: product.data.color,
        likes: product.data.likes,
        comments: product.data.comments,
        rating: product.data.rating,
        washAndCare: product.data.washAndCare,
        description: product.data.description,
        keywords: product.data.keywords,
        status: product.data.status,
        occasionCasual: product.data.occasionCasual,
        occasionParty: product.data.occasionParty,
        occasionOffice: product.data.occasionOffice,
        occasionCocktail: product.data.occasionCocktail,
        occasionWedding: product.data.occasionWedding
      });
    });
  };

  // pagination change page
  handlePage = page => {
    this.setState({ currentPage: page });

    // scroll up to results
    if (window.innerWidth > 575) {
      window.scrollTo({ top: 0, behavior: "smooth" });
    } else {
      window.scrollTo({ top: 2000, behavior: "smooth" });
    }
  };

  render() {
    // pagination
    // get current products
    const indexOfLast = this.state.currentPage * this.state.itemsPerPage;
    const indexOfFirst = indexOfLast - this.state.itemsPerPage;
    const currentItems = this.state.products.slice(indexOfFirst, indexOfLast);

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Sold Product
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View Sold Products
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW SOLD PRODUCTS</h6>
                </div>

                <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                  {this.state.products.length > 0
                    ? `Showing ${indexOfFirst + 1} - ${indexOfFirst +
                        currentItems.length} out of ${
                        this.state.products.length
                      } results`
                    : "No sold products"}
                </p>

                <div className="table-responsive">
                  <table className="table table-hover table-bordered">
                    <thead>
                      <tr className="view-table-tr-color">
                        <td width="110px">Product ID</td>
                        <td>Product Name</td>
                        <td>Category</td>
                        <td>Fabric</td>
                        <td>Image</td>
                        <td>Price</td>
                        <td>Status</td>
                        <td width="160px">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.products.length > 0 ? (
                        currentItems.map((products, i) => (
                          <tr key={i}>
                            <td>{products._id}</td>
                            <td>{products.name}</td>
                            <td>{products.category.name} </td>
                            <td>{products.fabric.name} </td>
                            <td align="center">
                              <img
                                src={`/api/product/photo/front/${products._id}`}
                                width="60px"
                                className="py-1"
                                alt={products.name}
                              />
                            </td>
                            <td>Rs. {products.newPrice}.00</td>
                            <td>
                              {products.status ? (
                                <span className="text-success">Active</span>
                              ) : (
                                <span className="text-danger">Deactive</span>
                              )}
                            </td>
                            <td>
                              <button
                                onClick={() => this.handelModel(products._id)}
                                className="btn btn-info btn-sm m-1"
                                data-toggle="modal"
                                data-target="#Modal"
                              >
                                <i className="fa fa-info px-1"></i>
                              </button>
                            </td>
                          </tr>
                        ))
                      ) : (
                        <tr>
                          <td colSpan="9" className="text-center">
                            No sold products
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
                {/* starts of pagination */}
                <Pagination
                  className="m-0"
                  noOfAllItems={this.state.products.length}
                  itemsPerPage={this.state.itemsPerPage}
                  currentPage={this.state.currentPage}
                  handlePage={this.handlePage}
                />
                {/* ends of pagination */}
              </div>
            </div>
          </div>
        </div>

        {/* starts of product modal */}
        {this.state.modelId ? (
          <div
            className="modal fade"
            id="Modal"
            // tabindex="-1"
            role="dialog"
            aria-hidden="true"
          >
            <div
              className="modal-dialog"
              role="document"
              aria-labelledby="ModalLabel"
              aria-hidden="true"
            >
              <div className="modal-content">
                <div className="modal-header bg-dark">
                  <h6 className="modal-title text-light" id="exampleModalLabel">
                    Full Details of {this.state.name}
                  </h6>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span className="text-white" aria-hidden="true">
                      &times;
                    </span>
                  </button>
                </div>
                <div className="modal-body text-muted">
                  <p>
                    <b>Product ID:</b> {this.state.productId}
                  </p>
                  <p>
                    <b>Product Name:</b> {this.state.name}
                  </p>
                  <p>
                    <b>Category:</b> {this.state.category}
                  </p>
                  <p>
                    <b>Fabric:</b> {this.state.fabric}
                  </p>
                  <p>
                    <b>Occation:</b>{" "}
                    {this.state.occasionCasual ? "Casual" : null}
                    {this.state.occasionParty ? " Party" : null}
                    {this.state.occasionOffice ? " Office" : null}
                    {this.state.occasionCocktail ? " Cocktail" : null}{" "}
                    {this.state.occasionWedding
                      ? " Wedding and engagement"
                      : null}
                  </p>
                  <p>
                    <b>Image - Front:</b> &nbsp;&nbsp; &nbsp;
                    <img
                      src={`/api/product/photo/front/${this.state.productId}`}
                      width="60px"
                      className="py-1"
                      alt={this.state.name}
                    />
                  </p>
                  <p>
                    <b>Image - Jacket:</b>&nbsp; &nbsp;
                    <img
                      src={`/api/product/photo/jacket/${this.state.productId}`}
                      width="60px"
                      className="py-1"
                      alt={this.state.name}
                    />
                  </p>
                  <p>
                    <b>Image - Border:</b> &nbsp;
                    <img
                      src={`/api/product/photo/border/${this.state.productId}`}
                      width="60px"
                      className="py-1"
                      alt={this.state.name}
                    />
                  </p>
                  <p>
                    <b>Image - Back:</b> &nbsp;&nbsp; &nbsp;
                    <img
                      src={`/api/product/photo/back/${this.state.productId}`}
                      width="60px"
                      className="py-1"
                      alt={this.state.name}
                    />
                  </p>
                  <p>
                    <b>Color:</b> {this.state.color}
                  </p>
                  <p>
                    <b>Old Price:</b> Rs. {this.state.oldPrice}.00
                  </p>
                  <p>
                    <b>New Price:</b> Rs. {this.state.newPrice}.00
                  </p>
                  <p>
                    <b>Cost:</b> Rs. {this.state.cost}.00
                  </p>
                  <p>
                    <b>Quantity:</b> {this.state.quantity}
                  </p>
                  <p>
                    <b>Likes:</b> {this.state.likes}
                  </p>
                  <p>
                    <b>Comments:</b> {this.state.comments}
                  </p>
                  <p>
                    <b>Ratings:</b> {this.state.rating}
                  </p>
                  <p>
                    <b>wash and Care:</b> {this.state.washAndCare}
                  </p>
                  <p>
                    <b>Description:</b> {this.state.description}
                  </p>
                  <p>
                    <b>Keywords:</b> {this.state.keywords}
                  </p>
                  <p>
                    <b>Status:</b>
                    {this.state.status === true ? (
                      <span className="text-success"> Active</span>
                    ) : (
                      <span className="text-danger"> Deactive</span>
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

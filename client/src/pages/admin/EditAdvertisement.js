// import packages
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

const initialState = {
  purpose: "",
  image: "",
  file: "",
  start: "",
  end: "",
  status: false,
  errors: {},
  success: "",
  loading: false,
  redirectToView: false
};

export default class EditAdvertisement extends Component {
  state = initialState;

  componentDidMount = () => {
    axios.get(`/api/advertisement/${this.props.match.params.id}`).then(advertisement => {
        console.log(advertisement)
      this.setState({
        purpose: advertisement.data.purpose,
        start: advertisement.data.start,
        end: advertisement.data.end,
        status: advertisement.data.status,
      });
    });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleChangeFile = e => {
    if (e.target.files[0]) {
      this.setState({
        image: e.target.files[0],
        file: URL.createObjectURL(e.target.files[0])
      });
    } else {
      this.setState({
        image: "",
        file: ""
      });
    }
  };

  // get checkbox value
  handleChangeCheckBox = e => {
    if (e.target.checked) {
      this.setState({ [e.target.name]: true });
    } else {
      this.setState({ [e.target.name]: false });
    }
  };

  // edit
  handleSubmit = e => {
    e.preventDefault();

    var bodyFormData = new FormData();
    bodyFormData.set("purpose", this.state.purpose);
    bodyFormData.append("image", this.state.image);
    bodyFormData.set("status", this.state.status);
    bodyFormData.set("start", this.state.start);
    bodyFormData.set("end", this.state.end);

    // set loading
    this.setState({ loading: true });

    axios({
      method: "put",
      url: `/api/advertisement/${this.props.match.params.id}/${
        isAuthenticated().user._id
      }`,
      data: bodyFormData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${isAuthenticated().token}`
      }
    })
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // reset image value
          document.getElementById("image").value = "";

          // set alert message
          this.setState({ success: true, loading: false });
        }
      })
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };

  // redirect
  ok = () => {
    this.setState({ success: false, redirectToView: true });
  };


  // close modal
  close = () => {
    this.setState({ success: false, errors: {} });
  };

  render() {

console.log(this.state.status)
    return (
      <div className="container-fluid p-0">

        {this.state.redirectToView ? (
          <Redirect to="/admin/view-advertisement" />
        ) : null}

        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Advertisement
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Add Advertisement
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">ADD ADVERTISEMENT</h6>
                </div>

                {this.state.success ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.ok}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Advertisement is added successfully.
                  </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                    danger
                    title="Oops, Something went wrong"
                    onConfirm={this.close}
                    confirmBtnStyle={{ background: "#423c59", border: 0 }}
                  >
                    Advertisement isn't added successfully.
                  </SweetAlert>
                ) : null}

                <form onSubmit={this.handleSubmit} className="mt-3" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="purpose" className="col-form-label">
                        Purpose <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="purpose"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.purpose
                          })}
                          id="purpose"
                          value={this.state.purpose}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.purpose}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="start" className="col-form-label">
                        start <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="date"
                          name="start"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.start
                          })}
                          id="start"
                          value={this.state.start}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.start}
                        </div>
                      </div>
                    </div>

 
                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="end" className="col-form-label">
                        end <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="date"
                          name="end"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.end
                          })}
                          id="end"
                          value={this.state.end}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.end}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label htmlFor="image" className="col-form-label">
                        Image<b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="file"
                          name="image"
                          id="image"
                          accept="image/jpeg"
                          onChange={this.handleChangeFile}
                        />
                        <div className="mt-1 invalid-error">
                          {this.state.errors.image}
                        </div>
                      </div>
                    </div>

                    {this.state.image ? (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={URL.createObjectURL(this.state.image)}
                            width="100"
                            alt="Front preview"
                          />
                        </div>
                      </div>
                    ) : (
                      <div className="col-md-12 mb-0 form-group row">
                        <label className="col-form-label"></label>
                        <div className="col-input">
                          <img
                            src={`/api/advertisement/photo/${this.props.match.params.id}`}
                            width="100"
                            alt="career"
                          />
                        </div>
                      </div>
                    )}

                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="status" className="col-form-label">
                        Activate
                      </label>
                      <div className="col-input">
                        <input
                          type="checkbox"
                          className="mt-3"
                          name="status"
                          id="status"
                          value={this.state.status}
                          onChange={this.handleChangeCheckBox}
                          checked={this.state.status}
                        />
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row mt-2">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <button
                          className="btn-register"
                          type="submit"
                          name="submit"
                        >
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Saving..." : "Save"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-12 mb-3 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <b className="text-danger" style={{ fontSize: "14px" }}>
                          * Denotes required.
                        </b>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

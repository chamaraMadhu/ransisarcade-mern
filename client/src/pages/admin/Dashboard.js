// import packages
import React, { Component } from "react";
import axios from "axios";

// import css
import "../../css/admin/commonStyle.css";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

export default class Dashboard extends Component {
  state = {
    customers: 0,
    inventory: [],
    sold: [],
    notProcessedOrders: 0,
    processingOrders: 0,
    categories: [],
    fabrics: [],
    citiesInColombo: 0,
    citiesInIslandwide: 0,
    testimonials: 0,
    advertisements: 0,
    careers: 0
  };

  componentDidMount = () => {
    axios.get(`/api/customers`).then(customers => {
      this.setState({
        customers: customers.data
      });
    });

    axios.get(`/api/inventory/report`).then(inventory => {
      this.setState({
        inventory: inventory.data
      });
    });

    axios.get(`/api/sold`).then(sold => {
      this.setState({
        sold: sold.data
      });
    });

    axios.get(`/api/notProcessed/count`).then(notProcessedOrders => {
      this.setState({
        notProcessedOrders: notProcessedOrders.data
      });
    });

    axios.get(`/api/processing/count`).then(processingOrders => {
      this.setState({
        processingOrders: processingOrders.data
      });
    });

    axios.get(`/api/categories`).then(categories => {
      this.setState({
        categories: categories.data
      });
    });

    axios.get(`/api/fabrics`).then(fabrics => {
      this.setState({
        fabrics: fabrics.data
      });
    });

    axios.get(`/api/islandwide/count`).then(citiesInIslandwide => {
      this.setState({
        citiesInIslandwide: citiesInIslandwide.data
      });
    });

    axios.get(`/api/colombo/count`).then(citiesInColombo => {
      this.setState({
        citiesInColombo: citiesInColombo.data
      });
    });

    axios.get(`/api/testimonials/count`).then(testimonials => {
      this.setState({
        testimonials: testimonials.data
      });
    });

    axios.get(`/api/advertisements/count`).then(advertisements => {
      this.setState({
        advertisements: advertisements.data
      });
    });

    axios.get(`/api/careers/count`).then(careers => {
      this.setState({
        careers: careers.data
      });
    });
  };

  render() {
    // calculate the inventory value
    const inventoryValue = this.state.inventory.reduce(
      (currentValue, nextValue) => {
        return currentValue + nextValue.cost;
      },
      0
    );
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />
          <div className="col p-0">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <div className="container-fluid bg-white pb-3">
              <div className="row text-center statistics">
                <div className=" box">
                  <p className="amount">{this.state.customers}</p>
                  <p className="stat">Customers</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.inventory.length}</p>
                  <p className="stat">Inventory</p>
                </div>
                <div className=" box">
                  <p className="amount">LKR. {inventoryValue}.00</p>
                  <p className="stat">Inventory Amount</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.sold.length}</p>
                  <p className="stat">Sold</p>
                </div>

                <div className=" box">
                  <p className="amount">{this.state.notProcessedOrders}</p>
                  <p className="stat">Not Processed Orders</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.processingOrders}</p>
                  <p className="stat">Processing Orders</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.categories.length}</p>
                  <p className="stat">Categories</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.fabrics.length}</p>
                  <p className="stat">Fabrics</p>
                </div>

                <div className=" box">
                  <p className="amount">{this.state.citiesInColombo}</p>
                  <p className="stat">Cities in Colombo</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.citiesInIslandwide}</p>
                  <p className="stat">Cities in Islandwide</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.testimonials}</p>
                  <p className="stat">Testimonials</p>
                </div>
                <div className=" box">
                  <p className="amount">{this.state.advertisements}</p>
                  <p className="stat">Advertisements</p>
                </div>

                <div className=" box">
                  <p className="amount">{this.state.careers}</p>
                  <p className="stat">Careers</p>
                </div>
                {/* <div className="">
                  <p className="amount">1</p>
                  <p className="stat">Cities in Islandwide</p>
                </div>
                <div className="">
                  <p className="amount">1</p>
                  <p className="stat">Testimonials</p>
                </div>
                <div className="">
                  <p className="amount">3</p>
                  <p className="stat">Advertisements</p>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

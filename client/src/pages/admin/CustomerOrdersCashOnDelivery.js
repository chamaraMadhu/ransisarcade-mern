// import packages
import React, { Component } from "react";
import axios from "axios";
import moment from "moment"; // npm i moment

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class CustomerOrderCashOnDelivery extends Component {
  state = {
    orders: [],
    status: ""
  };

  componentDidMount() {
    axios
      .get(`/api/orders/cash-on-delivery/${isAuthenticated().user._id}`, {
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
      .then(orders => {
        if (orders.data.length > 0) {
          this.setState({
            orders: orders.data
          });
        }
      });
  }

  handleChange = id => {
    // get status value
    let status = document.getElementById("status").value;

    // assign into status state
    this.setState({ status: status });

    const data = {
      status: status
    };

    axios({
      method: "put",
      url: `/api/order/change/status/${id}/${isAuthenticated().user._id}`,
      data: data,
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    }).then(result => {
      // clear state
      this.setState({ status: "" });

      // to reload with updated status
      axios
        .get(`/api/orders/${isAuthenticated().user._id}`, {
          headers: { Authorization: `Bearer ${isAuthenticated().token}` }
        })
        .then(orders => {
          if (orders.data.length > 0) {
            this.setState({
              orders: orders.data
            });
          }
        });
    });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Customer Orders
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View Orders
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW ORDERS</h6>
                </div>

                {this.state.orders.map((order, i) => (
                  <div key={i} className="orders-div-sec">
                    <h6 className="p-3 bg-dark text-light">
                      <b>Order ID : </b> {order._id}
                    </h6>
                    <ul>
                      <li
                        style={{
                          width: "210px",
                          background: "orange",
                          fontSize: "18px",
                          padding: "7px 10px"
                        }}
                      >
                        <b>Status :</b> {order.status}
                      </li>
                      <li>
                        <select
                          className="form-control my-3"
                          style={{ width: "210px" }}
                          id="status"
                          value={this.state.status}
                          onChange={() => this.handleChange(order._id)}
                        >
                          <option>-- Select Status --</option>
                          <option>Not Processed</option>
                          <option>Processing</option>
                          <option>Delivered</option>
                          <option>Cancelled</option>
                        </select>
                      </li>
                      <li>
                        <b>Reciepient's Name :</b> {order.title}{" "}
                        {order.recipientsName}
                      </li>
                      <li>
                        <b>Amount : </b> Rs. {order.amount}.00
                      </li>
                      <li>
                        <b>Order Date : </b>{" "}
                        {moment(order.transactionTime).fromNow()}
                      </li>
                      <li>
                        <b>Address :</b> {order.address}
                      </li>
                      <li>
                        <b>Phone :</b> {order.phone}
                      </li>
                      <li>
                        <b>Location Type :</b> {order.locationType}
                      </li>
                      <li>
                        <b>Additional Delivery Instructions :</b>{" "}
                        {order.deliveryInstructions}
                      </li>
                      <li>
                        <b>Delivery Fee :</b> Rs. {order.deliveryFee}.00
                      </li>
                    </ul>

                    <table
                      className="table table-responsive"
                      style={{ border: "0" }}
                    >
                      <thead>
                        <tr className="bg-dark text-light">
                          <td>product ID</td>
                          <td>Name</td>
                          <td>Image</td>
                          <td>Price</td>
                          <td>Quantity</td>
                          <td>Color</td>
                        </tr>
                      </thead>
                      <tbody>
                        {order.orderItems.map((orderItem, pIndex) => (
                          <tr key={pIndex}>
                            <td>{orderItem._id}</td>
                            <td>{orderItem.name}</td>
                            <td>
                              <img
                                src={`/api/product/photo/front/${orderItem._id}`}
                                width="80"
                                alt={orderItem.name}
                              />
                            </td>
                            <td>Rs. {orderItem.newPrice}.00</td>
                            <td>{orderItem.quantity}</td>
                            <td>{orderItem.color}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";


// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class ViewTestimonial extends Component {
  state = {
    testimonials: [],
    deleteSuccess: false
  };

  componentDidMount() {
    axios.get(`/api/testimonials`).then(testimonials => {
      if (testimonials.data.length > 0) {
        this.setState({
          testimonials: testimonials.data
        });
      }
    });
  }

  handleDelete = id => {
    axios.delete(`/api/testimonial/${id}/${isAuthenticated().user._id}`, {
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    });

    const filtertestimonials = this.state.testimonials.filter(
      testimonials => testimonials._id !== id
    );

    this.setState({
      testimonials: filtertestimonials,
      deleteSuccess: true
    });

    setTimeout(() => this.setState({ deleteSuccess: false }), 5000);
  };

  // close modal
  close = () => {
    this.setState({ deleteSuccess: false });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Testimonials
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View Testimonials
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW TESTIMONIALS</h6>
                </div>

                {this.state.deleteSuccess ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Teatimponial has been deleted successfully.
                  </SweetAlert>
                ) : null}

                <div className="table-responsive mt-4">
                  <table
                    id="zero_config"
                    className="table table-hover table-bordered"
                  >
                    <thead>
                      <tr className="view-table-tr-color">
                        <td style={{ maxWidth: "200px" }}>Customer Name</td>
                        <td align="center" width="150">
                          Customer Image
                        </td>
                        <td style={{ minWidth: "400px" }}>Customer Opinion</td>
                        <td style={{ width: "110px" }}>Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.testimonials.map((testimonial, i) => (
                        <tr key={i}>
                          <td>{testimonial.name}</td>
                          <td align="center">
                            <img
                              src={
                                `/api/testimonial/photo/` +
                                testimonial._id
                              }
                              alt={testimonial.name}
                              width="60px"
                              className="py-1"
                            />
                          </td>
                          <td>{testimonial.testimonial}</td>
                          <td>
                            <Link
                              to={"/admin/edit-testimonial/" + testimonial._id}
                              className="btn btn-secondary btn-sm m-1"
                            >
                              <i className="fas fa-pencil-alt"></i>
                            </Link>

                            <button
                              type="button"
                              className="btn btn-danger btn-sm m-1"
                              onClick={() => this.handleDelete(testimonial._id)}
                            >
                              <i className="fa fa-trash"></i>
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

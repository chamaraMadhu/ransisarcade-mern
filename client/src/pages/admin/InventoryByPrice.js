// import packages
import React, { Component } from "react";
import axios from "axios";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// import CanvasJSReact
import CanvasJSReact from "../../assets/canvasjs.react";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default class InventoryByPrice extends Component {
  state = {
    priceRange1: "",
    priceRange2: "",
    priceRange3: "",
    priceRange4: "",
    priceRange5: "",
    priceRange6: "",
    priceRange7: "",
    priceRange8: "",
    priceRange9: "",
    inventory: 0
  };

  componentDidMount() {
    axios.get(`/api/products/priceRange/0/2500`).then(priceRange1 => {
      this.setState({
        priceRange1: priceRange1.data
      });
    });

    axios.get(`/api/products/priceRange/2501/5000`).then(priceRange2 => {
      this.setState({
        priceRange2: priceRange2.data
      });
    });

    axios.get(`/api/products/priceRange/5001/7500`).then(priceRange3 => {
      this.setState({
        priceRange3: priceRange3.data
      });
    });

    axios.get(`/api/products/priceRange/7501/10000`).then(priceRange4 => {
      this.setState({
        priceRange4: priceRange4.data
      });
    });

    axios.get(`/api/products/priceRange/10001/15000`).then(priceRange5 => {
      this.setState({
        priceRange5: priceRange5.data
      });
    });

    axios.get(`/api/products/priceRange/15001/20000`).then(priceRange6 => {
      this.setState({
        priceRange6: priceRange6.data
      });
    });

    axios.get(`/api/products/priceRange/20001/25000`).then(priceRange7 => {
      this.setState({
        priceRange7: priceRange7.data
      });
    });

    axios.get(`/api/products/priceRange/25001/35000`).then(priceRange8 => {
      this.setState({
        priceRange8: priceRange8.data
      });
    });

    axios.get(`/api/products/priceRange/35001/50000`).then(priceRange9 => {
      this.setState({
        priceRange9: priceRange9.data
      });
    });

    axios.get(`/api/products/priceRange/50001/100000`).then(priceRange10 => {
      this.setState({
        priceRange10: priceRange10.data
      });
    });

    axios.get(`/api/inventory/report`).then(inventory => {
      this.setState({
        inventory: inventory.data
      });
    });
  }

  render() {
    // total inventory
    const total = this.state.inventory.length;

    let pieValues = [
      {
        y: ((this.state.priceRange1 / total) * 100).toFixed(2),
        label: "LKR 0 - 2500"
      },
      {
        y: ((this.state.priceRange2 / total) * 100).toFixed(2),
        label: "LKR 2501 - 5000"
      },
      {
        y: ((this.state.priceRange3 / total) * 100).toFixed(2),
        label: "LKR 5001 - 7500"
      },
      {
        y: ((this.state.priceRange4 / total) * 100).toFixed(2),
        label: "LKR 7501 - 10000"
      },
      {
        y: ((this.state.priceRange5 / total) * 100).toFixed(2),
        label: "LKR 10001 - 15000"
      },
      {
        y: ((this.state.priceRange6 / total) * 100).toFixed(2),
        label: "LKR 15001 - 20000"
      },
      {
        y: ((this.state.priceRange7 / total) * 100).toFixed(2),
        label: "LKR 20001 - 25000"
      },
      {
        y: ((this.state.priceRange8 / total) * 100).toFixed(2),
        label: "LKR 25001 - 35000"
      },
      {
        y: ((this.state.priceRange9 / total) * 100).toFixed(2),
        label: "LKR 35001 - 50000"
      },
      {
        y: ((this.state.priceRange10 / total) * 100).toFixed(2),
        label: "LKR 50001 - 100000"
      }
    ];

    // CanvasJSReact
    const options = {
      exportEnabled: true,
      animationEnabled: true,
      data: [
        {
          type: "pie",
          startAngle: 75,
          toolTipContent: "<b>{label}</b>: {y}%",
          showInLegend: "true",
          legendText: "{label}",
          indexLabelFontSize: 16,
          indexLabel: "{label} - {y}%",
          dataPoints: pieValues
        }
      ]
    };

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Report
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Price wise Inventory
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">PRICE WISE INVENTORY</h6>
                </div>

                <div className="row">
                  <div className="col">
                    <CanvasJSChart options={options} />
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <table
                      className="table mt-4 table-bordered"
                      style={{ border: "none" }}
                    >
                      <tbody>
                        <tr align="center" className="view-table-tr-color">
                          <td width="175px">Price Range</td>
                          <td width="175px">No. of Inventory</td>
                          <td width="175px">%</td>
                        </tr>

                        <tr align="center">
                          <td>LKR. 0.00 - LKR. 2500.00</td>
                          <td>{this.state.priceRange1}</td>
                          <td>
                            {((this.state.priceRange1 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 2501.00 - LKR. 5000.00</td>
                          <td>{this.state.priceRange2}</td>
                          <td>
                            {((this.state.priceRange2 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 5001.00 - LKR. 7500.00</td>
                          <td>{this.state.priceRange3}</td>
                          <td>
                            {((this.state.priceRange3 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 7501.00 - LKR. 10000.00</td>
                          <td>{this.state.priceRange4}</td>
                          <td>
                            {((this.state.priceRange4 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 10001.00 - LKR. 15000.00</td>
                          <td>{this.state.priceRange5}</td>
                          <td>
                            {((this.state.priceRange5 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 15001.00 - LKR. 20000.00</td>
                          <td>{this.state.priceRange6}</td>
                          <td>
                            {((this.state.priceRange6 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 20001.00 - LKR. 25000.00</td>
                          <td>{this.state.priceRange7}</td>
                          <td>
                            {((this.state.priceRange7 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 25001.00 - LKR. 35000.00</td>
                          <td>{this.state.priceRange8}</td>
                          <td>
                            {((this.state.priceRange8 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 35001.00 - LKR. 50000.00</td>
                          <td>{this.state.priceRange9}</td>
                          <td>
                            {((this.state.priceRange9 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>
                        <tr align="center">
                          <td>LKR. 50001.00 - LKR. 100000.00</td>
                          <td>{this.state.priceRange10}</td>
                          <td>
                            {((this.state.priceRange10 / total) * 100).toFixed(
                              2
                            )}
                            %
                          </td>
                        </tr>

                        <tr className="font-weight-bold" align="center">
                          <td>Total</td>
                          <td>{total}</td>
                          <td>100.00%</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// import packages
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

// initiate state
const initialState = {
  name: "",
  errors: {},
  success: "",
  loading: false,
  redirectToView: false
};

export default class EditCategory extends Component {
  state = initialState;

  componentDidMount() {
    // get edit category
    axios.get(`/api/category/` + this.props.match.params.id).then(category => {
      console.log(category);
      this.setState({
        name: category.data.name
      });
    });
  }

  // get input values
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // save updates
  handleSubmit = e => {
    e.preventDefault();

    let editCategoryData = {
      name: this.state.name
    };

    // set loading
    this.setState({ loading: true });

    // send put req
    axios
      .put(
        `/api/category/${this.props.match.params.id}/${
          isAuthenticated().user._id
        }`,
        editCategoryData,
        {
          headers: {
            Authorization: `Bearer ${isAuthenticated().token}`
          }
        }
      )
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // set alert message
          this.setState({ success: true, loading: false });

          window.location.href = "http://localhost:3000/admin/view-category";
        }
      })
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };

  // redirect
  ok = () => {
    this.setState({ success: false, redirectToView: true });
  };

  // close modal
  close = () => {
    this.setState({ errors: {} });
  };

  render() {
    return (
      <div className="container-fluid p-0">
        {this.state.redirectToView ? (
          <Redirect to="/admin/view-category" />
        ) : null}

        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Category
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Edit Category
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">EDIT CATEGORY</h6>
                </div>

                {this.state.success ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.ok}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Category is updated successfully.
                  </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                    danger
                    title="Oops, Something went wrong"
                    onConfirm={this.close}
                    confirmBtnStyle={{ background: "#423c59", border: 0 }}
                  >
                    Category isn't updated successfully.
                  </SweetAlert>
                ) : null}

                <form onSubmit={this.handleSubmit} className="mt-3" noValidate>
                  <div className="form-row">
                    <div className="col-md-12 mb-3 form-group row">
                      <label htmlFor="name" className="col-form-label">
                        Category name <b className="text-danger">*</b>
                      </label>
                      <div className="col-input">
                        <input
                          type="text"
                          name="name"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.name
                          })}
                          id="name"
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.name}
                        </div>
                      </div>
                    </div>

                    <div className="col-md-12 mb-0 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <button
                          className="btn-register"
                          type="submit"
                          name="submit"
                        >
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Saving..." : "Save"}
                        </button>
                      </div>
                    </div>

                    <div className="col-md-12 mt-2 mb-3 form-group row">
                      <label className="col-form-label"></label>
                      <div className="col-input">
                        <b className="text-danger" style={{ fontSize: "14px" }}>
                          * Denotes required.
                        </b>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";


// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";
import Pagination from "../../components/Pagination";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class ViewFabric extends Component {
  state = {
    deliveryFees: [],
    deleteSuccess: false,

    // pagination
    currentPage: 1,
    itemsPerPage: 25
  };

  componentDidMount() {
    axios.get(`/api/deliveryFees`).then(deliveryFee => {
      if (deliveryFee.data.length > 0) {
        this.setState({
          deliveryFees: deliveryFee.data
        });
      }
    });
  }

  handleDelete = id => {
    axios.delete(`/api/deliveryFee/${id}/${isAuthenticated().user._id}`, {
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    });

    const filterDeliveryFee = this.state.deliveryFees.filter(
      deliveryFee => deliveryFee._id !== id
    );

    this.setState({
      deliveryFees: filterDeliveryFee,
      deleteSuccess: true
    });

    setTimeout(() => this.setState({ deleteSuccess: "" }), 5000);
  };

  // pagination change page
  handlePage = page => {
    this.setState({ currentPage: page });

    // scroll up to results
    if (window.innerWidth > 575) {
      window.scrollTo({ top: 0, behavior: "smooth" });
    } else {
      window.scrollTo({ top: 2000, behavior: "smooth" });
    }
  };

   // close modal
   close = () => {
    this.setState({ deleteSuccess: false });
  };

  render() {
    // pagination
    // get current products
    const indexOfLast = this.state.currentPage * this.state.itemsPerPage;
    const indexOfFirst = indexOfLast - this.state.itemsPerPage;
    const currentItems = this.state.deliveryFees.slice(
      indexOfFirst,
      indexOfLast
    );

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Delivery Fee
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View Delivery Fee
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW DELIVERY FEE</h6>
                </div>

                {this.state.deleteSuccess ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Delivery fee has been deleted successfully.
                  </SweetAlert>
                ) : null}

                <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                  {this.state.deliveryFees.length > 0
                    ? `Showing ${indexOfFirst + 1} - ${indexOfFirst +
                        currentItems.length} out of ${
                        this.state.deliveryFees.length
                      } results`
                    : "No delivery Fees"}
                </p>

                <div className="table-responsive">
                  <table
                    id="zero_config"
                    className="table table-hover table-bordered"
                  >
                    <thead>
                      <tr className="view-table-tr-color">
                        <td style={{ minWidth: "250px" }}>Delivery City</td>
                        <td>Delivery Fee</td>
                        <td style={{ minWidth: "200px", textAlign: "center" }}>
                          Is Belongs to Colombo
                        </td>
                        <td width="110">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {currentItems.map((deliveryFee, i) => (
                        <tr key={i}>
                          <td>{deliveryFee.city}</td>
                          <td>Rs. {deliveryFee.fee}.00</td>
                          <td style={{ textAlign: "center" }}>
                            {deliveryFee.belongsToColombo === 0 ? "No" : "Yes"}
                          </td>
                          <td>
                            <Link
                              to={"/admin/edit-delivery-fee/" + deliveryFee._id}
                              className="btn btn-secondary btn-sm m-1"
                            >
                              <i className="fas fa-pencil-alt"></i>
                            </Link>

                            <button
                              type="button"
                              className="btn btn-danger btn-sm m-1"
                              onClick={() => this.handleDelete(deliveryFee._id)}
                            >
                              <i className="fa fa-trash"></i>
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
                {/* starts of pagination */}
                <Pagination
                  className="m-0"
                  noOfAllItems={this.state.deliveryFees.length}
                  itemsPerPage={this.state.itemsPerPage}
                  currentPage={this.state.currentPage}
                  handlePage={this.handlePage}
                />
                {/* ends of pagination */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

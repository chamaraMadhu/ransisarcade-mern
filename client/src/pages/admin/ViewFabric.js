// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class ViewFabric extends Component {
  state = {
    activeFabrics: [],
    fabrics: [],
    deleteSuccess: ""
  };

  componentDidMount() {
    // get active fabrics for hide delete buttomn for them
    axios.get(`/api/products/active/fabrics`).then(activeFabrics => {
      if (activeFabrics.data.length > 0) {
        this.setState({
          activeFabrics: activeFabrics.data
        });
      }
    });

    // get all fabrics
    axios.get(`/api/fabrics`).then(fabrics => {
      if (fabrics.data.length > 0) {
        this.setState({
          fabrics: fabrics.data
        });
      }
    });
  }

  // delete fabric
  handleDelete = id => {
    // send delete request
    axios.delete(`/api/fabric/${id}/${isAuthenticated().user._id}`, {
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    });

    const filterFabrics = this.state.fabrics.filter(
      fabrics => fabrics._id !== id
    );

    this.setState({
      fabrics: filterFabrics,
      deleteSuccess: true
    });

    setTimeout(() => this.setState({ deleteSuccess: "" }), 5000);
  };

   // close modal
   close = () => {
    this.setState({ deleteSuccess: false });
  };

  render() {
    let x = this.state.activeFabrics.filter(filter => console.log(filter));

    console.log(x);
    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Fabric
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  View/Edit/Delete Fabric
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">VIEW/EDIT/DELETE FABRIC</h6>
                </div>

                {this.state.deleteSuccess ? (
                  <SweetAlert
                    success
                    title="Successfull"
                    onConfirm={this.close}
                    confirmBtnText="Ok"
                    confirmBtnStyle={{
                      background: "#423c59",
                      border: 0,
                      width: 70
                    }}
                  >
                    Fabric has been deleted successfully.
                  </SweetAlert>
                ) : null}

                <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                  {this.state.fabrics.length > 0
                    ? `Showing ${this.state.fabrics.length} fabrics`
                    : "No fabrics"}
                </p>

                <div className="table-responsive">
                  <table
                    id="zero_config"
                    className="table table-hover table-bordered"
                  >
                    <thead>
                      <tr className="view-table-tr-color">
                        <td>Fabric Name</td>
                        <td>Fabric ID</td>
                        <td width="110">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.fabrics.map((fabrics, i) => (
                        <tr key={i}>
                          <td>{fabrics._id}</td>
                          <td>{fabrics.name}</td>
                          <td>
                            <Link
                              to={"/admin/edit-fabric/" + fabrics._id}
                              className="btn btn-secondary btn-sm m-1"
                            >
                              <i className="fas fa-pencil-alt"></i>
                            </Link>

                            {`${this.state.activeFabrics.filter(filter =>
                              filter === fabrics._id ? true : false
                            )}` ? null : (
                              <button
                                key={fabrics._id}
                                type="button"
                                className="btn btn-danger btn-sm m-1"
                                onClick={() => this.handleDelete(fabrics._id)}
                              >
                                <i className="fa fa-trash"></i>
                              </button>
                            )}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// import packages
import React, { Component } from "react";
import axios from "axios";
import moment from "moment";

// import components
import Navbar from "../../components/admin/Navbar";
import Sidebar from "../../components/admin/Sidebar";
import Pagination from "../../components/Pagination";

export default class CustomerReport extends Component {
  state = {
    customers: [],

    // pagination
    currentPage: 1,
    itemsPerPage: 20
  };

  componentDidMount = () => {
    axios.get(`/api/customer/report`).then(customers => {
      this.setState({
        customers: customers.data
      });
    });
  };

  // pagination change page
  handlePage = page => {
    this.setState({ currentPage: page });

    // scroll up to results
    if (window.innerWidth > 575) {
      window.scrollTo({ top: 0, behavior: "smooth" });
    } else {
      window.scrollTo({ top: 2000, behavior: "smooth" });
    }
  };

  render() {
    // pagination
    // get current products
    const indexOfLast = this.state.currentPage * this.state.itemsPerPage;
    const indexOfFirst = indexOfLast - this.state.itemsPerPage;
    const currentItems = this.state.customers.slice(indexOfFirst, indexOfLast);

    return (
      <div className="container-fluid p-0">
        <Navbar />
        <div className="row m-0">
          <Sidebar />

          <div className="content-col">
            <div className="row m-0">
              <div className="col">
                <i className="fas fa-chevron-up rounded-circle" id="btn"></i>
              </div>
            </div>

            <nav aria-label="breadcrumb" className="inner-breadcrumb">
              <ol className="breadcrumb rounded-0 mb-0">
                <li className="breadcrumb-item" aria-current="page">
                  Report
                </li>
                <li className="breadcrumb-item" aria-current="page">
                  Customers
                </li>
              </ol>
            </nav>

            <div className="container-fluid inner-content py-md-3">
              <div className="container-fluid bg-white pb-3">
                <div className="row pt-2 page-heading-sec">
                  <h6 className="col-12">CUSTOMERS</h6>
                </div>

                <div className="row">
                  <div className="col">
                    <p className="pl-1 mb-2 mt-4" style={{ fontWeight: "600" }}>
                      {this.state.customers.length > 0
                        ? `Showing ${indexOfFirst + 1} - ${indexOfFirst +
                            currentItems.length} out of ${
                            this.state.customers.length
                          } results`
                        : "No customers"}
                    </p>

                    <div className="table-responsive">
                      <table id="zero_config" className="table table-bordered">
                        <tbody>
                          <tr className="view-table-tr-color">
                            <td>Created At</td>
                            <td>Customer Image</td>
                            <td>Customer Name</td>
                            <td>Email</td>
                            <td>Mobile</td>
                          </tr>
                          {currentItems.map((customer, i) => (
                            <tr key={i}>
                              <td>
                                {moment(customer.createdAt).format("DD-MM-YYYY hh:mm:A")}
                                {/* DD-MM-YYYY hh:mm a */}
                              </td>
                              <td>
                                <img
                                  src={customer.image}
                                  style={{
                                    width: "50px",
                                    height: "50px",
                                    borderRadius: "50%"
                                  }}
                                  alt={customer.fName}
                                />
                              </td>
                              <td>
                                {customer.fName} {customer.lName}
                              </td>
                              <td>{customer.email}</td>
                              <td>{customer.mobile}</td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>

                {/* starts of pagination */}
                <Pagination
                  className="m-0"
                  noOfAllItems={this.state.customers.length}
                  itemsPerPage={this.state.itemsPerPage}
                  currentPage={this.state.currentPage}
                  handlePage={this.handlePage}
                />
                {/* ends of pagination */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// import packages
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import classnames from "classnames";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";

// import css
import "../../css/client/UserLogin.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class Login extends Component {
  state = {
    email: "nlcm.gunathilaka@gmail.com", // nlcm.gunathilaka@gmail.com
    password: "Chathurani1@", // Chathurani1@
    emailForForgetPassword: "",
    errors: {},
    loginFail: false,
    forgetPasswordSuccess: false,
    forgetPasswordFail: false,
    redirectTo: false,
    loading: false
  };

  // handle inputs
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // login
  handleSubmit = e => {
    e.preventDefault();

    // create login data object
    const loginCredentials = {
      email: this.state.email,
      password: this.state.password
    };

    // set loading
    this.setState({ loading: true });

    // send post request
    axios
      .post(`/api/login`, loginCredentials)
      .then(result => {
        if (result) {
          // set jwt on local storage
          if (typeof window !== "undefined") {
            localStorage.setItem("jwt", JSON.stringify(result.data));
          }

          // clear form
          this.setState({
            email: "",
            password: "",
            redirectTo: true,
            errors: {},
            loading: false
          });

          // this.props.history.push("/");
        }
      })
      .catch(err => {
        this.setState({ errors: err.response.data, loading: false });

        if (err.response.data.message) {
          // fail state
          this.setState({ loginFail: true });
        }
      });
  };

  // handle forget password
  handleForgetPassword = e => {
    e.preventDefault();

    // send post req for forget password
    axios
      .post(`/api/forget/password`, {
        emailForForgetPassword: this.state.emailForForgetPassword
      })
      .then(result => {
        this.setState({
          forgetPasswordSuccess: true,
          emailForForgetPassword: ""
        });

        // close the model
        document.getElementById("forgetpasswordModel").className = "modal fade";
        document.querySelector(".modal-backdrop").style.display = "none";
      })
      .catch(err => {
        // assign errors
        this.setState({ errors: err.response.data });

        if (err.response.data.fail) {
          this.setState({
            forgetPasswordFail: true,
            emailForForgetPassword: ""
          });

          // clear alert message
          setTimeout(() => this.setState({ forgetPasswordFail: false }), 5000);
        }
      });
  };

  // close modal
  close = () => {
    this.setState({ loginFail: false, forgetPasswordSuccess: false });
  };

  render() {
    // get user
    const { user } = isAuthenticated();

    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Member Login - Ransis Arcade" />

        <Navbar />

        {/* starts of jumbotron */}
        <div className="jumbotron jumbotron-about mb-0">
          <div className="container">
            <div className="pt-5">
              <h2 className="text-center" style={{ color: "#fff" }}>
                Login
              </h2>
            </div>
          </div>
        </div>
        {/* ends of jumbotron */}

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-4">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">Login</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container">
          {this.state.redirectTo ? (
            user.role === 1 || user.role === 2 ? (
              <Redirect to="/admin/dashboard" />
            ) : (
              <Redirect to="/user-account" />
            )
          ) : null}

          {this.state.loginFail ? (
            <SweetAlert
              danger
              title="Oops, Something went wrong"
              onConfirm={this.close}
              confirmBtnStyle={{ background: "#423c59", border: 0, width: 70 }}
            >
              Invalid username or password!
            </SweetAlert>
          ) : null}

          {this.state.forgetPasswordSuccess ? (
            <SweetAlert
              info
              title="Successfull"
              onConfirm={this.close}
              confirmBtnText="Ok"
              confirmBtnStyle={{ background: "#423c59", border: 0, width: 70 }}
            >
              New password has been sent to your given mail successfully.
            </SweetAlert>
          ) : null}

          <form noValidate>
            <div className="form-group p-0">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.email
                })}
                id="email"
                name="email"
                onChange={this.handleChange}
                value={this.state.email}
              />
              <div className="invalid-feedback">{this.state.errors.email}</div>
            </div>
            <div className="form-group p-0 mb-0">
              <label htmlFor="inputPassword4">Password</label>
              <input
                type="password"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.password
                })}
                id="pwd"
                name="password"
                onChange={this.handleChange}
                value={this.state.password}
              />
              <div className="invalid-feedback">
                {this.state.errors.password}
              </div>
            </div>
            <Link
              to="forget-password"
              data-toggle="modal"
              data-target="#forgetpasswordModel"
              style={{ fontSize: "14px" }}
            >
              Forgot your Password?
            </Link>
            <button
              type="submit"
              className="btn-register mt-4"
              onClick={this.handleSubmit}
            >
              {this.state.loading ? (
                <div
                  className="spinner-border spinner-border-sm text-light"
                  role="status"
                >
                  <span className="sr-only">Loading...</span>
                </div>
              ) : null}
              {this.state.loading ? " Logging..." : <span> Login</span>}
            </button>
          </form>
          <p className="mt-2">
            Please click here to <Link to="/register">register now</Link>
          </p>

          {/* starts of model */}
          <div
            className="modal fade"
            id="forgetpasswordModel"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="forgetPassword"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">&times;</span>
                  </button>

                  <h4>Forgot Password</h4>
                  <p>Please enter your email you use for logging in</p>

                  {this.state.forgetPasswordFail ? (
                    <div
                      className="alert alert-danger alert-dismissible fade show"
                      role="alert"
                    >
                      There's no such an email in our database.
                    </div>
                  ) : null}

                  <input
                    type="email"
                    name="emailForForgetPassword"
                    className={classnames("form-control", {
                      "is-invalid": this.state.errors.emailForForgetPassword
                    })}
                    placeholder="Email"
                    value={this.state.emailForForgetPassword}
                    onChange={this.handleChange}
                  />
                  <div className="invalid-feedback">
                    {this.state.errors.emailForForgetPassword}
                  </div>
                  <button
                    className="btn-register w-100 mt-4"
                    onClick={this.handleForgetPassword}
                  >
                    Submit
                  </button>
                </div>
              </div>
            </div>
          </div>
          {/* ends of model */}
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

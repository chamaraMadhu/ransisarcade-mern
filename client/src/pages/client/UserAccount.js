// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";

// import css
import "../../css/client/userAccountStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import UserAccountSidebar from "../../components/client/UserAccountSidebar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class UserAccount extends Component {
  render() {
    // get authenticated user info
    const {
      user: { fName, lName, mobile, email }
    } = isAuthenticated();

    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="My Profile - Ransis Arcade" />

        <Navbar />
        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-0">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">Customer Account</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container my-3">
          <div className="row">
            <UserAccountSidebar />

            <div className="bg-light px-0 profile-content">
              <h5>Personal Details</h5>
              <div className="mb-4">
                <table className="profile-table">
                  <tbody>
                    <tr>
                      <td>First Name</td>
                      <td>{fName}</td>
                    </tr>
                    <tr>
                      <td>Last Name</td>
                      <td>{lName}</td>
                    </tr>
                    <tr>
                      <td>Mobile No</td>
                      <td>(+94) {mobile}</td>
                    </tr>
                    <tr>
                      <td>E-mail</td>
                      <td>{email}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

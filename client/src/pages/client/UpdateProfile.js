// import packages
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import InputMask from "react-input-mask";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/client/Navbar";
import UserAccountSidebar from "../../components/client/UserAccountSidebar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

// initiate state
const initialState = {
  fName: "",
  lName: "",
  mobile: "",
  errors: {},
  success: false,
  fail: false,
  directToUserAccount: false
};

export default class UpdateProfile extends Component {
  state = initialState;

  componentWillMount = () => {
    this.setState({
      fName: isAuthenticated() && isAuthenticated().user.fName,
      lName: isAuthenticated() && isAuthenticated().user.lName,
      mobile: isAuthenticated() && isAuthenticated().user.mobile
    });
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = () => {
    let mobile;

    if (this.state.mobile.length !== 9) {
      console.log(this.state.mobile.length);
      mobile = this.state.mobile.split("(+94)");
      mobile = mobile[1].split(" ");
      mobile = mobile[1] + mobile[2] + mobile[3];
    } else {
      mobile = this.state.mobile;
    }

    const userData = {
      fName: this.state.fName,
      lName: this.state.lName,
      mobile: mobile
    };

    console.log(userData);

    axios
      .put(`/api/user/update/${isAuthenticated().user._id}`, userData, {
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
      .then(user => {
        if (user) {
          if (typeof window !== "undefined") {
            if (localStorage.getItem("jwt")) {
              let auth = JSON.parse(localStorage.getItem("jwt"));

              // update user data
              auth.user = user.data;

              // set updated jwt
              localStorage.setItem("jwt", JSON.stringify(auth));
            }
          }

          this.setState(initialState);
          this.setState({ success: true });
        }
      })
      .catch(err => this.setState({ errors: err.response.data }));
  };

  // close modal
  close = () => {
    this.setState({ success: false, errors: {} });
  };

  render() {
    if (this.state.directToUserAccount) {
      return <Redirect to="/user-account" />;
    }
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Update My Account- Ransis Arcade" />

        <Navbar />
        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-0">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/user-account" className="breadcrumb-item-text-bold">
                Customer Account
              </Link>
            </li>
            <li className="breadcrumb-item">Update Profile</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container my-3">
          <div className="row">
            <UserAccountSidebar />

            <div className="bg-light px-0 profile-content">
              <h5>UPDATE YOUR PROFILE</h5>
              <div className="mb-4">
                {this.state.success ? (
                    <SweetAlert
                      success
                      title="Successfull"
                      onConfirm={this.close}
                      confirmBtnText="Ok"
                      confirmBtnStyle={{
                        background: "#423c59",
                        border: 0,
                        width: 70
                      }}
                    >
                      your account has been updated successfully.
                    </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                  warning
                  title="Oops, Something went wrong!"
                  onConfirm={this.close}
                  confirmBtnText="Ok"
                  confirmBtnStyle={{
                    background: "#423c59",
                    border: 0,
                    width: 70
                  }}
                >
                  your account hasn't been updated successfully.
                </SweetAlert>
                ) : null}

                <table className="profile-table">
                  <tbody>
                    <tr>
                      <td>First Name</td>
                      <td>
                        <input
                          type="text"
                          name="fName"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.fName
                          })}
                          value={this.state.fName}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.fName}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Last Name</td>
                      <td>
                        <input
                          type="text"
                          name="lName"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.lName
                          })}
                          value={this.state.lName}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.lName}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Mobile No</td>
                      <td>
                        <InputMask
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.mobile
                          })}
                          id="mobile"
                          name="mobile"
                          value={this.state.mobile}
                          onChange={this.handleChange}
                          mask="(+\94) 99 999 9999"
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.mobile}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>E-mail</td>
                      <td>
                        {isAuthenticated() && isAuthenticated().user.email}
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <button
                          className="btn-register"
                          type="button"
                          onClick={this.handleSubmit}
                        >
                          Update Now
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

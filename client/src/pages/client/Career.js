// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

// import css
import "../../css/client/careerStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

export default class Career extends Component {
  state = {
    careers: []
  };

  componentDidMount = () => {
    // get careers
    axios.get(`/api/careers`).then(careers => {
      if (careers.data.length > 0) {
        this.setState({
          careers: careers.data
        });
      }
    });
  };
  render() {
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Career - Ransis Arcade" />

        <Navbar />
        {/* starts of jumbotron */}
        <div class="jumbotron jumbotron-career mb-0">
          <div class="container">
            <div class="pt-5">
              <h2 class="text-center" style={{ color: "#fff" }}>
                Career
              </h2>
            </div>
          </div>
        </div>
        {/* ends of jumbotron */}

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-4">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">Career</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container">
          <section className="text-center mb-5">
            <h3 className="second-color mb-3 mt-3">Join Our Family</h3>
            <p>
              <span
                style={{
                  fontSize: "14px",
                  fontWeight: "500",
                  color: "#6D6D6D"
                }}
              >
                If you are interested to be part of the latest growing
                e-commerce saree shop in Sri Lanka, then please join us!
              </span>
            </p>

            <table className="table table-striped text-left mt-5">
              <thead>
                <tr>
                  <th scope="col">Job Type</th>
                  <th scope="col">Position</th>
                  <th scope="col" width="250px">
                    Job Advertisement
                  </th>
                  <th scope="col">Closing Date</th>
                </tr>
              </thead>
              <tbody>
                {this.state.careers.map((career, i) => (
                  <tr>
                    <td>{career.jobType}</td>
                    <td>{career.position}</td>
                    <td className="text-center">
                      <a
                        href={`/api/career/photo/` + career._id}
                        target="_blank"
                      >
                        <img
                          src={`/api/career/photo/` + career._id}
                          alt={career.position}
                          width="60px"
                          className="py-1"
                        />
                      </a>
                    </td>
                    <td>{career.closingDate}</td>
                  </tr>
                ))}
                {this.state.careers.length < 1 ? (
                  <tr>
                    <td colspan="4" align="center">
                      There are no current vacancies.
                    </td>
                  </tr>
                ) : null}
              </tbody>
            </table>
          </section>
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

// const style = {
//   backgroundImage: "url(./career.jpg)",
//   height: "250px"
// };

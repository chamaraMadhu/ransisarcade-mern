// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import InputMask from "react-input-mask";
import SweetAlert from "react-bootstrap-sweetalert";

// import css
import "../../css/client/RegisterStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// initiate state
const initialState = {
  fName: "",
  lName: "",
  email: "",
  mobile: "",
  password: "",
  confirmPassword: "",
  errors: {},
  success: false,
  loading: false
};

export default class Register extends Component {
  state = initialState;

  // handle inputs
  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // submit registration form
  handleSubmit = e => {
    e.preventDefault();

    // get mobile number and splited to create 9 value number for validate through
    let mobile;

    if (this.state.mobile !== "") {
      mobile = this.state.mobile.split("(+94)");
      mobile = mobile[1].split(" ");
      mobile = mobile[1] + mobile[2] + mobile[3];
    } else {
      mobile = this.state.mobile;
    }

    // create user data object
    const user = {
      fName: this.state.fName,
      lName: this.state.lName,
      email: this.state.email,
      role: 0,
      mobile: mobile,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword
    };

    // set loading
    this.setState({ loading: true });

    // send post req to register an user
    axios
      .post(`/api/register`, user)
      .then(result => {
        if (result) {
          // create object for send mail
          const userRegistrationData = {
            fName: result.data.fName,
            lName: result.data.lName,
            email: result.data.email,
            createdAt: result.data.createdAt
          };

          // thanking for registration
          axios
            .post(`/api/send/email/registration`, userRegistrationData)
            .then(result => console.log("sent mail"))
            .catch(err => console.log(err));

          // clear form
          this.setState(initialState);

          // set alert message
          this.setState({ success: true, loading: false });
        }
      })
      .catch(err =>
        this.setState({ errors: err.response.data, loading: false })
      );
  };

  // close modal
  close = () => {
    this.setState({ success: false });
  };

  render() {
    return (
      <React.Fragment>
        {this.state.success ? (
          <SweetAlert
            success
            title="Successfull"
            onConfirm={this.close}
            confirmBtnText="Ok"
            confirmBtnStyle={{ background: "#423c59", border: 0, width: 70 }}
          >
            You has been registered successfully.
          </SweetAlert>
        ) : null}

        <PageTitle title="Member Registration- Ransis Arcade" />

        <Navbar />

        {/* starts of jumbotron */}
        <div className="jumbotron jumbotron-about mb-0">
          <div className="container">
            <div className="pt-5">
              <h2 className="text-center" style={{ color: "#fff" }}>
                Register
              </h2>
            </div>
          </div>
        </div>
        {/* ends of jumbotron */}

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-4">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">Register</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container">
          <form onSubmit={this.handleSubmit} noValidate>
            <div className="form-group p-0 mt-4">
              <label htmlFor="fName">
                First Name<span className="text-danger">*</span>
              </label>
              <input
                type="text"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.fName
                })}
                id="fName"
                name="fName"
                value={this.state.fName}
                onChange={this.handleChange}
              />
              <div className="invalid-feedback">{this.state.errors.fName}</div>
            </div>
            <div className="form-group p-0">
              <label htmlFor="lName">
                Last Name<span className="text-danger">*</span>
              </label>
              <input
                type="text"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.lName
                })}
                id="lName"
                name="lName"
                value={this.state.lName}
                onChange={this.handleChange}
              />
              <div className="invalid-feedback">{this.state.errors.lName}</div>
            </div>
            <div className="form-group p-0">
              <label htmlFor="email">
                Email<span className="text-danger">*</span>
              </label>
              <input
                type="email"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.email
                })}
                id="email"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
              />
              <div className="invalid-feedback">{this.state.errors.email}</div>
            </div>
            <div className="form-group p-0">
              <label htmlFor="mobile">
                Mobile number<span className="text-danger">*</span>
              </label>
              <InputMask
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.mobile
                })}
                id="mobile"
                name="mobile"
                value={this.state.mobile}
                onChange={this.handleChange}
                mask="(+\94) 99 999 9999"
              />
              <p className="input-guidance mb-0">
                Eg: Mobile Format (+94) 7X XXX XXXX
              </p>
              <div className="invalid-feedback">{this.state.errors.mobile}</div>
            </div>

            <div className="form-group p-0">
              <label htmlFor="pwd">
                Password<span className="text-danger">*</span>
              </label>
              <input
                type="password"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.password
                })}
                id="pwd"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
              />
              <div className="invalid-feedback">
                {this.state.errors.password}
              </div>
            </div>
            <div className="form-group p-0">
              <label htmlFor="confirPwd">
                Confirm Password<span className="text-danger">*</span>
              </label>
              <input
                type="password"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.confirmPassword
                })}
                id="confirPwd"
                name="confirmPassword"
                value={this.state.confirmPassword}
                onChange={this.handleChange}
              />
              <div className="invalid-feedback">
                {this.state.errors.confirmPassword}
              </div>
            </div>
            <button type="submit" className="btn-register mb-4">
              {this.state.loading ? (
                <div
                  className="spinner-border spinner-border-sm text-light"
                  role="status"
                >
                  <span className="sr-only">Loading...</span>
                </div>
              ) : null}
              {this.state.loading ? " Registering..." : <span> Register</span>}
            </button>
          </form>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

// import css
import "../../css/client/cartStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load addToCart methord from cartMethods
import { addToCart } from "../../components/client/CartMethods";

// load wishlist methords from cartMethods
import { removeWishlistItem } from "../../methods/wishlistMethods";

export default class Wishlist extends Component {
  state = {
    wishlistItems: []
  };

  componentDidMount = () => {
    if (typeof window !== "undefined") {
      if (localStorage.getItem("wishlist")) {
        const wishlist = JSON.parse(localStorage.getItem("wishlist"));

        if (wishlist.length > 0) {
          let arr = [];
          let insertId;

          for (let i = 0; i < wishlist.length; i++) {
            insertId = wishlist[i]._id;

            arr = [...arr, insertId];
          }

          axios
            .post(`/api/filter/wishlist/products`, { wishlistItemsIds: arr })
            .then(result => {
              if (result.data.length > 0) {
                this.setState({ wishlistItems: result.data });

                // store cart on local storage
                localStorage.setItem("wishlist", JSON.stringify(result.data));
              } else {
                localStorage.removeItem("wishlist");
              }
            })
            .catch(err => console.log(err));
        }
      }
    }
  };

  // remove items from wishlist
  handleRemoveWishlistItem = id => {
    // execute removeWishlistItem methord
    removeWishlistItem(id);

    // update the wishlist
    const wishlist = JSON.parse(localStorage.getItem("wishlist"));
    this.setState({ wishlistItems: wishlist });
  };

  // add to cart
  handleAddToCart = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let cartItem = products.data;

      // add to cart and remove item from wishlist
      addToCart(cartItem, () => {
        // remove item
        removeWishlistItem(id);

        // update the wishlist
        const wishlist = JSON.parse(localStorage.getItem("wishlist"));
        this.setState({ wishlistItems: wishlist });
      });
    });
  };

  render() {
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Wishlist - Ransis Arcade" />

        <Navbar />
        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-5">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/shop" className="breadcrumb-item-text-bold">
                Shop
              </Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              Wishlist
            </li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container mb-5" style={{ minHeight: "35vh" }}>
          <h4 className="text-center mt-5 mb-4 second-color">Wishlist</h4>
          {this.state.wishlistItems && this.state.wishlistItems.length > 0 ? (
            <div>
              <div className="row m-0">
                <div className="col p-0" style={{ overflowX: "auto" }}>
                  <table className="w-100">
                    <tbody>
                      <tr
                        className="text-light bg-dark"
                        style={{ height: "40px" }}
                      >
                        <td></td>
                        <td className="column-image">Product</td>
                        <td className="column-name">Name</td>
                        <td className="column-color">Color</td>
                        <td className="column-price">Price</td>
                        <td className="column-qty">Qty</td>
                        <td className="column-subtotal pr-3"></td>
                      </tr>

                      {this.state.wishlistItems.map((wishlistItem, i) => (
                        <tr className="text-muted border-bottom" key={i}>
                          <td>
                            <i
                              className="fa fa-trash-alt btn remove-btn"
                              aria-hidden="true"
                              onClick={() =>
                                this.handleRemoveWishlistItem(wishlistItem._id)
                              }
                              style={{ cursor: "pointer" }}
                            ></i>
                          </td>
                          <td>
                            <img
                              className="border mr-2 mt-2 mb-2"
                              src={`/api/product/photo/front/${wishlistItem._id}`}
                              width="80px"
                              alt={wishlistItem.name}
                            />
                          </td>
                          <td>{wishlistItem.name}</td>
                          <td>{wishlistItem.color}</td>
                          <td>LKR. {wishlistItem.newPrice}.00</td>
                          <td align="center">
                            <span className="order-quantity">
                              {wishlistItem.count}
                            </span>
                          </td>
                          <td align="right" className="pr-3">
                            <button
                              className="btn-register"
                              onClick={() =>
                                this.handleAddToCart(wishlistItem._id)
                              }
                            >
                              Add to Cart
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>

              <div className="row mt-3">
                <div className="col-sm">
                  <Link to="/shop" className="continue-shopping-btn">
                    <i className="fas fa-angle-double-left"></i>
                    &nbsp; Continue Shopping
                  </Link>
                </div>
                <div className="col-sm"></div>
              </div>
            </div>
          ) : (
            <p className="text-center">No wishlist items</p>
          )}
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

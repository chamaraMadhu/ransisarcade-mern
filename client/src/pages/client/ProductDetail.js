// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
// import ReactImageMagnify from "react-image-magnify"; // npm install react-image-magnify
import axios from "axios";
import moment from "moment";
import Rating from "react-rating";
import {
  FacebookShareButton,
  TwitterShareButton,
  FacebookIcon,
  TwitterIcon
} from "react-share";

// import css
import "../../css/client/detailStyle.css";

// import images
import like from "../../images/like.png";
import payment from "../../images/payment-methods.png";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";
import AddToModal from "../../components/client/AddToModal";

// load addToCart methord from cartMethods
import { addToCart } from "../../components/client/CartMethods";

// load addToCart methord from cartMethods
import { addToWishlist } from "../../methods/wishlistMethods";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class ProductDetail extends Component {
  state = {
    // detail product
    productId: "",
    name: "",
    category: "",
    fabric: "",
    color: "",
    oldPrice: "",
    newPrice: "",
    quantity: "",
    sold: "",
    avgRating: 0,
    description: "",
    occasionCasual: "",
    occasionParty: "",
    occasionOffice: "",
    occasionCocktail: "",
    occasionWedding: "",
    washAndCare: "",
    redirectToCart: false,
    largeImage: "",

    // related products
    products: [],

    // rating
    review: "",
    rating: 0,
    ratings: [],

    // add to cart model
    modelProductId: ""
  };

  componentDidMount = () => {
    // get details of product
    axios.get(`/api/product/${this.props.match.params.id}`).then(products => {
      console.log(products);
      this.setState({
        productId: products.data._id,
        name: products.data.name,
        category: products.data.category.name,
        fabric: products.data.fabric.name,
        color: products.data.color,
        oldPrice: products.data.oldPrice,
        newPrice: products.data.newPrice,
        quantity: products.data.quantity,
        avgRating: products.data.rating,
        description: products.data.description,
        occasionCasual: products.data.occasionCasual,
        occasionParty: products.data.occasionParty,
        occasionOffice: products.data.occasionOffice,
        occasionCocktail: products.data.occasionCocktail,
        occasionWedding: products.data.occasionWedding,
        washAndCare: products.data.washAndCare
      });
    });

    // get related products
    axios
      .get(`/api/product/related/${this.props.match.params.id}`)
      .then(products => {
        if (products.data.length > 0) {
          this.setState({ products: products.data });
        }
      });

    // get product ratings
    axios
      .get(`/api/product/ratings/${this.props.match.params.id}`)
      .then(products => {
        if (products.data.length > 0) {
          this.setState({ ratings: products.data });
        }
      });
  };

  // add to cart
  handleAddToCart = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let cartItem = products.data;

      addToCart(cartItem, () => {
        this.setState({ modelProductId: id });

        // clear alert
        setTimeout(() => this.setState({ modelProductId: "" }), 3000);
      });
    });
  };

  // add to wishlist
  handleAddToWishlist = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let wishlistItem = products.data;

      addToWishlist(wishlistItem, () => {
        this.setState({ modelProductId: id });

        // clear alert
        setTimeout(() => this.setState({ modelProductId: "" }), 2000);
      });
    });
  };

  // handle detail product images
  handleDetailImages = e => {
    // tumb mail border
    document.getElementById("img1").style.border = "0";
    document.getElementById("img2").style.border = "0";
    document.getElementById("img3").style.border = "0";
    document.getElementById("img4").style.border = "0";

    document.getElementById(e.target.id).style.border = "1px solid #423c59";

    // get src of clicked image
    let imageSrc = e.target.src;

    this.setState({ largeImage: imageSrc });

    // asign that src to the main image
    document.getElementsByClassName("detail-main-img")[0].src = imageSrc;
  };

  // handle related product images
  handleImages = e => {
    // get src of clicked image

    let imageSrc = e.target.src;

    // asign that src to the main image
    e.target.parentElement.parentElement.previousSibling.children[0].src = imageSrc;
  };

  // get ratings value
  changeRate = rate => {
    this.setState({ rating: rate });
  };

  // get inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // submit review and ratings
  handleRatingSubmit = e => {
    e.preventDefault();

    if (this.state.rating !== 0 && this.state.review !== "") {
      // create ratings object
      const ratingData = {
        productId: this.state.productId,
        userId: isAuthenticated().user._id,
        rating: this.state.rating,
        review: this.state.review
      };

      // send post req for add review and ratings
      axios.post(`/api/product/add/rating`, ratingData).then(product => {
        if (product) {
          // set average ratings
          this.setState({ avgRating: product.data });
          // get reviews
          axios
            .get(`/api/product/ratings/${this.props.match.params.id}`)
            .then(products => {
              if (products.data.length > 0) {
                this.setState({ ratings: products.data });
              }
            });

          // clear states
          this.setState({
            rating: 0,
            review: ""
          });
        }
      });
    }
  };

  render() {
    // assign large image when largeImage state is empty
    const largeImage =
      this.state.largeImage === ""
        ? `/api/product/photo/front/${this.state.productId}`
        : this.state.largeImage;

    // share
    const url = `https://ransisarcade.herokuapp.com/product-detail/${this.state.productId}`;

    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title={`${this.state.name} - Ransis Arcade`} />

        <Navbar />

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-4">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/shop" className="breadcrumb-item-text-bold">
                shop
              </Link>
            </li>
            <li className="breadcrumb-item">{this.state.name}</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container">
          <div className="row">
            <div className="col-md-1 mt-3"></div>

            <div className="col-sm-4 mt-3 product-detail-image-sec">
              <img
                src={`/api/product/photo/front/${this.state.productId}`}
                className="detail-main-img"
                id="detail-main-img"
                alt={this.state.name}
              />
              {/* <ReactImageMagnify
                imageClassName="detail-main-img"
                {...{
                  smallImage: {
                    alt: this.state.name,
                    width: 350,
                    height: 450,
                    sizes:
                      "(max-width: 480px) 100vw, (max-width: 1200px) 30vw, 360px",
                    src: `/api/product/photo/front/${this.state.productId}`
                  },
                  largeImage: {
                    src: largeImage,
                    width: 1200,
                    height: 1800
                  }
                }}
              /> */}

              <img
                src={`/api/product/photo/front/${this.state.productId}`}
                className="detail-thumb-img"
                onClick={this.handleDetailImages}
                alt={this.state.name}
                id="img1"
              />
              <img
                src={`/api/product/photo/jacket/${this.state.productId}`}
                className="detail-thumb-img"
                onClick={this.handleDetailImages}
                alt={this.state.name}
                id="img2"
              />
              <img
                src={`/api/product/photo/border/${this.state.productId}`}
                className="detail-thumb-img"
                onClick={this.handleDetailImages}
                alt={this.state.name}
                id="img3"
              />
              <img
                src={`/api/product/photo/back/${this.state.productId}`}
                className="detail-thumb-img"
                onClick={this.handleDetailImages}
                alt={this.state.name}
                id="img4"
              />
            </div>

            <div className="col-md-6 mt-3 bg-light product-detail-sec">
              <h1 className="product-detail-name">{this.state.name}</h1>
              {/* <h6 className="mb-1">Product ID : {this.state.productId}</h6> */}
              <Rating
                initialRating={this.state.avgRating}
                className="rating-stars-detail mt-2"
                readonly
                emptySymbol="far fa-star"
                fullSymbol="fas fa-star"
              />
              {/* <div className="row">
                <div className="col">
                  <img
                    src={like}
                    className="rounded-circle mt-2"
                    width="20"
                    height="20"
                    title="like"
                    alt="like"
                  />
                  <span className="text-muted mr-2"> 1</span>
                </div>
              </div> */}

              <div className="dropdown-divider mt-2 mb-3"></div>
              <h4 className="mb-3 mt-3 detail-product-price">
                <b>Rs. {this.state.newPrice}.00</b>
              </h4>
              <p className="text-justify mt-4">{this.state.description}</p>

              <div className="dropdown-divider mt-3 mb-3"></div>
              <button
                className="btn-register d-inline"
                onClick={() => this.handleAddToCart(this.state.productId)}
              >
                <i className="fas fa-shopping-cart"></i> &nbsp; Add to Cart
              </button>
              <button
                className="add-to-wishlist-btn"
                onClick={() => this.handleAddToWishlist(this.state.productId)}
              >
                <i className="fas fa-heart"></i> &nbsp; Add to Wishlist
              </button>

              <div className="dropdown-divider mt-3 mb-3"></div>
              <div className="social-media-share">
                <FacebookShareButton url={url} title={this.state.name}>
                  <FacebookIcon size={32} round />
                </FacebookShareButton>
              </div>
              <div className="social-media-share">
                <TwitterShareButton url={url} title={this.state.name}>
                  <TwitterIcon size={32} round />
                </TwitterShareButton>
              </div>
              <p className="mb-0 mt-3 delivery-features">
                Delivery within 5 days for online payments
              </p>
              <p className="delivery-features">Cash on delivery eligable</p>
              <img src={payment} width="250" alt="payment methods" />
            </div>
          </div>

          <div className="product-info mt-5">
            <div>
              <ul className="nav nav-tabs" id="myTab">
                <li className="nav-item">
                  <a
                    className="nav-link active text-muted"
                    role="tab"
                    data-toggle="tab"
                    href="#description"
                    id="description-tab"
                  >
                    <b>Specifications</b>
                  </a>
                </li>
                {/* <li className="nav-item"><a className="nav-link text-muted" role="tab" data-toggle="tab" href="#comments" id="reviews-tab"><b>Comments</b></a></li> */}
              </ul>
              <div className="tab-content" id="myTabContent">
                <div
                  className="tab-pane active fade show description"
                  role="tabpanel"
                  id="description"
                >
                  <div className="px-3">
                    <div className="table-responsive">
                      <table className="product-features">
                        <tbody className="description-table">
                          <tr>
                            <td>
                              <b>Color</b>
                            </td>
                            <td>: {this.state.color}</td>
                          </tr>
                          <tr>
                            <td>
                              <b>Category</b>
                            </td>
                            <td>: {this.state.category}</td>
                          </tr>
                          <tr>
                            <td>
                              <b>Fabric</b>
                            </td>
                            <td>: {this.state.fabric}</td>
                          </tr>
                          <tr>
                            <td className="pr-2">
                              <b>Wash and Care</b>
                            </td>
                            <td>: {this.state.washAndCare}</td>
                          </tr>
                          <tr>
                            <td>
                              <b>Occasion</b>
                            </td>
                            <td>:</td>
                          </tr>
                        </tbody>
                      </table>
                      <table>
                        <tbody>
                          <tr>
                            <td className="pl-3">Casual </td>
                            <td className="pl-3">
                              {this.state.occasionCasual === true ? (
                                <i
                                  className="fas fa-check"
                                  style={{ fontSize: 13, color: "#42b942" }}
                                ></i>
                              ) : (
                                <i
                                  className="fas fa-minus"
                                  style={{ fontSize: 10, color: "#7f7f7f" }}
                                ></i>
                              )}{" "}
                            </td>
                          </tr>

                          <tr>
                            <td className="pl-3">Party </td>
                            <td className="pl-3">
                              {this.state.occasionParty === true ? (
                                <i
                                  className="fas fa-check"
                                  style={{ fontSize: 13, color: "#42b942" }}
                                ></i>
                              ) : (
                                <i
                                  className="fas fa-minus"
                                  style={{ fontSize: 10, color: "#7f7f7f" }}
                                ></i>
                              )}{" "}
                            </td>
                          </tr>

                          <tr>
                            <td className="pl-3">Office</td>
                            <td className="pl-3">
                              {this.state.occasionOffice === true ? (
                                <i
                                  className="fas fa-check"
                                  style={{ fontSize: 13, color: "#42b942" }}
                                ></i>
                              ) : (
                                <i
                                  className="fas fa-minus"
                                  style={{ fontSize: 10, color: "#7f7f7f" }}
                                ></i>
                              )}{" "}
                            </td>
                          </tr>

                          <tr>
                            <td className="pl-3">Cocktail</td>
                            <td className="pl-3">
                              {this.state.occasionCocktail === true ? (
                                <i
                                  className="fas fa-check"
                                  style={{ fontSize: 13, color: "#42b942" }}
                                ></i>
                              ) : (
                                <i
                                  className="fas fa-minus"
                                  style={{ fontSize: 10, color: "#7f7f7f" }}
                                ></i>
                              )}{" "}
                            </td>
                          </tr>

                          <tr>
                            <td className="pl-3">Wedding & Engagement</td>
                            <td className="pl-3">
                              {this.state.occasionWedding === true ? (
                                <i
                                  className="fas fa-check"
                                  style={{ fontSize: 13, color: "#42b942" }}
                                ></i>
                              ) : (
                                <i
                                  className="fas fa-minus"
                                  style={{ fontSize: 10, color: "#7f7f7f" }}
                                ></i>
                              )}{" "}
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>

                  <div className="row desc-images-sec">
                    <div className="col p-0">
                      <img
                        className="product-images"
                        src={`/api/product/photo/front/${this.state.productId}`}
                        alt={this.state.name}
                      />
                    </div>
                    <div className="col p-0">
                      <img
                        className="product-images"
                        src={`/api/product/photo/jacket/${this.state.productId}`}
                        alt={this.state.name}
                      />
                    </div>
                    <div className="col p-0">
                      <img
                        className="product-images"
                        src={`/api/product/photo/border/${this.state.productId}`}
                        alt={this.state.name}
                      />
                    </div>
                    <div className="col p-0">
                      <img
                        className="product-images"
                        src={`/api/product/photo/back/${this.state.productId}`}
                        alt={this.state.name}
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col">
                    <h4 className="text-muted mb-4">
                      Reviews &nbsp;
                      <Rating
                        initialRating={this.state.avgRating}
                        className="avgrating"
                        readonly
                        emptySymbol="far fa-star"
                        fullSymbol="fas fa-star"
                      />
                      &nbsp;
                      <span style={{ fontSize: "15px" }}>
                        {this.state.avgRating.toFixed(1)}
                      </span>
                    </h4>

                    {this.state.ratings.map((rating, i) => (
                      <div className="row m-0" key={i}>
                        <div className="review-img-col">
                          <img
                            src={rating.userId.image}
                            className="reviwer-img"
                            alt={rating.userId.fName}
                          />
                        </div>
                        <div className="review-desc-col">
                          <p className="reviwer-name">
                            {rating.userId.fName} {rating.userId.lName}
                            <span className="reviwe-date">
                              &nbsp; {moment(rating.createdAt).fromNow()}
                            </span>
                          </p>
                          <Rating
                            initialRating={rating.rating}
                            className="rate-starts"
                            readonly
                            emptySymbol="far fa-star"
                            fullSymbol="fas fa-star"
                          />
                          <p className="review">{rating.review}</p>
                        </div>
                      </div>
                    ))}

                    <div className="dropdown-divider mt-2 mb-3"></div>
                    <div className="row m-0">
                      {isAuthenticated() && isAuthenticated().user._id ? (
                        <React.Fragment>
                          <div className="review-img-col">
                            <img
                              src={
                                isAuthenticated() &&
                                isAuthenticated().user.image
                              }
                              className="reviwer-img"
                              alt="name"
                            />
                          </div>
                          <div className="review-desc-col">
                            <Rating
                              initialRating={this.state.rating}
                              onChange={rate => this.changeRate(rate)}
                              className="rate-starts-input mt-2"
                              emptySymbol="far fa-star"
                              fullSymbol="fas fa-star"
                            />
                            <textarea
                              className="review-input d-block mt-3"
                              type="text"
                              name="review"
                              value={this.state.review}
                              onChange={this.handleChange}
                            ></textarea>

                            <button
                              className="btn-register mt-2 mb-4"
                              onClick={this.handleRatingSubmit}
                            >
                              Submit
                            </button>
                          </div>
                        </React.Fragment>
                      ) : (
                        <Link className="btn-register" to="/login">
                          Login to Review
                        </Link>
                      )}
                    </div>
                  </div>
                </div>

                {/* <div className="row">
                  <div className="col">
                    <p>Rate this product</p>
                  </div>
                </div> */}
              </div>
            </div>
          </div>

          {this.state.products.length > 0 ? (
            <div className="clean-related-items mt-4 mb-5">
              <h4 className="text-center second-color">Related Products</h4>
              <hr className="related-product-hr" width="50px" />
              <div className="items mt-4">
                <div className="row justify-content-center related-product-sec">
                  {this.state.products.map((products, i) => (
                    <div key={i} className="card product">
                      <img
                        src={`/api/product/photo/front/${products._id}`}
                        className="related-product-main-image"
                        alt={products.name}
                      />
                      <Link
                        to={`/product-detail/${products._id}`}
                        className="view-details-btn"
                      >
                        VIEW DETAILS
                      </Link>
                      <button
                        className="add-to-cart-btn"
                        onClick={() => this.handleAddToCart(products._id)}
                      >
                        ADD TO CART
                      </button>
                      <button
                        className="mr-1 product-wishlist-btn"
                        onClick={() => this.handleAddToWishlist(products._id)}
                      >
                        <i className="far fa-heart"></i>
                      </button>
                      <div className="row p-1">
                        <div className="col">
                          <img
                            src={`/api/product/photo/front/${products._id}`}
                            className="thumb-images"
                            alt={products.name}
                            onClick={this.handleImages}
                          />
                          <img
                            src={`/api/product/photo/jacket/${products._id}`}
                            className="thumb-images"
                            alt={products.name}
                            onClick={this.handleImages}
                          />
                          <img
                            src={`/api/product/photo/border/${products._id}`}
                            className="thumb-images"
                            alt={products.name}
                            onClick={this.handleImages}
                          />
                          <img
                            src={`/api/product/photo/back/${products._id}`}
                            className="thumb-images"
                            alt={products.name}
                            onClick={this.handleImages}
                          />
                        </div>
                      </div>
                      <div className="px-2 pb-2 product-body">
                        <Rating
                          className="product-card-ratings"
                          initialRating={products.rating}
                          readonly
                          emptySymbol="far fa-star"
                          fullSymbol="fas fa-star"
                        />
                        <p className="text-muted text-sm pt-0 category">
                          {products.category.name}
                        </p>
                        <h6 className="mb-0 product-name">{products.name}</h6>

                        <span className="card-text text-sm mb-0 text-muted product-price">
                          LKR. {products.newPrice}.00
                        </span>
                        <span className="product-dis-price">
                          <strike>
                            {products.oldPrice !== 0
                              ? `LKR. ${products.oldPrice}.00`
                              : null}
                          </strike>
                        </span>
                        <span>{console.log("a")}</span>

                        <div className="row px-3 mt-2">
                          <span>
                            <img
                              src={like}
                              className="rounded-circle like-images"
                              alt="like"
                              data-toggle="modal"
                              data-target="#like-product"
                              onClick={() => this.handleViewLikes(products._id)}
                            />
                          </span>
                          <span className="text-muted p-1 like-counts">
                            {products.likes}
                          </span>
                          <span
                            className="text-muted comment"
                            data-toggle="modal"
                            data-target="#comment-product"
                            onClick={() =>
                              this.handleViewComments(products._id)
                            }
                          >
                            {products.comments} Comments
                          </span>
                        </div>
                      </div>

                      <div className="dropdown-divider mb-1"></div>

                      <div className="row px-3 mb-1">
                        <div className="col p-0">
                          <span
                            className="text-muted like-action"
                            onClick={() => this.addLike(products._id)}
                          >
                            <i className="far fa-thumbs-up"></i> Like
                          </span>
                        </div>

                        <div className="col pl-0 pr-1">
                          <span
                            className="text-muted comment-action"
                            data-toggle="modal"
                            data-target="#comment-product"
                            onClick={() =>
                              this.handleViewComments(products._id)
                            }
                          >
                            <i className="far fa-comment-alt"></i> Comment
                          </span>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          ) : null}
        </div>

        {/* starts of AddToModal */}
        {this.state.modelProductId !== "" ? (
          <React.Fragment>
            <AddToModal modalId={this.state.modelProductId} />
            <div className="modal-backdrop fade show" id="modal-dark"></div>
          </React.Fragment>
        ) : null}
        {/* ends of AddToModal */}

        <Footer />
      </React.Fragment>
    );
  }
}

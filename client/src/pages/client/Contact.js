// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import InputMask from "react-input-mask";
import SweetAlert from "react-bootstrap-sweetalert";

// import css
import "../../css/client/ContactStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// initiate state
const initialState = {
  fName: "",
  lName: "",
  email: "",
  phone: "",
  subject: "",
  message: "",
  errors: {},
  success: false
};

export default class Contact extends Component {
  state = initialState;

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // submit contact form
  handleSubmit = e => {
    e.preventDefault();

    // get mobile number and splited to create 9 value number for validate through
    let phone;

    if (this.state.phone !== "") {
      phone = this.state.phone.split("(+94)");
      phone = phone[1].split(" ");
      phone = phone[1] + phone[2] + phone[3];
    } else {
      phone = this.state.phone;
    }

    // create contact data object
    const sendData = {
      fName: this.state.fName,
      lName: this.state.lName,
      email: this.state.email,
      phone: phone,
      subject: this.state.subject,
      message: this.state.message
    };

    // send mail
    axios
      .post(`/api/send/email`, sendData)
      .then(result => {
        if (result) {
          // clear form
          this.setState(initialState);

          // set alert message
          this.setState({ success: true });
        }
      })
      .catch(err => this.setState({ errors: err.response.data }));
  };

  // close modal
  close = () => {
    this.setState({ success: false, errors: {} });
  };

  render() {
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Contact us - Ransis Arcade" />

        <Navbar />

        {/* starts of jumbotron */}
        <div className="jumbotron jumbotron-contact mb-0">
          <div className="container">
            <div className="pt-5">
              <h2 className="text-center" style={{ color: "#fff" }}>
                Contact Us
              </h2>
            </div>
          </div>
        </div>
        {/* ends of jumbotron */}

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-4">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">Contact</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-md-6 px-4 opening-days-hours-sec">
              <h3 className="second-color mb-3 mt-3">Opening Hours</h3>
              <p>
                <span className="opening-days-hours-first-para">
                  We will happy to accommedate your wishes.If you need further
                  information, don't hesitate to contact us for free
                  consultation.
                </span>
              </p>
              <ul>
                <li>
                  <span className="opening-days-hours">
                    Monday-Sunday : 9.30AM - 7.30PM
                  </span>
                </li>
                <li>
                  <span className="opening-days-hours">Poya days : Close</span>
                </li>
                <li>
                  <span className="opening-days-hours">
                    Bank & Mercantile Holidays : Open
                  </span>
                </li>
                <li>
                  <span className="opening-days-hours">
                    Special times can be arranged.
                  </span>
                </li>
              </ul>

              <table>
                <tbody>
                  <tr>
                    <td>
                      <i
                        className="fa fa-map-marker mr-4"
                        aria-hidden="true"
                      ></i>
                    </td>
                    <td className="contact-details">No. 72/2,</td>
                  </tr>

                  <tr>
                    <td></td>
                    <td className="contact-details">Buthgamuwa Road,</td>
                  </tr>

                  <tr>
                    <td></td>
                    <td className="contact-details">Rajagiriya.</td>
                  </tr>

                  <tr>
                    <td>
                      <i className="fa fa-envelope" aria-hidden="true"></i>
                    </td>
                    <td className="contact-details">
                      <a href="mailto:ransisarcade@gmail.com">
                        ransir@yahoo.com
                      </a>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <i
                        className="fa fa-phone fa-rotate-90"
                        aria-hidden="true"
                      ></i>
                    </td>
                    <td className="contact-details">
                      (+011) 2 793 889 (General line)
                    </td>
                  </tr>

                  <tr>
                    <td>
                      <i className="fa fa-mobile" aria-hidden="true"></i>
                    </td>
                    <td className="contact-details">
                      (+94) 727 352 576 / (+94) 772 725 205 (Sales)
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div className="col-sm-12 col-md-6 mt-5 px-4">
              {this.state.success ? (
                <SweetAlert
                  success
                  title="Successfull"
                  onConfirm={this.close}
                  confirmBtnText="Ok"
                  confirmBtnStyle={{
                    background: "#423c59",
                    border: 0,
                    width: 70
                  }}
                >
                  Email has been sent successfully.
                </SweetAlert>
              ) : null}

              {this.state.errors.fail ? (
                <SweetAlert
                  warning
                  title="Oops, Something went wrong."
                  onConfirm={this.close}
                  confirmBtnText="Ok"
                  confirmBtnStyle={{
                    background: "#423c59",
                    border: 0,
                    width: 70
                  }}
                >
                  Email has not been sent successfully.
                </SweetAlert>
              ) : null}

              <form>
                <div className="row mb-3">
                  <div className="col">
                    <input
                      type="text"
                      className={classnames("form-control", {
                        "is-invalid": this.state.errors.fName
                      })}
                      name="fName"
                      placeholder="First name"
                      value={this.state.fName}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback mt-0">
                      {this.state.errors.fName}
                    </div>
                  </div>

                  <div className="col">
                    <input
                      type="text"
                      className={classnames("form-control", {
                        "is-invalid": this.state.errors.lName
                      })}
                      name="lName"
                      placeholder="Last name"
                      value={this.state.lName}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback mt-0">
                      {this.state.errors.lName}
                    </div>
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    <input
                      type="email"
                      className={classnames("form-control", {
                        "is-invalid": this.state.errors.email
                      })}
                      name="email"
                      placeholder="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback mt-0">
                      {this.state.errors.email}
                    </div>
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    <InputMask
                      className={classnames("form-control", {
                        "is-invalid": this.state.errors.phone
                      })}
                      id="phone"
                      name="phone"
                      value={this.state.phone}
                      onChange={this.handleChange}
                      mask="(+\94) 99 999 9999"
                      placeholder="Mobile"
                    />
                    <p className="input-guidance mb-0">
                      Eg: Mobile Format (+94) 7X XXX XXXX
                    </p>
                    <div className="invalid-feedback mt-0">
                      {this.state.errors.phone}
                    </div>
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="col">
                    <input
                      type="text"
                      className={classnames("form-control", {
                        "is-invalid": this.state.errors.subject
                      })}
                      name="subject"
                      placeholder="Subject"
                      value={this.state.subject}
                      onChange={this.handleChange}
                    />
                    <div className="invalid-feedback mt-0">
                      {this.state.errors.subject}
                    </div>
                  </div>
                </div>

                <div className="row mb-3" id="map">
                  <div className="col">
                    <textarea
                      className={classnames("form-control", {
                        "is-invalid": this.state.errors.message
                      })}
                      placeholder="Message"
                      name="message"
                      style={{ height: "150px" }}
                      value={this.state.message}
                      onChange={this.handleChange}
                    ></textarea>
                    <div className="invalid-feedback mt-0">
                      {this.state.errors.message}
                    </div>
                  </div>
                </div>

                <div className="row mb-5">
                  <div className="col">
                    <button
                      className="btn-register w-100"
                      onClick={this.handleSubmit}
                    >
                      Send
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <div className="row">
            <iframe
              src="https://www.google.com/maps/d/embed?mid=1-jxhsdRqukkHMEHn-0_h2wU1EjRXNecq"
              width="100%"
              height="300"
              title="ransis map"
            ></iframe>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

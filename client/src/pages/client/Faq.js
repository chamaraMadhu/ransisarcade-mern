// import packages
import React from "react";
import { Link } from "react-router-dom";

// import css
import "../../css/client/FaqStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

export default function Faq() {
  return (
    <React.Fragment>
      {/* page title */}
      <PageTitle title="FAQ - Ransis Arcade" />

      <Navbar />

      {/* starts of jumbotron */}
      <div className="jumbotron jumbotron-faq mb-0">
        <div className="container">
          <div className="pt-5">
            <h2 className="text-center" style={{ color: "#fff" }}>
              FAQ
            </h2>
          </div>
        </div>
      </div>
      {/* ends of jumbotron */}

      {/* starts of  breadcrumb  */}
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb bg-light mb-4">
          <li className="breadcrumb-item">
            <Link to="/" className="breadcrumb-item-text-bold">
              Home
            </Link>
          </li>
          <li className="breadcrumb-item">FAQ</li>
        </ol>
      </nav>
      {/* ends of breadcrumb */}

      <div className="container mb-5">
        <section className="text-center mb-5">
          <h3 className="second-color mb-3 mt-3">Frequenty Asked Quections</h3>
          <p style={{ fontSize: "14px", fontWeight: "500", color: "#6D6D6D" }}>
            Here you will find all quections you are facing and full knowledge
            base
          </p>
        </section>

        <section className="faq">
          <div id="accordion" className="accordion mb-4">
            <h6 className="faq-color mb-3 mt-3">- About Ransi's Arcade </h6>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse1"
                aria-expanded="true"
              >
                <span className="card-title">
                  1. Where Is Ransi's Arcade Located?
                </span>
              </div>

              <div className="card-block collapse" id="collapse1">
                <div className="card-body text-muted">
                  The Ransi's Arcade is in Mahaniyara Junction at: No. 72/2,
                  Buthgamuwa Road, Rajagiriya, Sri Lanka which is in front of
                  the "GL Piyadasa Eco Friendly Bird Park, Rajagiriya".
                </div>
              </div>
            </div>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse2"
                aria-expanded="true"
              >
                <span className="card-title">
                  2. Are Ransi's Arcade Products Available Internationally?
                </span>
              </div>

              <div className="card-block collapse" id="collapse2">
                <div className="card-body text-muted">
                  No, we are a local business located in Rajagiriya, Sri Lanka.
                </div>
              </div>
            </div>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse3"
                aria-expanded="true"
              >
                <span className="card-title">
                  3. Are Gift Vouchers Available At The Store?
                </span>
              </div>

              <div className="card-block collapse" id="collapse3">
                <div className="card-body text-muted">
                  Yes. we are selling gift vouchers.
                </div>
              </div>
            </div>

            <h6 className="faq-color mb-3 mt-4">- About Our Products </h6>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse4"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  4. What Kind of sarees do you have?
                </span>
              </div>

              <div className="card-block collapse" id="collapse4">
                <div className="card-body text-muted">
                  Bangalore Silk, Banarasi Silk, Chanderi, Kanchipuram Silk,
                  Jamdani etc
                </div>
              </div>
            </div>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse5"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  5. Do you manufacture sarees?
                </span>
              </div>

              <div className="card-block collapse" id="collapse5">
                <div className="card-body text-muted">
                  No, we import sarees from India and Bangaladesh.
                </div>
              </div>
            </div>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse14"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  6. Are you selling bridle sarees?
                </span>
              </div>

              <div className="card-block collapse" id="collapse14">
                <div className="card-body text-muted">
                  No, we are not selling bridle sarees.
                </div>
              </div>
            </div>

            <h6 className="faq-color mb-3 mt-4">- Payment Method </h6>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#colla7"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  7. How can I pay for the products?
                </span>
              </div>

              <div className="card-block collapse" id="colla7">
                <div className="card-body text-muted pb-2">
                  The following payment methods are accepted.
                  <ul className="mt-2">
                    <li>Cash</li>
                    <li>
                      Credit Cards: Domestic and International Visa, MasterCard
                      and American Express credit cards.
                    </li>
                    <li>
                      Debit Cards: Debit cards issued by most of the major
                      banks.
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <h6 className="faq-color mb-3 mt-4">- Delivery </h6>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse7"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  8. Do you have any delivery system?
                </span>
              </div>

              <div className="card-block collapse" id="collapse7">
                <div className="card-body text-muted">
                  No, you have to come our store and buy them.
                </div>
              </div>
            </div>

            <h6 className="faq-color mb-3 mt-4">- Returns & Exchanges </h6>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse8"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  9. Do you allow return and exchange?
                </span>
              </div>

              <div className="card-block collapse" id="collapse8">
                <div className="card-body text-muted pb-2">
                  Yes, but we have following conditions.
                  <ul className="mt-2">
                    <li>
                      You must return and exchange the saree within 10 days from
                      the date of delivery.
                    </li>
                    <li>
                      Please ensure that the returned merchandize is unused,
                      unworn, unwashed, undamaged and is in a saleable
                      condition.
                    </li>
                    <li>
                      We request you to maintain the original packaging of the
                      items to be returned, including the tags.
                    </li>
                    <li>
                      Ransi's Arcade reserves the right to reject returns from
                      any particular customer, in case of frequent returns by
                      such customer, which shall mean returns on more than 3
                      occasions in a financial year.
                    </li>
                  </ul>
                </div>
              </div>
            </div>

            <h6 className="faq-color mb-3 mt-4">- Customer Service </h6>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse9"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  10. How Do I Contact Customer Service?
                </span>
              </div>

              <div className="card-block collapse" id="collapse9">
                <div className="card-body text-muted">
                  You can contact us through land-line, mobile, email, facebook,
                  contact-form giving in our Contact Us page.
                </div>
              </div>
            </div>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse10"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  11. Can I Email My Order?
                </span>
              </div>

              <div className="card-block collapse" id="collapse10">
                <div className="card-body text-muted">
                  Yes. You can email us your order at ransir@gmail.com.
                </div>
              </div>
            </div>

            <div className="mb-2 border">
              <div
                className="card-header collapsed pl-3 pt-2 pb-2 pr-2 bg-white"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapse11"
                aria-expanded="true"
              >
                <span className="card-title text-capitalize">
                  12. If I Have Problems With My Order, Whom Should I Contact?
                </span>
              </div>

              <div className="card-block collapse" id="collapse11">
                <div className="card-body text-muted">
                  If you have any problems or concerns about your order, please
                  email us at ransir@gmail.com.
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      <Footer />
    </React.Fragment>
  );
}

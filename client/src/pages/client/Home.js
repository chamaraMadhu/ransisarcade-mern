// import packages
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import moment from "moment";
import Rating from "react-rating";

// import css
import "../../css/client/homeStyle.css";

// import images
import like from "../../images/like.png";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import NotificationMsg from "../../components/client/NotificationMsg";
import PageTitle from "../../components/client/PageTitle";
import Testimonials from "../../components/client/Testimonials";
import Advertisements from "../../components/client/Advertisements";
import AddToModal from "../../components/client/AddToModal";

// load addToCart methord from cartMethods
import { addToCart } from "../../components/client/CartMethods";

// load addToCart methord from cartMethods
import { addToWishlist } from "../../methods/wishlistMethods";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class Home extends Component {
  state = {
    latestProducts: [],
    topRatedProducts: [],
    topLikedProducts: [],

    // likes
    likes: [],

    // redirect to login to like
    redirectToLogin: false,
    error: "",

    // comment
    productId: "",
    comment: "",
    comments: [],

    // add to cart model
    modelProductId: ""
  };

  componentDidMount = () => {
    // get latest products
    axios
      .get(`/api/products?sortBy=createdAt&order=desc&limit=5`)
      .then(productResult => {
        this.setState({ latestProducts: productResult.data });
      });

    // get top rated products
    axios.get(`/api/products?sortBy=rating&order=desc&limit=5`).then(result => {
      this.setState({ topRatedProducts: result.data });
    });

    // get top liked products
    axios.get(`/api/products?sortBy=likes&order=desc&limit=5`).then(result => {
      this.setState({ topLikedProducts: result.data });
    });
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // handle images
  handleImages = e => {
    // get src of clicked image
    let imageSrc = e.target.src;

    // asign that src to the main image
    e.target.parentElement.parentElement.parentElement.children[0].src = imageSrc;
  };

  // add to cart
  handleAddToCart = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let cartItem = products.data;

      addToCart(cartItem, () => {
        this.setState({ modelProductId: id });

        // clear alert
        setTimeout(() => this.setState({ modelProductId: "" }), 3000);
      });
    });
  };

  // add to wishlist
  handleAddToWishlist = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let wishlistItem = products.data;

      addToWishlist(wishlistItem, () => {
        this.setState({ modelProductId: id });

        // clear alert
        setTimeout(() => this.setState({ modelProductId: "" }), 2000);
      });
    });
  };

  // add like
  addLike = id => {
    // check user is authenticated
    if (isAuthenticated() && isAuthenticated().user.role === 0) {
      axios
        .post(`/api/product/add/like/${id}/${isAuthenticated().user._id}`)
        .then(products => {
          // update latest products
          axios
            .get(`/api/products?sortBy=createdAt&order=desc&limit=5`)
            .then(productResult => {
              this.setState({ latestProducts: productResult.data });
            });

          // update top rated products
          axios
            .get(`/api/products?sortBy=rating&order=desc&limit=5`)
            .then(result => {
              this.setState({ topRatedProducts: result.data });
            });

          // update top liked products
          axios
            .get(`/api/products?sortBy=likes&order=desc&limit=5`)
            .then(result => {
              this.setState({ topLikedProducts: result.data });
            });
        })
        .catch(err => {
          if (err.response.data.alreadyLikeError) {
            // set alert message
            this.setState({ error: err.response.data.alreadyLikeError });

            // clear alert message
            setTimeout(() => this.setState({ error: "" }), 3000);
          }
        });
    } else {
      // redirect to login page
      this.setState({ redirectToLogin: true });
    }
  };

  // handle likes
  handleViewLikes = id => {
    axios.get(`/api/product/likes/${id}`).then(products => {
      this.setState({ likes: products.data });
    });
  };

  // view products comments
  handleViewComments = id => {
    this.setState({ productId: id });

    // get product comments
    axios.get(`/api/product/comments/${id}`).then(products => {
      this.setState({ comments: products.data });
    });
  };

  // handle comments
  handleCommentSubmit = id => {
    if (this.state.comment !== "") {
      // create comment data object
      const commentData = {
        productId: id,
        userId: isAuthenticated().user._id,
        comment: this.state.comment
      };

      // add comment
      axios.post(`/api/product/add/comment`, commentData).then(product => {
        if (product) {
          // update latest products
          axios
            .get(`/api/products?sortBy=createdAt&order=desc&limit=5`)
            .then(productResult => {
              this.setState({ latestProducts: productResult.data });
            });

          // update top rated products
          axios
            .get(`/api/products?sortBy=rating&order=desc&limit=5`)
            .then(result => {
              this.setState({ topRatedProducts: result.data });
            });

          // update top liked products
          axios
            .get(`/api/products?sortBy=likes&order=desc&limit=5`)
            .then(result => {
              this.setState({ topLikedProducts: result.data });
            });

          // update comments
          axios.get(`/api/product/comments/${id}`).then(products => {
            if (products.data.length > 0) {
              this.setState({ comments: products.data });
            }
          });

          // clear comment input
          this.setState({
            comment: ""
          });
        }
      });
    }
  };

  // unlogged customers redirect to login page
  handleLoginToComment = () => {
    this.setState({ redirectToLogin: true });
  };

  render() {
    // redirect to cart
    if (this.state.redirectToCart === true) {
      return <Redirect to="/cart" />;
    }

    // redirect to login
    if (this.state.redirectToLogin === true) {
      return <Redirect to="/login" />;
    }

    return (
      <React.Fragment>
        {/* error and success messages */}
        {this.state.error !== "" ? (
          <NotificationMsg msgType="error" message={this.state.error} />
        ) : null}

        {/* page title */}
        <PageTitle title="Online Saree Shop - Ransis Arcade" />

        <Navbar />

        {/* starts of jumbotron */}
        <div className="jumbotron jumbotron-home mb-0">
          <div className="jumbotron-sec mt-2">
            <h1 className="home-page-heading">Women's Sarees</h1>
            <h1 className="home-page-heading">Collection</h1>
            <p className="page-subheading mb-0 mt-4">Exclusive sarees from </p>
            <p className="page-subheading mb-1">India and Bangaladesh</p>
            <Link
              className="shop-now-btn btn-register"
              to="/shop"
              style={{ textDecoration: "none", borderRadius: "0" }}
            >
              Shop Now
            </Link>
          </div>
        </div>
        {/* ends of jumbotron */}

        <div className="shop-container mt-5 px-1">
          <div className="row m-0">
            <div className="other-sec">
              {/* starts of latest product section */}
              {this.state.latestProducts.length > 0 ? (
                <React.Fragment>
                  <div className="row mx-0 home-page-products">
                    <div className="col">
                      <h5 className="text-uppercase second-color">
                        <i
                          className="fa fa-th text-danger"
                          aria-hidden="true"
                        ></i>{" "}
                        New Arrivals
                      </h5>

                      {this.state.latestProducts.map((products, i) => (
                        <div key={i} className="card product">
                          <img
                            src={`/api/product/photo/front/${products._id}`}
                            className="product-image"
                            alt={products.name}
                          />
                          <Link
                            to={`/product-detail/${products._id}`}
                            className="view-details-btn"
                          >
                            VIEW DETAILS
                          </Link>
                          <button
                            className="add-to-cart-btn"
                            onClick={() => this.handleAddToCart(products._id)}
                          >
                            ADD TO CART
                          </button>
                          <button
                            className="mr-1 product-wishlist-btn"
                            onClick={() =>
                              this.handleAddToWishlist(products._id)
                            }
                          >
                            <i className="far fa-heart"></i>
                          </button>
                          <div className="row p-1">
                            <div className="col">
                              <img
                                src={`/api/product/photo/front/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/jacket/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/border/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/back/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                            </div>
                          </div>
                          <div className="px-2 pb-2 product-body">
                            <Rating
                              className="product-card-ratings"
                              initialRating={products.rating}
                              readonly
                              emptySymbol="far fa-star"
                              fullSymbol="fas fa-star"
                            />
                            <p className="text-sm pt-0 category">
                              {products.category.name}
                            </p>
                            <h6 className="mb-0 product-name">
                              {products.name}
                            </h6>

                            <span className="card-text text-sm mb-0 product-price">
                              LKR. {products.newPrice}.00
                            </span>
                            <span className="product-dis-price">
                              <strike>
                                {products.oldPrice !== 0
                                  ? `LKR. ${products.oldPrice}.00`
                                  : null}
                              </strike>
                            </span>

                            <div className="row px-3 mt-2">
                              <span>
                                <img
                                  src={like}
                                  className="rounded-circle like-images"
                                  alt="like"
                                  data-toggle="modal"
                                  data-target="#like-product"
                                  onClick={() =>
                                    this.handleViewLikes(products._id)
                                  }
                                />
                              </span>
                              <span className="text-muted p-1 like-counts">
                                {products.likes}
                              </span>
                              <span
                                className="text-muted comment"
                                data-toggle="modal"
                                data-target="#comment-product"
                                onClick={() =>
                                  this.handleViewComments(products._id)
                                }
                              >
                                {products.comments} Comments
                              </span>
                            </div>
                          </div>

                          <div className="dropdown-divider mb-1"></div>

                          <div className="row px-3 mb-1">
                            <div className="col p-0">
                              <span
                                className="text-muted like-action"
                                onClick={() => this.addLike(products._id)}
                              >
                                <i className="far fa-thumbs-up"></i> Like
                              </span>
                            </div>

                            <div className="col pl-0 pr-1">
                              <span
                                className="text-muted comment-action"
                                data-toggle="modal"
                                data-target="#comment-product"
                                onClick={() =>
                                  this.handleViewComments(products._id)
                                }
                              >
                                <i className="far fa-comment-alt"></i> Comment
                              </span>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </React.Fragment>
              ) : null}
              {/* ends of latest product section */}

              {/* starts of top rated product section */}
              {this.state.topRatedProducts.length > 0 ? (
                <React.Fragment>
                  <div className="row mx-0 home-page-products">
                    <div className="col">
                      <h5 className="text-uppercase second-color mt-5">
                        <i
                          className="fa fa-th text-danger"
                          aria-hidden="true"
                        ></i>{" "}
                        Top Rated
                      </h5>
                      {this.state.topRatedProducts.map((products, i) => (
                        <div key={i} className="card product">
                          <img
                            src={`/api/product/photo/front/${products._id}`}
                            className="product-image"
                            alt={products.name}
                          />
                          <Link
                            to={`/product-detail/${products._id}`}
                            className="view-details-btn"
                          >
                            VIEW DETAILS
                          </Link>
                          <button
                            className="add-to-cart-btn"
                            onClick={() => this.handleAddToCart(products._id)}
                          >
                            ADD TO CART
                          </button>
                          <button
                            className="mr-1 product-wishlist-btn"
                            onClick={() =>
                              this.handleAddToWishlist(products._id)
                            }
                          >
                            <i className="far fa-heart"></i>
                          </button>
                          <div className="row p-1">
                            <div className="col">
                              <img
                                src={`/api/product/photo/front/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/jacket/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/border/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/back/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                            </div>
                          </div>
                          <div className="px-2 pb-2 product-body">
                            <Rating
                              className="product-card-ratings"
                              initialRating={products.rating}
                              readonly
                              emptySymbol="far fa-star"
                              fullSymbol="fas fa-star"
                            />
                            <p className="text-sm pt-0 category">
                              {products.category.name}
                            </p>
                            <h6 className="mb-0 product-name">
                              {products.name}
                            </h6>

                            <span className="card-text text-sm mb-0 product-price">
                              LKR. {products.newPrice}.00
                            </span>
                            <span className="product-dis-price">
                              <strike>LKR. {products.oldPrice}.00</strike>
                            </span>

                            <div className="row px-3 mt-2">
                              <span>
                                <img
                                  src={like}
                                  className="rounded-circle like-images"
                                  alt="like"
                                  data-toggle="modal"
                                  data-target="#like-product"
                                  onClick={() =>
                                    this.handleViewLikes(products._id)
                                  }
                                />
                              </span>
                              <span className="text-muted p-1 like-counts">
                                {products.likes}
                              </span>
                              <span
                                className="text-muted comment"
                                data-toggle="modal"
                                data-target="#comment-product"
                                onClick={() =>
                                  this.handleViewComments(products._id)
                                }
                              >
                                {products.comments} Comments
                              </span>
                            </div>
                          </div>

                          <div className="dropdown-divider mb-1"></div>

                          <div className="row px-3 mb-1">
                            <div className="col p-0">
                              <span
                                className="text-muted like-action"
                                onClick={() => this.addLike(products._id)}
                              >
                                <i className="far fa-thumbs-up"></i> Like
                              </span>
                            </div>

                            <div className="col pl-0 pr-1">
                              <span
                                className="text-muted comment-action"
                                data-toggle="modal"
                                data-target="#comment-product"
                                onClick={() =>
                                  this.handleViewComments(products._id)
                                }
                              >
                                <i className="far fa-comment-alt"></i> Comment
                              </span>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </React.Fragment>
              ) : null}
              {/* ends of top rated product section */}

              {/* starts of top liked product section */}
              {this.state.topLikedProducts.length > 0 ? (
                <React.Fragment>
                  <div className="row mx-0 home-page-products">
                    <div className="col">
                      <h5 className="text-uppercase second-color mt-5">
                        <i
                          className="fa fa-th text-danger"
                          aria-hidden="true"
                        ></i>{" "}
                        Top Liked
                      </h5>
                      {this.state.topLikedProducts.map((products, i) => (
                        <div key={i} className="card product">
                          <img
                            src={`/api/product/photo/front/${products._id}`}
                            className="product-image"
                            alt={products.name}
                          />
                          <Link
                            to={`/product-detail/${products._id}`}
                            className="view-details-btn"
                          >
                            VIEW DETAILS
                          </Link>
                          <button
                            className="add-to-cart-btn"
                            onClick={() => this.handleAddToCart(products._id)}
                          >
                            ADD TO CART
                          </button>
                          <button
                            className="mr-1 product-wishlist-btn"
                            onClick={() =>
                              this.handleAddToWishlist(products._id)
                            }
                          >
                            <i className="far fa-heart"></i>
                          </button>
                          <div className="row p-1">
                            <div className="col">
                              <img
                                src={`/api/product/photo/front/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/jacket/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/border/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                              <img
                                src={`/api/product/photo/back/${products._id}`}
                                className="thumb-images"
                                alt={products.name}
                                onClick={this.handleImages}
                              />
                            </div>
                          </div>
                          <div className="px-2 pb-2 product-body">
                            <Rating
                              className="product-card-ratings"
                              initialRating={products.rating}
                              readonly
                              emptySymbol="far fa-star"
                              fullSymbol="fas fa-star"
                            />
                            <p className="text-sm pt-0 category">
                              {products.category.name}
                            </p>
                            <h6 className="mb-0 product-name">
                              {products.name}
                            </h6>

                            <span className="card-text text-sm mb-0 product-price">
                              LKR. {products.newPrice}.00
                            </span>
                            <span className="product-dis-price">
                              <strike>LKR. {products.oldPrice}.00</strike>
                            </span>

                            <div className="row px-3 mt-2">
                              <span>
                                <img
                                  src={like}
                                  className="rounded-circle like-images"
                                  alt="like"
                                  data-toggle="modal"
                                  data-target="#like-product"
                                  onClick={() =>
                                    this.handleViewLikes(products._id)
                                  }
                                />
                              </span>
                              <span className="text-muted p-1 like-counts">
                                {products.likes}
                              </span>
                              <span
                                className="text-muted comment"
                                data-toggle="modal"
                                data-target="#comment-product"
                                onClick={() =>
                                  this.handleViewComments(products._id)
                                }
                              >
                                {products.comments} Comments
                              </span>
                            </div>
                          </div>

                          <div className="dropdown-divider mb-1"></div>

                          <div className="row px-3 mb-1">
                            <div className="col p-0">
                              <span
                                className="text-muted like-action"
                                onClick={() => this.addLike(products._id)}
                              >
                                <i className="far fa-thumbs-up"></i> Like
                              </span>
                            </div>

                            <div className="col pl-0 pr-1">
                              <span
                                className="text-muted comment-action"
                                data-toggle="modal"
                                data-target="#comment-product"
                                onClick={() =>
                                  this.handleViewComments(products._id)
                                }
                              >
                                <i className="far fa-comment-alt"></i> Comment
                              </span>
                            </div>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </React.Fragment>
              ) : null}
              {/* ends of top liked product section */}

              {/* starts of like model */}
              <div
                className="modal fade"
                id="like-product"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="like-product"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-centered"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-body">
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>

                      <p style={{ fontSize: "17px", fontWeight: "600" }}>
                        {this.state.likes.length} Likes
                      </p>
                      {this.state.likes.length > 0
                        ? this.state.likes.map((like, i) => (
                            <div className="row m-0 mt-3" key={i}>
                              <div className="comment-img-col">
                                <img
                                  src={like.userId.image}
                                  className="reviwer-img"
                                  alt={like.userId.fName}
                                />
                              </div>
                              <div className="comment-desc-col">
                                <p className="reviwer-name mt-2">
                                  {like.userId.fName} {like.userId.lName}
                                  <span className="reviwe-date">
                                    &nbsp; {moment(like.createdAt).fromNow()}
                                  </span>
                                </p>
                              </div>
                            </div>
                          ))
                        : null}
                    </div>
                  </div>
                </div>
              </div>
              {/* ends of like model */}

              {/* starts of comment model */}
              <div
                className="modal fade"
                id="comment-product"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="comment-product"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-centered"
                  role="document"
                >
                  <div className="modal-content">
                    <div className="modal-body">
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>

                      <p style={{ fontSize: "17px", fontWeight: "600" }}>
                        {this.state.comments.length} comments
                      </p>
                      {this.state.comments.length > 0
                        ? this.state.comments.map((comment, i) => (
                            <div className="row m-0" key={i}>
                              <div className="comment-img-col">
                                <img
                                  src={comment.userId.image}
                                  className="reviwer-img"
                                  alt={comment.userId.fName}
                                />
                              </div>
                              <div className="comment-desc-col">
                                <p className="reviwer-name">
                                  {comment.userId.fName} {comment.userId.lName}
                                  <span className="reviwe-date">
                                    &nbsp; {moment(comment.createdAt).fromNow()}
                                  </span>
                                </p>
                                <p className="review">{comment.comment}</p>
                              </div>
                            </div>
                          ))
                        : null}

                      <div className="row m-0 mt-3">
                        {isAuthenticated() && isAuthenticated().user._id ? (
                          <React.Fragment>
                            <div className="comment-img-col">
                              <img
                                src={
                                  isAuthenticated() &&
                                  isAuthenticated().user.image
                                }
                                className="reviwer-img"
                                alt="reviwer img"
                              />
                            </div>
                            <div className="comment-desc-col">
                              <textarea
                                className="comment-input d-block"
                                type="text"
                                name="comment"
                                placeholder="Write a comment"
                                value={this.state.comment}
                                onChange={this.handleChange}
                              ></textarea>

                              <button
                                className="btn-register w-100 mt-2"
                                onClick={() =>
                                  this.handleCommentSubmit(this.state.productId)
                                }
                              >
                                Submit
                              </button>
                            </div>
                          </React.Fragment>
                        ) : (
                          <Link
                            className="btn-register"
                            data-dismiss="modal"
                            aria-label="Close"
                            onClick={this.handleLoginToComment}
                            to="/login"
                          >
                            Login to Comment
                          </Link>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* ends of comment model */}

              {/* starts of AddToModal */}
              {this.state.modelProductId !== "" ? (
                <React.Fragment>
                  <AddToModal modalId={this.state.modelProductId} />
                  <div
                    className="modal-backdrop fade show"
                    id="modal-dark"
                  ></div>
                </React.Fragment>
              ) : null}
              {/* ends of AddToModal */}
            </div>

            <div className="advertisement-sec">
              <Advertisements />
            </div>
          </div>
        </div>

        <Testimonials />

        <Footer />
      </React.Fragment>
    );
  }
}

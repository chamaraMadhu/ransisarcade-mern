// import packages
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";
import InputMask from "react-input-mask";

// import css
import "../../css/client/checkoutStyle.css";

// import braintree drop in react
import DropIn from "braintree-web-drop-in-react";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

const initialState = {
  // delivery states
  title: "",
  recipientsName: "",
  phone: "",
  address: "",
  city: "",
  cities: [],
  locationType: "",
  deliveryInstructions: "",
  errors: {},
  displayDeliveryInfoStage: true,
  displayOrderInfoStage: false,
  displayPaymentStage: false,
  displayDeliveryInfoStageColor: false,
  displayOrderInfoStageColor: false,
  displayPaymentStageColor: false,

  // order states
  cartItems: [],

  // deliveryFee
  deliveryFee: 0,

  // payment states
  success: false,
  clientToken: null,
  instance: {},

  // redirect to cart page
  redirectToCart: false
};

export default class Checkout extends Component {
  state = initialState;

  componentDidMount = () => {
    // load delivery cities
    axios.get(`/api/deliveryFees`).then(cities => {
      if (cities.data.length > 0) {
        this.setState({
          cities: cities.data
        });
      }
    });

    // get card items from local storage
    if (typeof window !== "undefined") {
      if (localStorage.getItem("cart")) {
        const cart = JSON.parse(localStorage.getItem("cart"));
        this.setState({ cartItems: cart });
      }
    }
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // submit delivery informations
  handleSubmitDeliveryInfo = e => {
    e.preventDefault();

    // get mobile number and splited to create 9 value number for validate through
    let phone;

    if (this.state.phone !== "") {
      phone = this.state.phone.split("(+94)");
      phone = phone[1].split(" ");
      phone = phone[1] + phone[2] + phone[3];
    } else {
      phone = this.state.phone;
    }

    // create delivery data object
    const data = {
      title: this.state.title,
      recipientsName: this.state.recipientsName,
      phone: phone,
      address: this.state.address,
      city: this.state.city,
      locationType: this.state.locationType,
      deliveryInstructions: this.state.deliveryInstructions
    };

    // send delivery information to server
    axios
      .post(`/api/braintree/deliveryInfo/${isAuthenticated().user._id}`, data, {
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
      .then(result => {
        this.setState({
          deliveryFee: result.data.deliveryFee,
          errors: {},
          displayDeliveryInfoStage: false,
          displayOrderInfoStage: true,
          displayDeliveryInfoStageColor: true
        });
      })
      .catch(err => {
        this.setState({
          errors: err.response.data
        });
      });
  };

  // submit order information
  handleSubmitOrderInfo = e => {
    e.preventDefault();

    // get client token that given by braintree
    axios({
      method: "get",
      url: `/api/braintree/getToken/${isAuthenticated().user._id}`,
      headers: { Authorization: `Bearer ${isAuthenticated().token}` }
    })
      .then(result => {
        if (result) {
          this.setState({
            clientToken: result.data.clientToken,
            displayPaymentStage: true,
            displayOrderInfoStage: false,
            displayDeliveryInfoStage: false,
            displayOrderInfoStageColor: true
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  // do payment
  handlePay = e => {
    e.preventDefault();

    // set loading
    this.setState({ loading: true });

    // calculate the total amount without delivery chargers
    const total = this.state.cartItems.reduce((currentValue, nextValue) => {
      return currentValue + nextValue.quantity * nextValue.newPrice;
    }, 0);

    // calculate the grand total amount with delivery chargers
    const grandTotal = total + parseInt(this.state.deliveryFee);

    // send the nonce to the server
    // nonce = this.state.instance.requestPaymentMethod()
    let nonce;

    this.state.instance.requestPaymentMethod().then(result => {
      nonce = result.nonce;

      // nonce you have nonce (card type, card number) send nonce as 'paymentMethodNonce'
      // and also total to be charged
      const paymentData = {
        paymentMethodNonce: nonce,
        amountFromTheClient: grandTotal
      };

      // post payment
      axios({
        method: "post",
        url: `/api/braintree/payment/${isAuthenticated().user._id}`,
        data: paymentData,
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
        .then(result => {
          console.log(result);
          if (result) {
            this.setState({
              displayPaymentStage: false,
              displayPaymentStageColor: true,
              success: true
            });

            // create order data object
            const orderData = {
              orderItems: this.state.cartItems,
              transactionId: result.data.transaction.id,
              amount: result.data.transaction.amount,
              transactionTime: result.data.transaction.createdAt,
              title: this.state.title,
              recipientsName: this.state.recipientsName,
              phone: this.state.phone,
              address: this.state.address,
              deliveryFee: this.state.city,
              locationType: this.state.locationType,
              isOnlinePayment: true,
              deliveryInstructions: this.state.deliveryInstructions
            };

            // create order
            axios({
              method: "post",
              url: `/api/order/add/${isAuthenticated().user._id}`,
              data: orderData,
              headers: { Authorization: `Bearer ${isAuthenticated().token}` }
            })
              .then(order => {
                console.log(order);
                // create mail data object
                const mailData = {
                  orderId: order.data._id,
                  orderItems: order.data.orderItems,
                  transactionId: result.data.transaction.id,
                  amount: result.data.transaction.amount,
                  transactionTime: order.data.createdAt,
                  title: this.state.title,
                  recipientsName: this.state.recipientsName,
                  phone: this.state.phone,
                  address: this.state.address,
                  deliveryFee: this.state.city,
                  locationType: this.state.locationType,
                  isOnlinePayment: true,
                  deliveryInstructions: this.state.deliveryInstructions
                };

                console.log(mailData);

                // thanking for payment
                axios
                  .post(
                    `/api/send/email/online-payment/${
                      isAuthenticated().user._id
                    }`,
                    mailData
                  )
                  .then(res => console.log("sent payment mail"))
                  .catch(err => console.log(err));

                this.setState({ loading: false });

                // remove all cart items
                if (typeof window !== "undefined") {
                  if (localStorage.getItem("cart")) {
                    localStorage.removeItem("cart");
                  }
                }
              })
              .catch(err => {
                console.log(err);
                this.setState({ loading: false });
              });
          }
        })
        .catch(err => {
          console.log(err);
          this.setState({ loading: false });
        });
    });
  };

  // close modal
  close = () => {
    this.setState({ success: false, redirectToCart: true, errors: {} });
  };

  render() {
    // calculate the total amount without delivery chargers
    const total = this.state.cartItems.reduce((currentValue, nextValue) => {
      return currentValue + nextValue.quantity * nextValue.newPrice;
    }, 0);

    // calculate the grand total amount with delivery chargers
    const grandTotal = total + parseInt(this.state.deliveryFee);

    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Online Checkout - Ransis Arcade" />

        <Navbar />

        {this.state.redirectToCart ? <Redirect to="/cart" /> : null}

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-xs-0 mb-sm-0 mb-md-0 mb-lg-5">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/shop" className="breadcrumb-item-text-bold">
                Shop
              </Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              <Link to="/cart" className="breadcrumb-item-text-bold">
                My Cart
              </Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              Online Payment Checkout
            </li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container">
          <div className="row my-3 m-0 delivery-info">
            <div className="col p-0">
              {this.state.success ? (
                <SweetAlert
                  success
                  title="Successfull"
                  onConfirm={this.close}
                  confirmBtnText="Ok"
                  confirmBtnStyle={{
                    background: "#423c59",
                    border: 0,
                    width: 70
                  }}
                >
                  Thanks! you payment was successfull.
                </SweetAlert>
              ) : null}

              <div className="row m-0">
                <div className="col p-0">
                  {!this.state.displayDeliveryInfoStageColor ? (
                    <h4 className="mb-4 mt-4 checkout-headings">
                      <span className="point-styles">1</span> Delivery
                      Information
                    </h4>
                  ) : (
                    <h4>
                      <span className="completed-task">
                        <i className="fas fa-check-circle text-success"></i>{" "}
                        Delivery Information
                      </span>
                    </h4>
                  )}
                </div>
              </div>

              {this.state.displayDeliveryInfoStage ? (
                <div className="row m-0">
                  <div className="col p-0">
                    <form>
                      <div className="form-row m-0">
                        <div className="form-group">
                          <label htmlFor="title">Title</label>
                          <select
                            id="title"
                            name="title"
                            className={classnames("form-control", {
                              "is-invalid": this.state.errors.title
                            })}
                            value={this.state.title}
                            onChange={this.handleChange}
                          >
                            <option></option>
                            <option>Mr.</option>
                            <option>Mrs.</option>
                            <option>Ms.</option>
                          </select>
                          <div className="invalid-feedback mt-0">
                            {this.state.errors.title}
                          </div>
                        </div>
                        <div className="form-group name">
                          <label htmlFor="name">Recipient's Name</label>
                          <input
                            type="text"
                            className={classnames("form-control", {
                              "is-invalid": this.state.errors.recipientsName
                            })}
                            id="name"
                            name="recipientsName"
                            value={this.state.recipientsName}
                            onChange={this.handleChange}
                            autoComplete="off"
                          />
                          <div
                            className="invalid-feedback mt-0"
                            style={{ width: "auto" }}
                          >
                            {this.state.errors.recipientsName}
                          </div>
                        </div>
                      </div>

                      <div className="form-row m-0">
                        <div className="form-group phone-city-location-type">
                          <label htmlFor="phone">Recipient's Phone</label>
                          <InputMask
                            className={classnames("form-control", {
                              "is-invalid": this.state.errors.phone
                            })}
                            id="phone"
                            name="phone"
                            value={this.state.phone}
                            onChange={this.handleChange}
                            mask="(+\94) 99 999 9999"
                          />
                          <p className="text-muted extra-info">
                            (Format 7xx xxx xxx)
                          </p>
                          <div
                            className="invalid-feedback mt-0"
                            style={{ width: "225px" }}
                          >
                            {this.state.errors.phone}
                          </div>
                        </div>
                      </div>

                      <div className="form-row m-0">
                        <div className="form-group address-option">
                          <label htmlFor="address">Delivery Address</label>
                          <input
                            type="text"
                            className={classnames("form-control", {
                              "is-invalid": this.state.errors.address
                            })}
                            id="address"
                            name="address"
                            value={this.state.address}
                            onChange={this.handleChange}
                            autoComplete="off"
                          />
                          <p className="text-muted extra-info">
                            (Apartment No, Street Address, City, State)
                          </p>
                          <div
                            className="invalid-feedback mt-0"
                            style={{ width: "17auto0px" }}
                          >
                            {this.state.errors.address}
                          </div>
                        </div>
                      </div>

                      <div className="form-row m-0">
                        <div className="form-group phone-city-location-type">
                          <label htmlFor="city">Delivery City</label>
                          <select
                            className={classnames("form-control", {
                              "is-invalid": this.state.errors.city
                            })}
                            id="city"
                            name="city"
                            value={this.state.city}
                            onChange={this.handleChange}
                          >
                            <option></option>
                            {this.state.cities.map((cities, i) => (
                              <option key={i} value={cities.fee}>
                                {cities.city}
                              </option>
                            ))}
                          </select>
                          <p className="text-muted extra-info">
                            Delivery charges will be added based on the city.
                          </p>
                          <div
                            className="invalid-feedback mt-0"
                            style={{ width: "170px" }}
                          >
                            {this.state.errors.city}
                          </div>
                        </div>
                      </div>

                      <div className="form-row m-0">
                        <div className="form-group phone-city-location-type">
                          <label htmlFor="location-type">Location Type</label>
                          <select
                            className={classnames("form-control", {
                              "is-invalid": this.state.errors.locationType
                            })}
                            id="location-type"
                            name="locationType"
                            value={this.state.locationType}
                            onChange={this.handleChange}
                          >
                            <option></option>
                            <option>House</option>
                            <option>Apartment</option>
                            <option>Office</option>
                            <option>Boarding</option>
                            <option>Other</option>
                          </select>
                          <div
                            className="invalid-feedback mt-0"
                            style={{ width: "170px" }}
                          >
                            {this.state.errors.locationType}
                          </div>
                        </div>
                      </div>

                      <div className="form-row mb-3 m-0">
                        <div className="form-group address-option">
                          <label htmlFor="optional">
                            Special delivery instructions: (Optional)
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="optional"
                            name="deliveryInstructions"
                            value={this.state.deliveryInstructions}
                            onChange={this.handleChange}
                            autoComplete="off"
                          />
                          <p className="text-muted extra-info">
                            Please make your special instructions short and
                            clear. Example: 'Call the recipient before the
                            delivery.' Specific delivery times are not
                            guaranteed.
                          </p>
                        </div>
                      </div>
                    </form>

                    <div className="row mt-3 m-0 mb-3">
                      <div className="col p-0">
                        <button
                          type="submit"
                          className="btn-register float-right"
                          onClick={this.handleSubmitDeliveryInfo}
                        >
                          Continue <i className="fas fa-angle-double-right"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}

              <div className="row m-0">
                <div className="col p-0">
                  {!this.state.displayOrderInfoStageColor ? (
                    <h4 className="mb-4 mt-4 checkout-headings">
                      <span className="point-styles">2</span> Your Order with
                      Full Amount
                    </h4>
                  ) : (
                    <h4>
                      <span className="completed-task">
                        <i className="fas fa-check-circle text-success"></i>{" "}
                        Your Order with Full Amount
                      </span>
                    </h4>
                  )}
                </div>
              </div>

              {this.state.displayOrderInfoStage === true ? (
                <div className="row mt-4 m-0 mb-4">
                  <div className="col p-0">
                    <table
                      className="table table-responsive"
                      style={{ border: 0, margin: 0, width: "100%" }}
                    >
                      <tbody>
                        <tr className="bg-dark text-light">
                          <td style={{ minWidth: "80px" }}></td>
                          <td style={{ minWidth: "300px" }}>Product</td>
                          <td align="right" style={{ width: "150px" }}>
                            Price
                          </td>
                          <td align="center" style={{ width: "135px" }}>
                            Quantity
                          </td>
                          <td align="right" style={{ width: "150px" }}>
                            Subtotal
                          </td>
                        </tr>
                        {this.state.cartItems.map((cartItems, i) => (
                          <tr key={i}>
                            <td>
                              <img
                                src={`/api/product/photo/front/${cartItems._id}`}
                                width="70"
                                alt={cartItems.name}
                              />
                            </td>
                            <td>
                              {cartItems.name}, {cartItems.color} color,{" "}
                              {cartItems.fabric.name} saree
                            </td>
                            <td align="right">Rs.{cartItems.newPrice}.00</td>
                            <td align="center">{cartItems.quantity}</td>
                            <td align="right">
                              Rs. {cartItems.newPrice * cartItems.quantity}.00
                            </td>
                          </tr>
                        ))}
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td align="center">
                            <b>Total</b>
                          </td>
                          <td align="right">
                            <b>Rs. {total}.00</b>
                          </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td align="center">
                            <b>Delivery Fee</b>
                          </td>
                          <td align="right">
                            <b>+ Rs. {this.state.deliveryFee}.00</b>
                          </td>
                        </tr>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td align="center">
                            <b>Grand Total</b>
                          </td>
                          <td align="right">
                            <b>Rs. {grandTotal}.00</b>
                          </td>
                        </tr>
                      </tbody>
                    </table>

                    <div className="row mt-3 m-0">
                      <div className="col p-0">
                        <button
                          type="submit"
                          className="btn-register float-right"
                          onClick={this.handleSubmitOrderInfo}
                        >
                          Continue <i className="fas fa-angle-double-right"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}

              <div className="row mb-4 m-0">
                <div className="col p-0">
                  {!this.state.displayPaymentStageColor ? (
                    <h4 className="mb-4 mt-4 checkout-headings">
                      <span className="point-styles">3</span> Payment
                    </h4>
                  ) : (
                    <h4>
                      <span className="completed-task">
                        <i className="fas fa-check-circle text-success"></i>{" "}
                        Payment
                      </span>
                    </h4>
                  )}
                </div>
              </div>
              {this.state.displayPaymentStage ? (
                <div className="row mb-4 m-0">
                  <div className="col p-0">
                    <h5 className="mb-3">
                      Your payment is Rs. {grandTotal}.00
                    </h5>
                    {this.state.clientToken !== null ? (
                      <div style={{ marginTop: "20px" }}>
                        <DropIn
                          options={{
                            authorization: this.state.clientToken,
                            paypal: { flow: "vault" }
                          }}
                          onInstance={instance => {
                            this.setState({ instance: instance });
                          }}
                        />
                      </div>
                    ) : null}
                    <div className="row mt-3 m-0">
                      <div className="col p-0">
                        {/* {this.state.instance} */}
                        <button className="btn-pay" onClick={this.handlePay}>
                          {this.state.loading ? (
                            <div
                              className="spinner-border spinner-border-sm text-light"
                              role="status"
                            >
                              <span className="sr-only">Loading...</span>
                            </div>
                          ) : null}
                          {this.state.loading ? " Paying..." : "Pay"}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

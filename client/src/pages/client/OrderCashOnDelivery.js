// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import moment from "moment";

// import components
import Navbar from "../../components/client/Navbar";
import UserAccountSidebar from "../../components/client/UserAccountSidebar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class OrderCashOnDelivery extends Component {
  state = {
    myOrders: []
  };

  componentDidMount = () => {
    // get orders
    axios
      .get(`/api/myOrders/cash-on-delivery/${isAuthenticated().user._id}`, {
        headers: { Authorization: `Bearer ${isAuthenticated().token}` }
      })
      .then(myOrders => {
        if (myOrders.data.length > 0) {
          this.setState({
            myOrders: myOrders.data
          });
        }
      });
  };

  render() {
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="My Orders - Ransis Arcade" />

        <Navbar />
        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-0">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/user-account" className="breadcrumb-item-text-bold">
                Customer Account
              </Link>
            </li>
            <li className="breadcrumb-item">My Orders - Cash On Delivery</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container my-3">
          <div className="row">
            <UserAccountSidebar />

            <div className="bg-light px-0 profile-content">
              <h5>My Orders</h5>

              <div className="container-fluid inner-content">
                <div className="container-fluid bg-white pb-3 px-0">
                  {this.state.myOrders.map((order, i) => (
                    <div key={i} className="orders-div-sec mb-3 mx-0 mt-0">
                      <h6 className="p-3 bg-dark text-light">
                        <b>Order ID :</b> {order._id}
                      </h6>
                      <ul>
                        <li>
                          <b>Reciepient's Name :</b> {order.title}{" "}
                          {order.recipientsName}
                        </li>
                        <li>
                          <b>Amount : </b> Rs. {order.amount}.00
                        </li>
                        <li>
                          <b>Delivery Fee :</b> Rs. {order.deliveryFee}.00
                        </li>
                        <li>
                          <b>Total Payemnt :</b> Rs.{" "}
                          {order.amount + parseInt(order.deliveryFee)}.00
                        </li>
                        <li>
                          <b>Order Date : </b>{" "}
                          {moment(order.transactionTime).format("LLLL")}
                        </li>
                        <li>
                          <b>Address :</b> {order.address}
                        </li>
                        <li>
                          <b>Phone :</b> {order.phone}
                        </li>
                        <li>
                          <b>Location Type :</b> {order.locationType}
                        </li>
                        <li>
                          <b>Additional Delivery Instructions :</b>{" "}
                          {order.deliveryInstructions !== ""
                            ? order.deliveryInstructions
                            : "none"}
                        </li>
                      </ul>

                      <table
                        className="table table-responsive"
                        style={{ border: "0" }}
                      >
                        <thead>
                          <tr className="bg-dark text-light">
                            <td>product ID</td>
                            <td>Name</td>
                            <td>Image</td>
                            <td>Price</td>
                            <td>Quantity</td>
                            <td>Color</td>
                          </tr>
                        </thead>
                        <tbody>
                          {order.orderItems.map((orderItem, pIndex) => (
                            <tr key={pIndex}>
                              <td>{orderItem._id}</td>
                              <td>{orderItem.name}</td>
                              <td>
                                <img
                                  src={`/api/product/photo/front/${orderItem._id}`}
                                  width="80"
                                  alt={orderItem.name}
                                />
                              </td>
                              <td>Rs. {orderItem.newPrice}.00</td>
                              <td align="center">{orderItem.quantity}</td>
                              <td>{orderItem.color}</td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

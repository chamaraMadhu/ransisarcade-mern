// import packages
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import moment from "moment";
import Rating from "react-rating";

// import css
import "../../css/client/shopStyle.css";

// import images
import like from "../../images/like.png";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import SearchSection from "../../components/client/SearchSection";
import AdvancedFilter from "../../components/client/AdvancedFilter";
import Sorting from "../../components/client/Sorting";
import ShopSideBarFilters from "../../components/client/ShopSideBarFilters";
import NotificationMsg from "../../components/client/NotificationMsg";
import AddToModal from "../../components/client/AddToModal";
import PageTitle from "../../components/client/PageTitle";
import Pagination from "../../components/Pagination";

// load addToCart methord from cartMethods
import { addToCart } from "../../components/client/CartMethods";

// load addToCart methord from cartMethods
import { addToWishlist } from "../../methods/wishlistMethods";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

export default class Shop extends Component {
  state = {
    products: [],

    // breadcrumb
    showingResults: "",

    // add to cart
    redirectToCart: false,

    // add to wishlist
    redirectToWishlist: false,

    // likes
    likes: [],

    // redirect to login to like
    redirectToLogin: false,
    error: "",

    // comment
    productId: "",
    comment: "",
    comments: [],

    // add to cart model
    modelProductId: "",

    // pagination
    currentPage: 1,
    itemsPerPage: 20
  };

  componentDidMount = () => {
    // get for sale products
    axios.get(`/api/products?sortBy=createdAt&order=desc`).then(products => {
      this.setState({ products: products.data, showingResults: "All Sarees" });
    });
  };

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // advanced filtering
  handleSubmit = (categoryVal, fabricVal, occasionVal, priceVal, colorVal) => {
    console.log(occasionVal);
    let category =
      categoryVal === "Any" || categoryVal === "" ? "Any" : categoryVal;
    let fabric = fabricVal === "Any" || fabricVal === "" ? "Any" : fabricVal;
    let occasion =
      occasionVal === "Any" || occasionVal === "" ? "Any" : occasionVal;
    let price = priceVal === "Any" || priceVal === "" ? "Any" : priceVal;
    let color = colorVal === "Any" || colorVal === "" ? "Any" : colorVal;

    let min;
    let max;

    switch (price) {
      case "LKR. 0 - LKR. 2500":
        min = 0;
        max = 0;
        break;
      case "LKR. 2500 - LKR. 5000":
        min = 2500;
        max = 5000;
        break;
      case "LKR. 5000 - LKR. 7500":
        min = 5000;
        max = 7500;
        break;
      case "LKR. 7500 - LKR. 10000":
        min = 7500;
        max = 10000;
        break;
      case "LKR. 10000 - LKR. 15000":
        min = 10000;
        max = 15000;
        break;
      case "LKR. 15000 - LKR. 20000":
        min = 15000;
        max = 20000;
        break;
      case "LKR. 20000 - LKR. 25000":
        min = 20000;
        max = 25000;
        break;
      case "LKR. 25000 - LKR. 35000":
        min = 25000;
        max = 35000;
        break;
      case "LKR. 35000 - LKR. 50000":
        min = 35000;
        max = 50000;
        break;
      case "LKR. 50000 - LKR. 100000":
        min = 50000;
        max = 100000;
        break;
      default:
        min = 0;
        max = 100000;
    }

    // send get request for advanced filtering
    axios
      .get(
        `/api/products/advancedFilter/${category}/${fabric}/${occasion}/${min}/${max}/${color}`
      )
      .then(products => {
        this.setState({
          products: products.data,
          showingResults: "Advanced Filtering"
        });
      });
  };

  // handle card images
  handleImages = e => {
    // get src of clicked image
    let imageSrc = e.target.src;

    // asign that src to the main image
    e.target.parentElement.parentElement.parentElement.children[0].src = imageSrc;
  };

  // basic filter by category
  handleCategory = categoryName => {
    // get category name from id
    axios.get(`/api/category/${categoryName}`).then(category => {
      this.setState({ showingResults: category.data.name });
    });

    axios
      .get(`/api/products/category/${categoryName}`)
      .then(products => {
        this.setState({ products: products.data });

        // scroll up to results
        if (window.innerWidth > 575) {
          window.scrollTo({ top: 500, behavior: "smooth" });
        } else {
          window.scrollTo({ top: 2500, behavior: "smooth" });
        }
      })
      .catch(err => console.log(err));
  };

  // basic filter by fabric
  handleFabric = fabricName => {
    // get fabric name from id
    axios.get(`/api/fabric/${fabricName}`).then(fabric => {
      this.setState({ showingResults: fabric.data.name });
    });

    axios
      .get(`/api/products/fabric/${fabricName}`)
      .then(products => {
        this.setState({ products: products.data });

        // scroll up to results
        if (window.innerWidth > 575) {
          window.scrollTo({ top: 500, behavior: "smooth" });
        } else {
          window.scrollTo({ top: 2500, behavior: "smooth" });
        }
      })
      .catch(err => console.log(err));
  };

  // basic filter by occasion
  handleOccasion = occasion => {
    axios
      .get(`/api/products/occasion/${occasion}`)
      .then(products => {
        this.setState({ products: products.data, showingResults: occasion });

        // scroll up to results
        if (window.innerWidth > 575) {
          window.scrollTo({ top: 500, behavior: "smooth" });
        } else {
          window.scrollTo({ top: 2500, behavior: "smooth" });
        }
      })
      .catch(err => console.log(err));
  };

  // basic filter by price
  handlePrice = (min, max) => {
    axios.get(`/api/products/price/${min}/${max}`).then(products => {
      this.setState({
        products: products.data,
        showingResults: `LKR.${min} - LKR.${max}`
      });

      // scroll up to results
      if (window.innerWidth > 575) {
        window.scrollTo({ top: 500, behavior: "smooth" });
      } else {
        window.scrollTo({ top: 2500, behavior: "smooth" });
      }
    });
  };

  // basic filter by color
  handleColor = color => {
    axios.get(`/api/products/color/${color}`).then(products => {
      this.setState({ products: products.data, showingResults: color });

      // scroll up to results
      if (window.innerWidth > 575) {
        window.scrollTo({ top: 500, behavior: "smooth" });
      } else {
        window.scrollTo({ top: 2500, behavior: "smooth" });
      }
    });
  };

  // basic filter by likes
  handleLikes = () => {
    axios.get(`/api/products/likes`).then(products => {
      this.setState({ products: products.data, showingResults: "Likes" });

      // scroll up to results
      if (window.innerWidth > 575) {
        window.scrollTo({ top: 500, behavior: "smooth" });
      } else {
        window.scrollTo({ top: 2500, behavior: "smooth" });
      }
    });
  };

  // basic filter by likes
  handleComments = () => {
    axios.get(`/api/products/comments`).then(products => {
      this.setState({ products: products.data, showingResults: "Comments" });

      // scroll up to results
      if (window.innerWidth > 575) {
        window.scrollTo({ top: 500, behavior: "smooth" });
      } else {
        window.scrollTo({ top: 2500, behavior: "smooth" });
      }
    });
  };

  // basic filter by ratings
  handleRatings = (ratingFloor, ratingCeil) => {
    axios
      .get(`/api/products/ratings/${ratingFloor}/${ratingCeil}`)
      .then(products => {
        this.setState({ products: products.data, showingResults: "Ratings" });

        // scroll up to results
        if (window.innerWidth > 575) {
          window.scrollTo({ top: 500, behavior: "smooth" });
        } else {
          window.scrollTo({ top: 2500, behavior: "smooth" });
        }
      })
      .catch(err => console.log(err));
  };

  // search product
  handleSearch = keyword => {
    if (this.state.keyword !== "") {
      axios.get(`/api/products/search/${keyword}`).then(products => {
        this.setState({
          products: products.data,
          showingResults: this.state.keyword
        });
      });
    }
  };

  sortByArrivals = () => {
    console.log(this.state.products);
    let newArrivals = this.state.products.sort((a, b) => {
      return b.createdAt.localeCompare(a.createdAt);
    });

    this.setState({ products: newArrivals });
  };

  sortByAZ = () => {
    let AZ = this.state.products.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });

    this.setState({ products: AZ });
  };

  sortByZA = () => {
    let ZA = this.state.products.sort((a, b) => {
      return b.name.localeCompare(a.name);
    });

    this.setState({ products: ZA });
  };

  sortByPriceLH = () => {
    let priceLH = this.state.products.sort((a, b) => {
      return a.newPrice - b.newPrice;
    });

    this.setState({ products: priceLH });
  };

  sortByPriceHL = () => {
    let priceHL = this.state.products.sort((a, b) => {
      return b.newPrice - a.newPrice;
    });

    this.setState({ products: priceHL });
  };

  // add to cart
  handleAddToCart = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let cartItem = products.data;

      addToCart(cartItem, () => {
        this.setState({ modelProductId: id });

        // clear alert
        setTimeout(() => this.setState({ modelProductId: "" }), 3000);
      });
    });
  };

  // add to wishlist
  handleAddToWishlist = id => {
    axios.get(`/api/product/${id}`).then(products => {
      let wishlistItem = products.data;

      addToWishlist(wishlistItem, () => {
        this.setState({ modelProductId: id });

        // clear alert
        setTimeout(() => this.setState({ modelProductId: "" }), 2000);
      });
    });
  };

  // handle likes
  handleViewLikes = id => {
    axios.get(`/api/product/likes/${id}`).then(products => {
      this.setState({ likes: products.data });
    });
  };

  // add like
  addLike = id => {
    if (isAuthenticated() && isAuthenticated().user.role === 0) {
      axios
        .post(`/api/product/add/like/${id}/${isAuthenticated().user._id}`)
        .then(products => {
          axios
            .get(`/api/products?sortBy=createdAt&order=desc`)
            .then(products => {
              this.setState({ products: products.data });
            });
        })
        .catch(err => {
          if (err.response.data.alreadyLikeError) {
            // set alert message
            this.setState({ error: err.response.data.alreadyLikeError });

            // clear alert message
            setTimeout(() => this.setState({ error: "" }), 3000);
          }
        });
    } else {
      this.setState({ redirectToLogin: true });
    }
  };

  // get view comments
  handleViewComments = id => {
    this.setState({ productId: id });

    axios.get(`/api/product/comments/${id}`).then(products => {
      this.setState({ comments: products.data });
    });
  };

  // add comment
  handleCommentSubmit = id => {
    if (this.state.comment !== "") {
      // create comment data object
      const commentData = {
        productId: id,
        userId: isAuthenticated().user._id,
        comment: this.state.comment
      };

      // send post req to add commnet
      axios.post(`/api/product/add/comment`, commentData).then(product => {
        if (product) {
          // update products
          axios
            .get(`/api/products?sortBy=createdAt&order=desc`)
            .then(products => {
              this.setState({ products: products.data });
            });

          // update comments
          axios.get(`/api/product/comments/${id}`).then(products => {
            if (products.data.length > 0) {
              this.setState({ comments: products.data });
            }
          });

          this.setState({
            comment: ""
          });
        }
      });
    }
  };

  // redirect to login
  handleLoginToComment = () => {
    this.setState({ redirectToLogin: true });
  };

  // pagination change page
  handlePage = page => {
    this.setState({ currentPage: page });

    // scroll up to results
    if (window.innerWidth > 575) {
      window.scrollTo({ top: 500, behavior: "smooth" });
    } else {
      window.scrollTo({ top: 2500, behavior: "smooth" });
    }
  };

  render() {
    // redirect to login to like
    if (this.state.redirectToLogin === true) {
      return <Redirect to="/login" />;
    }

    // pagination
    // get current products
    const indexOfLast = this.state.currentPage * this.state.itemsPerPage;
    const indexOfFirst = indexOfLast - this.state.itemsPerPage;
    const currentItems = this.state.products.slice(indexOfFirst, indexOfLast);

    return (
      <React.Fragment>
        {this.state.error !== "" ? (
          <NotificationMsg msgType="error" message={this.state.error} />
        ) : null}

        {/* page title */}
        <PageTitle title="Shop - Ransis Arcade" />

        <Navbar />

        <SearchSection searchProducts={this.handleSearch} />

        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-4">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">Shop</li>
            <li className="breadcrumb-item">{this.state.showingResults}</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="shop-container mt-0">
          <AdvancedFilter advancedFilter={this.handleSubmit} />

          <Sorting
            sortByArrivals={this.sortByArrivals}
            sortByAZ={this.sortByAZ}
            sortByZA={this.sortByZA}
            sortByPriceLH={this.sortByPriceLH}
            sortByPriceHL={this.sortByPriceHL}
          />

          <div className="row p-0 m-0">
            <ShopSideBarFilters
              likeFilter={this.handleLikes}
              commentFilter={this.handleComments}
              ratingsFilter={this.handleRatings}
              categoryFilter={this.handleCategory}
              fabricFilter={this.handleFabric}
              occasionFilter={this.handleOccasion}
              priceFilter={this.handlePrice}
              colorFilter={this.handleColor}
            />

            <div className="shop-content">
              <p className="pl-4" style={{ fontWeight: "600" }}>
                {this.state.products.length > 0
                  ? `Showing ${indexOfFirst + 1} - ${indexOfFirst +
                      currentItems.length} out of ${
                      this.state.products.length
                    } results`
                  : "No results"}
              </p>
              {currentItems.map(products => (
                <div key={products._id} className="card product">
                  <img
                    src={`/api/product/photo/front/${products._id}`}
                    className="product-image"
                    alt={products.name}
                  />
                  <Link
                    to={`/product-detail/${products._id}`}
                    className="view-details-btn"
                  >
                    VIEW DETAILS
                  </Link>
                  <button
                    className="add-to-cart-btn"
                    onClick={() => this.handleAddToCart(products._id)}
                  >
                    ADD TO CART
                  </button>
                  <button
                    className="mr-1 product-wishlist-btn"
                    onClick={() => this.handleAddToWishlist(products._id)}
                  >
                    <i className="far fa-heart"></i>
                  </button>
                  <div className="row mx-0 p-1">
                    <div className="col p-0">
                      <img
                        src={`/api/product/photo/front/${products._id}`}
                        className="thumb-images"
                        alt={products.name}
                        onClick={this.handleImages}
                      />
                      <img
                        src={`/api/product/photo/jacket/${products._id}`}
                        className="thumb-images"
                        alt={products.name}
                        onClick={this.handleImages}
                      />
                      <img
                        src={`/api/product/photo/border/${products._id}`}
                        className="thumb-images"
                        alt={products.name}
                        onClick={this.handleImages}
                      />
                      <img
                        src={`/api/product/photo/back/${products._id}`}
                        className="thumb-images"
                        alt={products.name}
                        onClick={this.handleImages}
                      />
                    </div>
                  </div>
                  <div className="px-2 pb-2 product-body">
                    <Rating
                      className="product-card-ratings"
                      initialRating={products.rating}
                      readonly
                      emptySymbol="far fa-star"
                      fullSymbol="fas fa-star"
                    />
                    <p className="text-sm pt-0 category">
                      {products.category.name}
                    </p>
                    <h6 className="mb-0 product-name">{products.name}</h6>

                    <span className="card-text text-sm mb-0 text-muted product-price">
                      LKR. {products.newPrice}.00
                    </span>
                    <span className="product-dis-price">
                      <strike>LKR. {products.oldPrice}.00</strike>
                    </span>

                    <div className="row mx-0 mt-2">
                      <span>
                        <img
                          src={like}
                          className="rounded-circle like-images"
                          alt="like"
                          data-toggle="modal"
                          data-target="#like-product"
                          onClick={() => this.handleViewLikes(products._id)}
                        />
                      </span>
                      <span className="text-muted p-1 like-counts">
                        {products.likes}
                      </span>
                      <span
                        className="text-muted comment"
                        data-toggle="modal"
                        data-target="#comment-product"
                        onClick={() => this.handleViewComments(products._id)}
                      >
                        {products.comments} Comments
                      </span>
                    </div>
                  </div>

                  <div className="dropdown-divider mb-1"></div>

                  <div className="row mx-0 mb-1">
                    <div className="col p-0">
                      <span
                        className="text-muted like-action"
                        onClick={() => this.addLike(products._id)}
                      >
                        <i className="far fa-thumbs-up"></i> Like
                      </span>
                    </div>

                    <div className="col pl-0 pr-1">
                      <span
                        className="text-muted comment-action"
                        data-toggle="modal"
                        data-target="#comment-product"
                        onClick={() => this.handleViewComments(products._id)}
                      >
                        <i className="far fa-comment-alt"></i> Comment
                      </span>
                    </div>
                  </div>
                </div>
              ))}
            </div>

            {/* starts of like model */}
            <div
              className="modal fade"
              id="like-product"
              tabIndex="-1"
              role="dialog"
              aria-labelledby="like-product"
              aria-hidden="true"
            >
              <div
                className="modal-dialog modal-dialog-centered"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-body">
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>

                    <p style={{ fontSize: "17px", fontWeight: "600" }}>
                      {this.state.likes.length} Likes
                    </p>
                    {this.state.likes.length > 0
                      ? this.state.likes.map((like, i) => (
                          <div className="row m-0 mt-3" key={i}>
                            <div className="comment-img-col">
                              <img
                                src={like.userId.image}
                                className="reviwer-img"
                                alt={like.userId.fName}
                              />
                            </div>
                            <div className="comment-desc-col">
                              <p className="reviwer-name mt-2">
                                {like.userId.fName} {like.userId.lName}
                                <span className="reviwe-date">
                                  &nbsp; {moment(like.createdAt).fromNow()}
                                </span>
                              </p>
                            </div>
                          </div>
                        ))
                      : null}
                  </div>
                </div>
              </div>
            </div>
            {/* ends of like model */}

            {/* starts of comment model */}
            <div
              className="modal fade"
              id="comment-product"
              tabIndex="-1"
              role="dialog"
              aria-labelledby="comment-product"
              aria-hidden="true"
            >
              <div
                className="modal-dialog modal-dialog-centered"
                role="document"
              >
                <div className="modal-content">
                  <div className="modal-body">
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">&times;</span>
                    </button>

                    <p style={{ fontSize: "17px", fontWeight: "600" }}>
                      {this.state.comments.length} comments
                    </p>
                    {this.state.comments.length > 0
                      ? this.state.comments.map((comment, i) => (
                          <div className="row m-0" key={i}>
                            <div className="comment-img-col">
                              <img
                                src={comment.userId.image}
                                className="reviwer-img"
                                alt={comment.userId.fName}
                              />
                            </div>
                            <div className="comment-desc-col">
                              <p className="reviwer-name">
                                {comment.userId.fName} {comment.userId.lName}
                                <span className="reviwe-date">
                                  &nbsp; {moment(comment.createdAt).fromNow()}
                                </span>
                              </p>
                              <p className="review">{comment.comment}</p>
                            </div>
                          </div>
                        ))
                      : null}

                    <div className="row m-0 mt-3">
                      {isAuthenticated() && isAuthenticated().user._id ? (
                        <React.Fragment>
                          <div className="comment-img-col">
                            <img
                              src={
                                isAuthenticated() &&
                                isAuthenticated().user.image
                              }
                              className="reviwer-img"
                              alt="reviwer img"
                            />
                          </div>
                          <div className="comment-desc-col">
                            <textarea
                              className="comment-input d-block"
                              type="text"
                              name="comment"
                              placeholder="Write a comment"
                              value={this.state.comment}
                              onChange={this.handleChange}
                            ></textarea>

                            <button
                              className="btn-register w-100 mt-2"
                              onClick={() =>
                                this.handleCommentSubmit(this.state.productId)
                              }
                            >
                              Submit
                            </button>
                          </div>
                        </React.Fragment>
                      ) : (
                        <Link
                          className="btn-register"
                          data-dismiss="modal"
                          aria-label="Close"
                          onClick={this.handleLoginToComment}
                          to="/login"
                        >
                          Login to Comment
                        </Link>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* ends of comment model */}

            {/* starts of AddToModal */}
            {this.state.modelProductId !== "" ? (
              <React.Fragment>
                <AddToModal modalId={this.state.modelProductId} />
                <div className="modal-backdrop fade show" id="modal-dark"></div>
              </React.Fragment>
            ) : null}
            {/* ends of AddToModal */}
          </div>

          {/* starts of pagination */}
          <Pagination
            className="m-0"
            noOfAllItems={this.state.products.length}
            itemsPerPage={this.state.itemsPerPage}
            currentPage={this.state.currentPage}
            handlePage={this.handlePage}
          />
          {/* ends of pagination */}
        </div>

        <Footer />
      </React.Fragment>
    );
  }
}

// import packages
import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

// import css
import "../../css/client/cartStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

// load cart methords from cartMethods
import { removeCartItem } from "../../components/client/CartMethods";

export default class Cart extends Component {
  state = {
    // cartItems: [],
    onlinePayment: false,
    cashOnDelivery: false
  };

  componentDidMount = () => {
    // get cart items from local storage
    if (typeof window !== "undefined") {
      if (localStorage.getItem("cart")) {
        const cart = JSON.parse(localStorage.getItem("cart"));

        if (cart.length > 0) {
          let arr = [];
          let insertId;

          for (let i = 0; i < cart.length; i++) {
            insertId = cart[i]._id;

            arr = [...arr, insertId];
          }

          axios
            .post(`/api/filter/cart/products`, { cartItemsIds: arr })
            .then(result => {
              if (result.data.length > 0) {
                this.setState({ cartItems: result.data });

                // store cart on local storage
                localStorage.setItem("cart", JSON.stringify(result.data));
              } else {
                localStorage.removeItem("cart");
              }
            })
            .catch(err => console.log(err));
        }
      }
    }
  };

  // remove items from cart
  handleRemoveCartItem = id => {
    // execute removeCartItem methord
    removeCartItem(id);

    // update the cart
    const cart = JSON.parse(localStorage.getItem("cart"));
    this.setState({ cartItems: cart });
  };

  // checkout options
  selectCheckoutOption = () => {
    if (document.getElementById("onlinePayment").checked === true) {
      this.setState({ onlinePayment: true });
    } else {
      this.setState({ cashOnDelivery: true });
    }
  };

  render() {
    // redirect to online payment
    if (this.state.onlinePayment) {
      return <Redirect to="/checkout" />;
    }

    // redirect to cash on delivery
    if (this.state.cashOnDelivery) {
      return <Redirect to="/checkout/cash-on-delivery" />;
    }
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Shopping Cart - Ransis Arcade" />

        <Navbar />
        {/* starts of  breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-5">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/shop" className="breadcrumb-item-text-bold">
                Shop
              </Link>
            </li>
            <li className="breadcrumb-item active" aria-current="page">
              My Cart
            </li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container mb-5" style={{ minHeight: "35vh" }}>
          <h4 className="text-center mt-5 mb-4 second-color">Shopping Cart</h4>
          {this.state.cartItems && this.state.cartItems.length > 0 ? (
            <div>
              <div className="row m-0">
                <div className="col p-0" style={{ overflowX: "auto" }}>
                  <table className="w-100">
                    <tbody>
                      <tr
                        className="text-light bg-dark"
                        style={{ height: "40px" }}
                      >
                        <td></td>
                        <td className="column-image">Product</td>
                        <td className="column-name">Name</td>
                        <td className="column-color">Color</td>
                        <td className="column-price">Price</td>
                        <td className="column-qty">Qty</td>
                        <td className="column-subtotal pr-3">Subtotal</td>
                      </tr>

                      {this.state.cartItems.map((cartItems, i) => (
                        <tr className="text-muted border-bottom" key={i}>
                          <td>
                            <i
                              className="fa fa-trash-alt btn remove-btn"
                              aria-hidden="true"
                              onClick={() =>
                                this.handleRemoveCartItem(cartItems._id)
                              }
                              style={{ cursor: "pointer" }}
                            ></i>
                          </td>
                          <td>
                            <img
                              className="border mr-2 mt-2 mb-2"
                              src={`/api/product/photo/front/${cartItems._id}`}
                              width="80px"
                              // height="110px"
                              alt={cartItems.name}
                            />
                          </td>
                          <td>{cartItems.name}</td>
                          <td>{cartItems.color}</td>
                          <td>LKR. {cartItems.newPrice}.00</td>
                          <td align="center">
                            <span className="order-quantity">
                              {cartItems.count}
                            </span>
                          </td>
                          <td align="right" className="pr-3">
                            LKR. {cartItems.newPrice * cartItems.quantity}.00
                          </td>
                        </tr>
                      ))}

                      <tr className="text-muted border-bottom">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td className="total">Total</td>
                        <td
                          style={{
                            fontSize: "18px",
                            fontWeight: "600",
                            height: "50px"
                          }}
                          align="right"
                          className="pr-3"
                        >
                          LKR.{" "}
                          {this.state.cartItems.reduce(
                            (currentValue, nextValue) => {
                              return (
                                currentValue +
                                nextValue.quantity * nextValue.newPrice
                              );
                            },
                            0
                          )}
                          .00
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <div className="row mt-3">
                <div className="col">
                  <div
                    style={{
                      width: "155px",
                      float: "right",
                      fontWeight: "600",
                      color: "#3e3e3e"
                    }}
                  >
                    <div className="custom-control custom-radio mb-1">
                      <input
                        type="radio"
                        id="onlinePayment"
                        name="customRadio"
                        className="custom-control-input"
                        defaultChecked
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="onlinePayment"
                      >
                        Online payment
                      </label>
                    </div>
                    <div className="custom-control custom-radio">
                      <input
                        type="radio"
                        id="cashOnDelivery"
                        name="customRadio"
                        className="custom-control-input"
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="cashOnDelivery"
                      >
                        Cash on delivery
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <div className="row mt-3">
                <div className="col-sm">
                  <Link to="/shop" className="continue-shopping-btn">
                    <i className="fas fa-angle-double-left"></i>
                    &nbsp; Continue Shopping
                  </Link>
                </div>
                <div className="col-sm">
                  {isAuthenticated() && isAuthenticated().user.role === 0 ? (
                    <button
                      className="btn-register float-right"
                      onClick={this.selectCheckoutOption}
                    >
                      Proceed to Checkout &nbsp;
                      <i className="fas fa-angle-double-right"></i>
                    </button>
                  ) : (
                    <Link to="/login" className="btn-register float-right">
                      Login to Checkout &nbsp;{" "}
                      <i className="fas fa-angle-double-right"></i>
                    </Link>
                  )}
                </div>
              </div>
            </div>
          ) : (
            <p className="text-center">No cart items</p>
          )}
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

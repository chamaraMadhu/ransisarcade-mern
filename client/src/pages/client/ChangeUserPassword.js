// import packages
import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import classnames from "classnames";
import SweetAlert from "react-bootstrap-sweetalert";

// import components
import Navbar from "../../components/client/Navbar";
import UserAccountSidebar from "../../components/client/UserAccountSidebar";
import Footer from "../../components/client/Footer";
import NotificationMsg from "../../components/client/NotificationMsg";
import PageTitle from "../../components/client/PageTitle";

// load isAuthenticated method
import { isAuthenticated } from "../../auth/auth.js";

// initiate state
const initialState = {
  oldPassword: "",
  newPassword: "",
  confirmPassword: "",
  errors: {},
  success: false,
  fail: false
};

export default class ChangeUserPassword extends Component {
  state = initialState;

  // handle inputs
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // handle change password
  handleSubmit = () => {
    // create user data object
    const userData = {
      oldPassword: this.state.oldPassword,
      newPassword: this.state.newPassword,
      confirmPassword: this.state.confirmPassword
    };

    // send put request to change password
    axios
      .put(
        `/api/user/change/password/${isAuthenticated().user._id}`,
        userData,
        { headers: { Authorization: `Bearer ${isAuthenticated().token}` } }
      )
      .then(user => {
        if (user) {
          // clear form
          this.setState(initialState);

          // for sending successfull alert
          this.setState({ success: true });
        }
      })
      .catch(err => this.setState({ errors: err.response.data }));
  };

  // close modal
  close = () => {
    this.setState({ success: false, errors: {} });
  };

  render() {
    return (
      <React.Fragment>
        {/* page title */}
        <PageTitle title="Change Account Password- Ransis Arcade" />

        <Navbar />
        {/* starts of breadcrumb  */}
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb bg-light mb-0">
            <li className="breadcrumb-item">
              <Link to="/" className="breadcrumb-item-text-bold">
                Home
              </Link>
            </li>
            <li className="breadcrumb-item">
              <Link to="/user-account" className="breadcrumb-item-text-bold">
                Customer Account
              </Link>
            </li>
            <li className="breadcrumb-item">Change Password</li>
          </ol>
        </nav>
        {/* ends of breadcrumb */}

        <div className="container my-3">
          <div className="row">
            <UserAccountSidebar />

            <div className="bg-light px-0 profile-content">
              <h5>CHANGE YOUR Password</h5>
              <div className="mb-4">
              {this.state.success ? (
                    <SweetAlert
                      success
                      title="Successfull"
                      onConfirm={this.close}
                      confirmBtnText="Ok"
                      confirmBtnStyle={{
                        background: "#423c59",
                        border: 0,
                        width: 70
                      }}
                    >
                      your password has been updated successfully.
                    </SweetAlert>
                ) : null}

                {this.state.errors.fail ? (
                  <SweetAlert
                  warning
                  title="Oops, Something went wrong!"
                  onConfirm={this.close}
                  confirmBtnText="Ok"
                  confirmBtnStyle={{
                    background: "#423c59",
                    border: 0,
                    width: 70
                  }}
                >
                  your password hasn't been updated successfully.
                </SweetAlert>
                ) : null}

                {this.state.fail ? "Updation is not successfull!" : null}

                <table className="profile-table">
                  <tbody>
                    <tr>
                      <td>Old Password</td>
                      <td>
                        <input
                          type="password"
                          name="oldPassword"
                          placeholder="Please enter old password"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.oldPassword
                          })}
                          value={this.state.oldPassword}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.oldPassword}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>New Password</td>
                      <td>
                        <input
                          type="password"
                          name="newPassword"
                          placeholder="Please enter new password"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.newPassword
                          })}
                          value={this.state.newPassword}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.newPassword}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Confirm Password</td>
                      <td>
                        <input
                          type="password"
                          name="confirmPassword"
                          placeholder="Please Confirm your password"
                          className={classnames("form-control", {
                            "is-invalid": this.state.errors.confirmPassword
                          })}
                          value={this.state.confirmPassword}
                          onChange={this.handleChange}
                        />
                        <div className="invalid-feedback">
                          {this.state.errors.confirmPassword}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <button
                          type="button"
                          className="btn-register"
                          onClick={this.handleSubmit}
                        >
                          Save Changes
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </React.Fragment>
    );
  }
}

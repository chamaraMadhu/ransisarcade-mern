// import packages
import React from "react";
import { Link } from "react-router-dom";

// import css
import "../../css/client/AboutStyle.css";

// import components
import Navbar from "../../components/client/Navbar";
import Footer from "../../components/client/Footer";
import PageTitle from "../../components/client/PageTitle";

export default function About() {
  return (
    <React.Fragment>
      {/* page title */}
      <PageTitle title="About us - Ransis Arcade" />

      <Navbar />
      {/* starts of jumbotron */}
      <div class="jumbotron jumbotron-about mb-0">
        <div class="container">
          <div class="pt-5">
            <h2 class="text-center" style={{ color: "#fff" }}>
              About Us
            </h2>
          </div>
        </div>
      </div>
      {/* ends of jumbotron */}

      {/* starts of  breadcrumb  */}
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb bg-light mb-4">
          <li className="breadcrumb-item">
            <Link to="/" className="breadcrumb-item-text-bold">
              Home
            </Link>
          </li>
          <li className="breadcrumb-item">About</li>
        </ol>
      </nav>
      {/* ends of breadcrumb */}

      <div class="container">
        <div class="row">
          <div class="col px-4 text-center">
            <h3 class="second-color mt-0 mb-3 mt-3">Our Story</h3>
            <p>
              <i
                class="fa fa-heart"
                aria-hidden="true"
                style={{ color: "#2ABDFC" }}
              ></i>
            </p>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col px-4 text-center">
            <p class="about-para">
              Most of the cloth shop sellers do not facilitate purchase clothes
              online from their own sites, the only facility which they
              providing to customer is to view the product online and go to
              their location and purchase it. Now days most people are like to
              purchase their products online rather than going and purchasing it
              from a cloth shop.
            </p>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col px-4 text-center">
            <h3 class="second-color mt-0 mb-3 mt-3">Vision</h3>
            <p class="about-para">
              Most of the cloth shop sellers do not facilitate purchase clothes
              online from their own sites, the only facility which they
              providing to customer is to view the product online and go to
              their location and purchase it.
            </p>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col px-4 text-center">
            <h3 class="second-color mt-0 mb-3 mt-3">Mission</h3>
            <p class="about-para">
              Most of the cloth shop sellers do not facilitate purchase clothes
              online from their own sites,{" "}
            </p>
          </div>
        </div>
      </div>

      <Footer />
    </React.Fragment>
  );
}
